// Chrome's currently missing some useful cache methods,
// this polyfill adds them.
importScripts('serviceworker-cache-polyfill.js');

// Here comes the install event!
// This only happens once, when the browser sees this
// version of the ServiceWorker for the first time.
self.addEventListener('install', function(event) {
  // We pass a promise to event.waitUntil to signal how 
  // long install takes, and if it failed
  event.waitUntil(
    // We open a cache…
    caches.open('simple-sw-v1').then(function(cache) {
      // And add resources to it
      return cache.addAll([
        './',
        
   'js/app.js',
   'js/controllers.js',
   'js/directives.js',
   'js/filters.js',
   'js/services.js',
   'js/factories.js',
   'js/views.js',
   'js/config.js',
   'js/ionic-ratings.js',

    'lib/ionic/js/ionic.bundle.min.js',
    'lib/jquery/jquery-1.11.0.min.js',
    'lib/angular-resource/angular-resource.min.js',
    'lib/underscore/underscore-min.js',
   
    'lib/ngCordova/dist/ng-cordova.min.js',
    'lib/moment/min/moment.min.js',
    'lib/angular-moment/angular-moment.min.js',
    'lib/moment/locale/mr.js',
    'lib/angular-slugify/dist/angular-slugify.min.js',
    'lib/angular-image-compress/angular-image-compress.js',
    'lib/collide/collide.js',
    'lib/ionic-zoom-view.js',
    
    'lib/angular-youtube-mb/dist/angular-youtube-embed.min.js',
    
    'lib/MultipleDatePicker/multipleDatePicker.min.js',
    'lib/ngCordova/dist/ng-cordova.min.js',
    'lib/ionic-datepicker/dist/ionic-datepicker.bundle.min.js'
      ]);
    })
  );
});

// The fetch event happens for the page request with the
// ServiceWorker's scope, and any request made within that
// page
/*self.addEventListener('fetch', function(event) {
  // Calling event.respondWith means we're in charge
  // of providing the response. We pass in a promise
  // that resolves with a response object
  event.respondWith(
    // First we look for something in the caches that
    // matches the request
    caches.match(event.request).then(function(response) {
      // If we get something, we return it, otherwise
      // it's null, and we'll pass the request to
      // fetch, which will use the network.
      console.log(JSON.stringify(response));
      return response || fetch(event.request);
    })
  );
});
*/
self.addEventListener('fetch', function(event) {

  var requestURL = new URL(event.request.url);
  console.log(event.request.url);
  if(requestURL.search.includes('news')){
     caches.match(event.request).then(response => {
    const reader = response.body.getReader();
    reader.read().then(result => {
            if (result.done) return ;
            
          });
});
  }else{
  event.respondWith(
    caches.open('simple-sw-v1').then(function(cache) {
      return fetch(event.request).then(function(response) {
        cache.put(event.request, response.clone());

        return response;
      });
    })
  );
} //end of else
});