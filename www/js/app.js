// Ionic Starter App

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

angular.module('bhavydivya', [
  'ionic',
  'angularMoment',
  'bhavydivya.controllers',
  'bhavydivya.directives',
  'bhavydivya.filters',
  'bhavydivya.services',
  'bhavydivya.factories',
  'bhavydivya.config',
  'bhavydivya.views',
  'underscore',
  'ngImageCompress',
  'multipleDatePicker',
  'ngResource',
  'ngCordova',
  'slugifier',
  'ionic-zoom-view',
  'ionic-datepicker',
  'youtube-embed',
  
  'btford.socket-io'
])

.run(function($ionicPlatform,RequestsService,$state,AdMob, $rootScope, $ionicConfig, $timeout,$ionicPopup,SocketService) {
        $ionicPlatform.registerBackButtonAction(function (event) {
            if($state.current.name=="app.Home"){
              navigator.app.exitApp();
            }
            else {
              navigator.app.backHistory();
            }
          }, 500);



                  $ionicPlatform.ready(function() {
                    // Enable to debug issues.
                  // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
                  
                  var notificationOpenedCallback = function(jsonData) {
                   // console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                   // alert(JSON.stringify(jsonData.notification));
                    var newList=localStorage.notification?JSON.parse(localStorage.notification):[];
                    newList.push(jsonData.notification);
                    localStorage.notification=JSON.stringify(newList);
                    $rootScope.$broadcast('new Notification',{});
                  };

                  window.plugins.OneSignal
                    .startInit("83a3815b-c0cd-4f0f-9035-eb32ab614845")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
                    

                  // Call syncHashedEmail anywhere in your app if you have the user's email.
                  // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
                  // window.plugins.OneSignal.syncHashedEmail(userEmail);

                  window.plugins.OneSignal.getIds(function(ids) {
                        console.log('getIds: ' + JSON.stringify(ids));
                        //alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);
                        if((localStorage.pushToken && localStorage.pushToken!==ids.pushToken) || !localStorage.pushToken)
                        {
                          RequestsService.register(ids.pushToken,ids.userId).then(function(success){
                            localStorage.pushToken=ids.pushToken;
                            localStorage.userId=ids.userId;
                          });

                        }
                        
                        
                      });

                     SocketService.on('get:taluka', function(data){alert(data);});
                     SocketService.on('get:district', function(data){alert(data);});
                     SocketService.on('get:villages', function(data){alert(data);});
                });


  $ionicPlatform.on("deviceready", function(){
    navigator.splashscreen.hide();


          if(typeof analytics !== undefined) {
                analytics.startTrackerWithId("UA-88265669-1");
                 analytics.trackView($state.current.name);
            } else {
                console.log("Google Analytics Unavailable");
            }
            
    
          if(window.Connection) {
            
                      if(navigator.connection.type == Connection.NONE) {
                          $ionicPopup.alert({
                              title: "इंटरनेट बंद",
                              content: "इंटरनेट बंद  आहे, सुरु करा",
                              okText :"ठीक"
                          })
                          .then(function(result) {
                              
                          });
                      }
                      else{}
                  }

            
    
            if(window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if(window.StatusBar) {
              StatusBar.styleDefault();
            }
    
    

  });


 $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    console.log(JSON.stringify(toState));
    $rootScope.globals = localStorage.currentUser|| [{currentUser:''}];
    console.log($rootScope.globals);
    $rootScope.globals = $rootScope.globals === localStorage.currentUser? angular.fromJson(localStorage.currentUser): [{currentUser:''}];
   
    var restrictedPage = $.inArray(toState.name, ['auth.walkthrough','auth.otp','auth.login','auth.signup','auth.forgot-password','auth.forgot-password','auth.userotp']) === -1;
           console.log("llogout testing  "+JSON.stringify(restrictedPage));
            var loggedIn = ($rootScope.globals[0].UserName!=='' && $rootScope.globals[0].IsVerified == 'True' )? $rootScope.globals[0].UserName: false;
            console.log("llogout testing  "+loggedIn);
             if (restrictedPage && !loggedIn) {
               // $location.path('/login');
               event.preventDefault();

       // $state.transitionTo("app.fooditems", null, {notify:false});
               console.log(JSON.stringify(restrictedPage));
               $state.go('auth.walkthrough');
            }




    if(toState.name.indexOf('auth.walkthrough') > -1)
    {
      
      // set transitions to android to avoid weird visual effect in the walkthrough transitions
      $timeout(function(){
        $ionicConfig.views.transition('android');
        $ionicConfig.views.swipeBackEnabled(false);
        console.log("setting transition to android and disabling swipe back");
      }, 0);
      if(loggedIn){event.preventDefault();

       // $state.transitionTo("app.fooditems", null, {notify:false});
               console.log(JSON.stringify(restrictedPage));
               $state.go('app.Home');}
    }

    

var newsads = $.inArray(toState.name, ['app.News.area','app.News.others','app.newsinclude','app.displaydetailnews','app.newslist','app.detailusernews','app.newsaddcomment']) !== -1;
 if(newsads) AdMob.showBanner('ca-app-pub-2696266532493197/3753570667');

 var specialNews=$.inArray(toState.name, ['app.specialinfo','app.specialinfo.all','app.specialinfo.selected','app.specialinfo.area','app.specialnewsaddcomment']) !== -1;
 if(specialNews) AdMob.showBanner('ca-app-pub-2696266532493197/5230303867');


//'app.News.feed-entries','app.News.area','app.News.others','app.category-feeds','app.feed-entries','app.newsinclude'
// ,'app.displaydetailnews','app.newslist','app.detailusernews','app.newsaddcomment'
// 'app.specialinfo','app.specialinfo.all','app.specialinfo.selected','app.specialinfo.area','app.specialnewsaddcomment'
    
   var simpleAds = $.inArray(toState.name, ['app.feed-entries','app.News.feed-entries','app.notification','app.aboutus','app.contactus','app.submittoadmin.submittoadmin','app.submittoadmin.shareapp','app.submittoadmin.peoplelist']) !== -1;
 if(simpleAds) AdMob.showBanner('ca-app-pub-2696266532493197/9800104268');

 var noticeAds=$.inArray(toState.name,['app.publicnotice','app.publicnotice.getnoticelist','app.detailnoticelist','app.publicnotice.searchnotice','app.publicnotice.Insertnotice','app.detailsearchlist','app.insertvillagepublicnotice','app.userpublicnotice','app.detailusernoticelist'])!== -1;
   if(noticeAds) AdMob.showBanner('ca-app-pub-2696266532493197/6707037065');
    if(toState.name.indexOf('app.feed-entries') > -1) AdMob.showBanner('ca-app-pub-2696266532493197/9800104268');

    var CommonBanner=$.inArray(toState.name, ['app.Home','app.profile','app.eventsearch','app.events','app.events.acceptedevent','app.detailacceptedevent','app.events.invitedevent','app.detailinvitedevents','app.events.allevent','app.detailallevent','app.pastevents','app.detailpastevent','app.insertcustomevent']) !== -1;

if(CommonBanner)  AdMob.removeAds();
if(toState.name.indexOf('auth') !== -1) AdMob.removeAds();
  
  });


 
  $rootScope.$on("$stateChangeSuccess", function(event, toState,toParams, fromState, fromParams){
    if(toState.name.indexOf('app.feeds-categories') > -1)
    {
      // Restore platform default transition. We are just hardcoding android transitions to auth views.
      $ionicConfig.views.transition('platform');
      // If it's ios, then enable swipe back again
      if(ionic.Platform.isIOS())
      {
        $ionicConfig.views.swipeBackEnabled(true);
      }
    	console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
    }
  });
SocketService.on('get:RSSData', function(data){
        localStorage.RSSURL= data;
        //alert(data);
      });
  SocketService.on('get:indData', function(data){

    /*var data={};
    data.storeName="District";
    data.operation="add" //"update" or "delete"
    data.data=[];
{"storeName":"District","operation":"add",
"data":[{"DistrictId":37,"District":"अकोलाPranayTesting","StateId":1,"IsActive":1}]}
    */
    //alert(JSON.stringify(data.storeName));
    var db = idop.result;
    if(db.objectStoreNames.contains(String(data.storeName))) { 
                
                var tx = db.transaction([data.storeName], "readwrite");
                
                var storeV = tx.objectStore(data.storeName);
                data.data.forEach(function(item){
                    if(data.operation=="add")storeV.add(item);
                    if(data.operation=="update")storeV.put(item);
                    if(data.operation=="delete")storeV.delete(item);
                });
                tx.onerror=function(err){};
        }
  
  });
 // SocketService.on('get:district', function(data){alert(angular.toJson(data));window.localStorage.district=angular.toJson(data);});
 // SocketService.on('get:villages', function(data){alert(angular.toJson(data));window.localStorage.villages=angular.toJson(data);});

})

.config(function($stateProvider, $urlRouterProvider,$httpProvider, $ionicConfigProvider)
 {


  $ionicConfigProvider.tabs.position('top');
  $stateProvider


  //INTRO
  .state('auth', {
    url: "/auth",
    templateUrl: "views/auth/auth.html",
    abstract: true,
    controller: 'AuthCtrl'
  })

  .state('auth.walkthrough', {
    url: '/walkthrough',
    templateUrl: "views/auth/walkthrough.html"
  })

  .state('auth.login', {
    url: '/login',
    templateUrl: "views/auth/login.html",
    controller: 'LoginCtrl'
  })

  .state('auth.logout', {
    url: '/logout',
    templateUrl: "views/auth/logout.html",
    controller: 'logoutctrl'
  })

  .state('auth.userotp', {
    url: '/userotp',
    templateUrl: "views/auth/userotp.html",
    controller: 'userotpctrl'
  })

  .state('auth.signup', {
    url: '/signup',
    templateUrl: "views/auth/signup.html",
    controller: 'SignupCtrl'
  })

  .state('auth.otp', {
    url: '/otp/:mobile/:pagefp/:GUID',
    templateUrl: "views/auth/otp.html",
    controller: 'oneTimePasswordCtrl'
  })

  .state('auth.forgot-password', {
    url: "/forgot-password",
    templateUrl: "views/auth/forgot-password.html",
    controller: 'ForgotPasswordCtrl'
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/app/side-menu.html",
    controller: 'AppCtrl',
    cache:false

  })

  //home
.state('app.Home', {
    url: "/dashboard",
    views: {
      'menuContent': {
        templateUrl: "views/app/Home/dashboard.html"
       
      }
    }
  })
//share
.state('app.share', {
    url: "/shareapp",
    views: {
      'menuContent': {
        templateUrl: "views/app/share/shareapp.html",
        controller : 'sharectrl'
        
      }
    }
  })
//notification
.state('app.notification', {
    url: "/notification",
    views: {
      'menuContent': {
        templateUrl: "views/app/Notification/notification.html",
        controller : 'notificationctrl'

      }
    }
  })

//event search
.state('app.eventsearch', {
    url: "/eventsearch",
    views: {
      'menuContent': {
        templateUrl: "views/app/eventsearch/eventsearch.html",
        controller : 'programsearchctrl'
      }
    }
  })

  .state('app.displayeventsearch', {
    url: "/eventsearch/displayeventserach",
    views: {
      'menuContent': {
        templateUrl: "views/app/eventsearch/displayeventserach.html",
        controller : 'programsearchctrl'

       
      }
    }
  })

   .state('app.detaileventlist', {
    url: "/eventsearch/detaileventlist/:EventListId",
    views: {
      'menuContent': {
        templateUrl: "views/app/eventsearch/detaileventlist.html",
        controller : 'detaileventlistctrl'

       
      }
    }
  })

//aboutus

.state('app.aboutus', {
    url: "/aboutus",
    views: {
      'menuContent': {
        templateUrl: "views/app/aboutus/aboutus.html",
        controller:'aboutusctrl'

       
      }
    }
  })

//contactus

.state('app.contactus', {
    url: "/contactus",
    views: {
      'menuContent': {
        templateUrl: "views/app/contactus/contactus.html",
        controller : 'contactusctrl'

       
      }
    }
  })
//termsconditions

.state('app.termsconditions', {
    url: "/termsconditions",
    views: {
      'menuContent': {
        templateUrl: "views/app/termsconditions/termsconditions.html",
        controller : 'termsconditionsctrl'

       
      }
    }
  })

//rateus

.state('app.rateus', {
    url: "/rateus",
    views: {
      'menuContent': {
        templateUrl: "views/app/rateus/rateus.html",
        controller :'ratectrl'

       
      }
    }
  })

//news
.state('app.News', {
    url: "/News",
    abstract: true,
    views:{
      'menuContent':{
        templateUrl: "views/app/News/newscontroller.html",
        controller : "areanewsctrl"
        
  
      }
    }
   
  })

 .state('app.News.feed-entries', {
    url: "/feed-entries/",
    views: {
      'tab2': {
        templateUrl: "views/app/News/feed-entries.html",
        controller: 'FeedEntriesCtrl'
      }
    }
  })


.state('app.News.area', {
    url: '/News/selectednewslist',
    views: {
       'tab3': {
         ///templateUrl: 'views/app/fooditems/tabsController.html',,
           
        templateUrl: 'views/app/News/selectednewslist.html',
        controller: 'areanewsctrl'
    

        
      }  
     
  }
})
  .state('app.News.others', {
    url: '/News/others',
    views: {
      'tab4': {
        templateUrl: 'views/app/News/others.html',
        controller: 'displaynewsctrl'
              
      }
    }
  })


  .state('app.searchnewslist', {
    url: "/News/searchnewslist/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/searchnewslist.html",
        controller: 'displaydetailsearchnews'
       
      }
    }
  })



  .state('app.category-feeds', {
    url: "/category-feeds/:categoryId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/category-feeds.html",
        controller: 'CategoryFeedsCtrl'
      }
    }
  })

  .state('app.feed-entries', {
    url: "/feed-entries/:categoryId/:sourceId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/feed-entries.html",
        controller: 'FeedEntriesCtrl'
      }
    }
  })

.state('app.displaydetailnews', {
    url: "/News/displaydetailnews/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/displaydetailnews.html",
        controller: 'displaydetailnewsctrl'
       
      }
    }
  })

.state('app.taluka', {
    url: "/News/taluka",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/taluka.html",
        controller:'displaynewsctrl'
      }
    }
  })

.state('app.newslist', {
    url: "/News/usernewslist",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/usernewslist.html",
        controller : 'newslistctrl'
      }
    }
  })

.state('app.detailusernews', {
    url: "/News/detailusernews/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/detailusernews.html",
        controller : 'detailusernewsctrl'
      }
    }
  })


.state('app.newsinclude', {
    url: "/News/newsinclude",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/newsinclude.html",
        controller:'insertnewsctrl'

      }
        
      }
    
  })

.state('app.newsaddcomment', {
    url: "/News/newsaddcomment/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/News/newsaddcomment.html",
        controller:'newsaddcommentctrl'
        
      }
    }
  })



//special news
.state('app.specialinfo', {
    url: "/specialinfo",
    abstract: true,
    views:{
      'menuContent':{
        templateUrl: "views/app/specialinfo/specialinfo.html",
        controller : "specialinfoctrl"
  
      }
    }
   
  })


.state('app.specialinfo.all', {
    url: '/specialinfo/all',
    views: {
       'tab3': {
       templateUrl: 'views/app/specialinfo/all.html',
       controller: 'specialareactrl',
        catche :false

       
      }  
     
  }
})
  .state('app.specialinfo.selected', {
    url: '/specialinfo/selectedspecialnews.html',
    views: {
      'tab4': {
        templateUrl: 'views/app/specialinfo/selectedspecialnews.html',
        controller: 'specialinfoctrl'
          
      }
    }
  })
  .state('app.specialinfo.area', {
    url: "/specialinfo/area",
    views: {
      'tab5': {
        templateUrl: "views/app/specialinfo/area.html",
        controller:'specialareactrl'
      }
    }
  })
.state('app.spcialnewsdetail', {
    url: "/specialinfo/spcialnewsdetails/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/specialinfo/spcialnewsdetails.html",
        controller:'spcialnewsdetailsctrl',
        cache:false
        
      }
    }
  })


.state('app.specialnewsaddcomment', {
    url: "/specialinfo/specialnewsaddcomment/:NewsId",
    views: {
      'menuContent': {
        templateUrl: "views/app/specialinfo/specialnewsaddcomment.html",
        controller:'spcialnewsdetailsctrl'
        
      }
    }
  })
  


// events
.state('app.events', {
    url: "/events",
    abstract: true,
    views:{
      'menuContent':{
        templateUrl: "views/app/events/events.html",
        controller :"eventctrl"
   
        }
    }
   
  })



.state('app.events.acceptedevent', {
    url: '/events/acceptedeventlist',
    views: {
       'tab1': {
         templateUrl: 'views/app/events/acceptedeventlist.html',
        controller:'eventctrl'
      
     }
       
      }  
})

.state('app.detailacceptedevent', {
    url: 'events/detailacceptedevent/:EventListId',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/detailacceptedevent.html',
        controller:'detailacceptedeventctrl'
      }  
   }
})

  .state('app.events.invitedevent', {
    url: '/events/invitedeventlist',
    views: {
      'tab2': {
        templateUrl: 'views/app/events/invitedeventlist.html',
        controller:'eventctrl'
      }
    }
  })


  .state('app.detailinvitedevents', {
    url: 'events/detailinvitedevents/:EventListId',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/detailinvitedevents.html',
        controller:'detailinvitedeventsctrl'
      }  
   }
})



  .state('app.events.allevent', {
    url: '/events/alleventlist',
    views: {
       'tab3': {
        templateUrl: 'views/app/events/alleventlist.html',
        controller:'eventctrl'
     }  
   }
})

.state('app.detailallevent', {
    url: 'events/detailallevent/:EventListId',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/detailallevent.html',
        controller:'detailalleventlistctrl'
      }  
   }
})


  .state('app.pastevents', {
    url: '/events/pastevents',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/pastevents.html',
        controller:'eventctrl'

      }  
   }
})
  .state('app.detailpastevent', {
    url: '/events/detailpastevent/:EventListId',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/detailpastevent.html',
        controller:'detailpasteventctrl'

      }  
   }
})

  .state('app.insertcustomevent', {
    url: '/events/insertcustomevent',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/insertcustomevent.html',
        controller:'insertcustomeventctrl'
     }  
  }
})


 

  .state('app.insertvillage', {
    url: '/events/insertvillage',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/insertvillage.html',
        controller:'insertvillagectrl'
     }  
  }
})

.state('app.usereventlist', {
    url: '/events/usereventlist',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/usereventlist.html',
        controller: 'usereventlistCtrl'
     }  
  }
})
  .state('app.usereventdetail', {
    url: '/events/usereventdetail/:EventListId',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/usereventdetail.html',
        controller: 'usereventdetailCtrl'
     }  
  }
})


  .state('app.yearswisecount', {
    url: '/events/yearswisecount',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/yearswisecount.html',
        controller: 'yearwisecountCtrl'
     }  
  }
})
  
   .state('app.msglistbdy', {
    url: '/events/msglistbdy',
    views: {
       'menuContent': {
        templateUrl: 'views/app/events/msglistbdy.html'
        
     }  
  }
})
// submit to admin
    /*.state('app.submittoadmin', {
    url: "/submittoadmin",
     views:{
      'menuContent':{
        templateUrl: "views/app/submittoadmin/submittoadmin.html",
        controller :'submittoadminctrl'
        }
    }
   
  })
*/
  //submit to admin
  .state('app.submittoadmin', {
    url: "/submittoadmin",
    abstract: true,
    views:{
      'menuContent':{
        templateUrl: "views/app/submittoadmin/submitcontroller.html",
        controller : "submittoadminctrl"
        
  
      }
    }
   
  })



  .state('app.submittoadmin.shareapp', {
    url: '/submittoadmin/shareapp',
    views: {
      'tab3': {
        templateUrl: 'views/app/submittoadmin/shareapp.html',
        controller: 'sharectrl'
              
      }
    }
  })

  .state('app.submittoadmin.submittoadmin', {
    url: '/submittoadmin/submittoadmin',
    views: {
       'tab4': {
         ///templateUrl: 'views/app/fooditems/tabsController.html',,
           
        templateUrl: 'views/app/submittoadmin/submittoadmin.html',
        controller: 'submittoadminctrl'
    

        
      }  
     
  }
})

   .state('app.submittoadmin.peoplelist', {
    url: '/submittoadmin/peoplelist',
    views: {
      'tab5': {
        templateUrl: 'views/app/submittoadmin/peoplelist.html',
        controller: 'peoplelistcountctrl'
              
      }
    }
  })

  //public notice
  .state('app.publicnotice', {
    url: "/publicnotice",
    abstract: true,
    views:{
      'menuContent':{
        templateUrl: "views/app/publicnotice/publicnotice.html",
        controller : "publicnoticectrl"
        
  
      }
    }
   
  })
.state('app.publicnotice.getnoticelist', {
    url: '/publicnotice/getnoticelist',
    views: {
      'tab2': {
        templateUrl: 'views/app/publicnotice/getnoticelist.html',
        controller: 'publicnoticectrl'
              
      }
    }
  })

  .state('app.detailnoticelist', {
    url: "/publicnotice/detailnoticelist/:NoticeId",
    views: {
      'menuContent': {
        templateUrl: "views/app/publicnotice/detailnoticelist.html",
        controller:'detailnoticelistctrl'
      }
    }
  })

  .state('app.publicnotice.searchnotice', {
    url: '/publicnotice/searchnotice',
    views: {
      'tab3': {
        templateUrl: 'views/app/publicnotice/searchnotice.html',
        controller: 'searchnoticectrl'
              
      }
    }
  })
  .state('app.publicnotice.Insertnotice', {
    url: '/publicnotice/Insertnotice',
    views: {
       
      'tab4':{
        templateUrl: 'views/app/publicnotice/Insertnotice.html',
        controller: 'insertnoticectrl'
      
      }  
     
  }
})


  .state('app.detailsearchlist', {
    url: "/publicnotice/detailsearchlist/:NoticeId",
    views: {
      'menuContent': {
        templateUrl: "views/app/publicnotice/detailsearchlist.html",
        controller:'detailsearchlist'
      }
    }
  })


  .state('app.insertvillagepublicnotice', {
    url: '/publicnotice/insertvillagepublicnotice',
    views: {
       
      'menuContent':{
        templateUrl: 'views/app/publicnotice/insertvillagepublicnotice.html',
        controller: 'insertnoticectrl'
      
      }  
     
  }
})
  //['app.publicnotice','app.publicnotice.getnoticelist','app.detailnoticelist','app.publicnotice.searchnotice','app.publicnotice.Insertnotice','app.detailsearchlist','app.insertvillagepublicnotice','app.userpublicnotice','app.detailusernoticelist']


  .state('app.userpublicnotice', {
    url: "/publicnotice/usernoticelist",
    views: {
      'menuContent': {
        templateUrl: "views/app/publicnotice/usernoticelist.html",
        controller:'displayusernoticectrl'
      }
    }
  })

  .state('app.detailusernoticelist', {
    url: "/publicnotice/detailusernoticelist/:NoticeId",
    views: {
      'menuContent': {
        templateUrl: "views/app/publicnotice/detailusernoticelist.html",
        controller:'detailusernoticelist'
      }
    }
  })

//profile
  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "views/app/profile.html",
        controller:'profilectrl',
        cache :false
      }
    }
  })
 
;
 $httpProvider.interceptors.push('APIInterceptor');
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/auth/walkthrough');
});
