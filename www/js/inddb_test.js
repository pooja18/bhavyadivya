var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

// Open (or create) the database
var idop = indexedDB.open("testDatabase2", 11);
var dbShouldInit = false;
// Create the schema
idop.onupgradeneeded = function() {
    var db = idop.result;
    dbShouldInit = true;
    if(db.objectStoreNames.contains("Village")) { db.deleteObjectStore("Village");}
    //if(db.objectStoreNames.contains("GeoData1")) var store = db.createObjectStore("GeoData1", {keyPath: "id"});
    if(db.objectStoreNames.contains("District")) { db.deleteObjectStore("District");}
    if(db.objectStoreNames.contains("Taluka")) { db.deleteObjectStore("Taluka");}
     if(db.objectStoreNames.contains("GeoData1")) { db.deleteObjectStore("GeoData1");}
    var store1 = db.createObjectStore("Village", {keyPath: "VillageId"});
    var store2 = db.createObjectStore("District", {keyPath: "DistrictId"});
    var store3 = db.createObjectStore("Taluka", {keyPath: "TalukaId"});
    var index1 = store1.createIndex("TalukaIndex", "TalukaId", { unique: false,multiEntry: true });
    var index2 = store2.createIndex("StateIndex", "StateId", { unique: false,multiEntry: true });
    var index3 = store3.createIndex("DistrictIndex", "DistrictId", { unique: false,multiEntry: true });
    dbShouldInit = true;
};

function db_populate() {
  init_store1(init_store2,init_store3); //pass init 2 as callback 
}

function init_store1(callback,cb1) { 
    
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
               // Typical action to be performed when the document is ready:
               var db = idop.result;
                var tx = db.transaction(["District"], "readwrite");
                var store = tx.objectStore("District");
                JSON.parse(xhttp.responseText).forEach(function(item){
                    store.add(item);
                });
                tx.oncomplete=function (){
                    callback(cb1);
                };
            }
        };
        xhttp.open("GET", "js/district.json", true);
        xhttp.send();
    
   }

function init_store2(callback) { 
    
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
               // Typical action to be performed when the document is ready:
               var db = idop.result;
                var tx = db.transaction(["Taluka"], "readwrite");
                var store = tx.objectStore("Taluka");
                JSON.parse(xhttp.responseText).forEach(function(item){
                    store.add(item);
                });
                tx.oncomplete=function (){
                        callback();
                };
            }
        };
        xhttp.open("GET", "js/taluka..json", true);
        xhttp.send();
    
}

function init_store3() {
    
   
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
               // Typical action to be performed when the document is ready:
               var db = idop.result;
                var tx = db.transaction(["Village"], "readwrite");
                var store = tx.objectStore("Village");
                JSON.parse(xhttp.responseText).forEach(function(item){
                    store.add(item);
                });

            }
        };
        xhttp.open("GET", "js/villagedata.json", true);
        xhttp.send();
    
}

idop.onsuccess = function() {
    // Start a new transaction
    if(dbShouldInit) db_populate();
    /*var db = open.result;
    var tx = db.transaction(["GeoData1","Village"], "readwrite");
    var store = tx.objectStore("GeoData1");
    //var txV = db.transaction("Village", "readwrite");
    var storeV = tx.objectStore("Village");
    // Add some data
    

    tempvillagedata.forEach(function(item){
        storeV.put(item);
    });*/
    //store.put({id:54321,  district:tempdistrict});
    
    // Query the data
    /*var getJohn = store.get(12345);
   // var getBob = index.get(["Smith", "Bob"]);

    getJohn.onsuccess = function() {
        console.log(getJohn.result.name.first);  // => "John"
    };*/

   /* getBob.onsuccess = function() {
        console.log(getBob.result.name.first);   // => "Bob"
    };*/

    // Close the db when the transaction is done
    /*tx.oncomplete = function() {
        db.close();
    };*/
    
};