angular.module('bhavydivya.factories', [])

.factory('FeedLoader', function ($resource){
  return $resource('http://ajax.googleapis.com/ajax/services/feed/load', {}, {
    fetch: { method: 'JSONP', params: {v: '1.0', callback: 'JSON_CALLBACK'} }
  });
})


// Factory for node-pushserver (running locally in this case), if you are using other push notifications server you need to change this
.factory('NodePushServer', function ($http){
  // Configure push notifications server address
  // 		- If you are running a local push notifications server you can test this by setting the local IP (on mac run: ipconfig getifaddr en1)
  var push_server_address = "http://192.168.1.102:8000";

  return {
    // Stores the device token in a db using node-pushserver
    // type:  Platform type (ios, android etc)
    storeDeviceToken: function(type, regId) {
      // Create a random userid to store with it
      var user = {
        user: 'user' + Math.floor((Math.random() * 10000000) + 1),
        type: type,
        token: regId
      };
      console.log("Post token for registered device with data " + JSON.stringify(user));

      $http.post(push_server_address+'/subscribe', JSON.stringify(user))
      .success(function (data, status) {
        console.log("Token stored, device is successfully subscribed to receive push notifications.");
      })
      .error(function (data, status) {
        console.log("Error storing device token." + data + " " + status);
      });
    },
   
    removeDeviceToken: function(token) {
      var tkn = {"token": token};
      $http.post(push_server_address+'/unsubscribe', JSON.stringify(tkn))
      .success(function (data, status) {
        console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
      })
      .error(function (data, status) {
        console.log("Error removing device token." + data + " " + status);
      });
    }
  };
})


.factory('AdMob', function ($window){
  var admob;
  var admobid = {};
function init(){
   admob = $window.AdMob;

  if(admob)
  {
   // alert("inside first AdMob");
    // Register AdMob events
    // new events, with variable to differentiate: adNetwork, adType, adEvent
    document.addEventListener('onAdFailLoad', function(data){
      console.log('error: ' + data.error +
      ', reason: ' + data.reason +
      ', adNetwork:' + data.adNetwork +
      ', adType:' + data.adType +
      ', adEvent:' + data.adEvent); // adType: 'banner' or 'interstitial'
    });
    document.addEventListener('onAdLoaded', function(data){
      console.log('onAdLoaded: ' + data);
    });
    document.addEventListener('onAdPresent', function(data){
      console.log('onAdPresent: ' + data);
    });
    document.addEventListener('onAdLeaveApp', function(data){
      console.log('onAdLeaveApp: ' + data);
    });
    document.addEventListener('onAdDismiss', function(data){
      console.log('onAdDismiss: ' + data);
    });

    var defaultOptions = {
      // bannerId: admobid.banner,
      // interstitialId: admobid.interstitial,
      // adSize: 'SMART_BANNER',
      // width: integer, // valid when set adSize 'CUSTOM'
      // height: integer, // valid when set adSize 'CUSTOM'
      position: admob.AD_POSITION.BOTTOM_CENTER,
      // offsetTopBar: false, // avoid overlapped by status bar, for iOS7+
      bgColor: 'black', // color name, or '#RRGGBB'
      // x: integer,		// valid when set position to 0 / POS_XY
      // y: integer,		// valid when set position to 0 / POS_XY
      isTesting: false, // set to true, to receiving test ad for testing purpose
      // autoShow: true // auto show interstitial ad when loaded, set to false if prepare/show
    };
    

    if(ionic.Platform.isAndroid())
    {
      admobid = { // for Android
        banner:       'ca-app-pub-2696266532493197/9800104268'
      };
    }

    if(ionic.Platform.isIOS())
    {
      admobid = { // for iOS
       /* banner: 'ca-app-pub-6869992474017983/4806197152',
        interstitial: 'ca-app-pub-6869992474017983/7563979554'*/
      };
    }

    admob.setOptions(defaultOptions);

  	//- (for example at the beginning of a game level)
    
  }
  else
  {
   // alert("no AdMob");
    console.log("No AdMob?");
  }
} 
init();
  return {
    showBanner: function(bid) {
      init();
      if(admob)
      { //alert("inside showbanner AdMob");

        if(bid) admobid.banner=bid;

        admob.createBanner({
          
          adId:admobid.banner,
          position:admob.AD_POSITION.BOTTOM_CENTER,
         
          autoShow:true,
          success: function(){
            console.log('banner created');
            //alert("inside success of showBanner " + admobid.banner);
          },
          error: function(){
            console.log('failed to create banner');
            //alert("inside error showBanner " + bid);
          }
        });
      }
    },
    removeAds: function() {
      init();
      if(admob)
      {
        admob.removeBanner();
      }
    }
  };
})

.factory('iAd', function ($window){
  var iAd = $window.iAd;

  // preppare and load ad resource in background, e.g. at begining of game level
  if(iAd) {
    iAd.prepareInterstitial( { autoShow:false } );
  }
  else
  {
    console.log("No iAd?");
  }

  return {
    showBanner: function() {
      if(iAd)
      {
        // show a default banner at bottom
        iAd.createBanner({
          position:iAd.AD_POSITION.BOTTOM_CENTER,
          autoShow:true
        });
      }
    },
    showInterstitial: function() {
      // ** Notice: iAd interstitial Ad only supports iPad.
      if(iAd)
      {
        // If you did prepare it before, then show it like this
        // 		- (for example: check and show it at end of a game level)
        iAd.showInterstitial();
      }
    },
    removeAds: function() {
      if(iAd)
      {
        iAd.removeBanner();
      }
    }
  };
})
.factory("popup", function ($ionicPopup) {

   function getConfirmPopup(params, callback) {
     $ionicPopup.show({
       template: params.template,
       title: params.title,
       subTitle: params.subtitle,
       buttons: [
         {
           text: 'Cancel',
           onTap: function(e) {
             callback(false);
           }
          },
         {
           text: '<b>Confirm</b>',
           type: 'button-positive',
           onTap: function(e) {
             callback(true);
           }
         },
       ]
     });
   }

   function getAlertPopup(params, callback) {
     $ionicPopup.alert({
       template: params.template,
       title: params.title,
       buttons: [
         {
           type: 'button-positive',
           text: 'Ok',
           onTap: function(e) {
             if(callback) callback(true);
             else return;
           }
         }
       ]
     });
   }
   return {
       getConfirmPopup: getConfirmPopup,
       getAlertPopup: getAlertPopup
   };

   })


.factory('Service', function($http,$q,$window) {
 var RestAPIURL='https://bhavyadivya.com/BDService.svc';

      var getRestAPIURL = function(){
        return RestAPIURL;
      };
          return { 
                  getRestAPIURL : getRestAPIURL,
                };


    })
;
