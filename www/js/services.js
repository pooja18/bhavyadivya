angular.module('bhavydivya.services', [])

.service('FeedList', function ($rootScope, FeedLoader, $q){
	this.get = function(feedSourceUrl) {
		var response = $q.defer();
		//num is the number of results to pull form the source
		FeedLoader.fetch({q: feedSourceUrl, num: 20}, {}, function (data){
			response.resolve(data.responseData);
		});
		return response.promise;
	};
})

.service('APIInterceptor', function($rootScope, $injector) {
  var service = this;
//var base_url='http://bdserviceajax.bhavyadivya.com/BDServiceAjax.svc';
var base_url='https://bhavyadivya.com/BDService.svc';
  service.request = function(config) {

    if (config.url.substr(config.url.length - 5) == '.html') {
      //console.log(config.url);
    } /*else if (store.get('popety')) {
      $injector.get("$ionicLoading").show({
        template:'Loading....'
      });
      var token = store.get('popety');
      config.headers.authorization = token.authToken;
    }*/else {
    	console.log(config.url);
      $injector.get("$ionicLoading").show({
        template:'प्रतिक्षा करा'
      });
    }

    return config;
  };

  service.response = function(response) {
  	
  	 if(response.config.url.substr(response.config.url.length - 5) == '.html'){
      // console.log(config.url);
    }else {
      $injector.get("$ionicLoading").hide();
     
    }

    return response;
    
  };

  service.responseError = function(response) {
  	 //alert("errorresponse" + angular.toJson(response));
    var popupParams={};
    $injector.get("$ionicLoading").hide();

    if(response.config.url.substr(response.config.url.length - 5) == '.html'){
      // console.log(config.url);
      //return response;
    }else if(response.config.url.indexOf(base_url + "auth/") >= 0){
      return response;
     
    }

    if(response.status === 500){
      popupParams = {template: "थोड्या वेळाने पुन्हा प्रयत्न करा."};
    }else if(response.status === 404){
      popupParams = {template: "माहिती आढळली नाही, कृपया पुन्हा प्रयत्न करा."};
    }else if (response.status === 401) {
      popupParams.template = "आपण वापरकर्ता अधिकृत नाही. पुन्हा लॉगिन करा.";
    }else if (response.status === 403) {
      popupParams.template = "चुकीचा पर्याय. पुन्हा प्रयत्न करा.";
    }else if (response.status === 422) {
      popupParams.template = "खराब विनंती.  पुन्हा प्रयत्न करा.";
    }else {
      return response;
      
    }
    $injector.get('popup').getAlertPopup(popupParams);
  };
})

.service('SocketService', ['socketFactory',function (socketFactory){
        return socketFactory({

            ioSocket: io.connect('http://50.62.135.247:24000/', {reconnection: false})

        });
    }])

// PUSH NOTIFICATIONS
.service('PushNotificationsService', function ($rootScope,RequestsService, $cordovaPush, NodePushServer, GCM_SENDER_ID){
	/* Apple recommends you register your application for push notifications on the device every time it’s run since tokens can change. The documentation says: ‘By requesting the device token and passing it to the provider every time your application launches, you help to ensure that the provider has the current token for the device. If a user restores a backup to a device other than the one that the backup was created for (for example, the user migrates data to a new device), he or she must launch the application at least once for it to receive notifications again. If the user restores backup data to a new device or reinstalls the operating system, the device token changes. Moreover, never cache a device token and give that to your provider; always get the token from the system whenever you need it.’ */
	this.register = function() {
		var config = {};

		// ANDROID PUSH NOTIFICATIONS
		if(ionic.Platform.isAndroid())
		{
			config = {
				"senderID": GCM_SENDER_ID
			};
			RequestsService.register();
			$cordovaPush.register(config).then(function(result) {
				// Success
				console.log("$cordovaPush.register Success");
				console.log(result);
			}, function(err) {
				// Error
				console.log("$cordovaPush.register Error");
				console.log(err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(JSON.stringify([notification]));
				switch(notification.event)
				{
					case 'registered':
						if (notification.regid.length > 0 ) {
							RequestsService.register(notification.regid);
							console.log('registration ID = ' + notification.regid);
							NodePushServer.storeDeviceToken("android", notification.regid);
						}
						break;

					case 'message':
						if(notification.foreground == "1")
						{
							console.log("Notification received when app was opened (foreground = true)");
						}
						else
						{
							if(notification.coldstart == "1")
							{
								console.log("Notification received when app was closed (not even in background, foreground = false, coldstart = true)");
							}
							else
							{
								console.log("Notification received when app was in background (started but not focused, foreground = false, coldstart = false)");
							}
						}

						// this is the actual push notification. its format depends on the data model from the push server
						console.log('message = ' + notification.message);
						break;

					case 'error':
						console.log('GCM error = ' + notification.msg);
						break;

					default:
						console.log('An unknown GCM event has occurred');
						break;
				}
			});

			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
			//   // Success!
			// }, function(err) {
			//   // Error
			// });
		}

		if(ionic.Platform.isIOS())
		{
			config = {
				"badge": true,
				"sound": true,
				"alert": true
			};

			$cordovaPush.register(config).then(function(result) {
				// Success -- send deviceToken to server, and store for future use
				console.log("result: " + result);
				NodePushServer.storeDeviceToken("ios", result);
			}, function(err) {
				console.log("Registration error: " + err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(notification.alert, "Push Notification Received");
			});
		}
	};
})

.service('RequestsService', function($http, $q,base_url, $ionicLoading,$window){
		//for server
		//var base_url = 'your-server.com';

		function register(device_token,oneSignalDeviceId){
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

//console.log("length====================================================================================="+  device_token.length);
			var deferred = $q.defer();
			//$ionicLoading.show();

			$http({method  : 'GET',
          url     : base_url+"/UpdateMobileDeviceId",
          headers: {'Content-Type': 'application/json'},
          params : {'userId' : USERID, 'mobileDeviceId':device_token,'oneSignalDeviceId':oneSignalDeviceId,"webServicePassword" : "21@bhavyadivya"}       
         })
			/*$http.get(base_url+"" + '/UpdateMobileDeviceId', 
				params:{'userId' : 49, 'mobileDeviceId':device_token, 'webServicePassword': "21@bhavyadivya"})*/
				
				.success(function(response){
					
					//$ionicLoading.hide();
					deferred.resolve(response);
					
				})
				.error(function(data){
					deferred.reject();	
				});
			

			return deferred.promise;			

		}


		return {
			register: register
		};
	})

.service('AuthService', function ($rootScope, $http,base_url, $q,$timeout,$window,$stateParams){

//var base_url='http://54.202.70.15/BDServiceAjax.svc';//'http://bdserviceajax.bhavyadivya.com/BDServiceAjax.svc';

this.submitOTP=function(otp,GUId){
  var deferred = $q.defer();
 
  /*	var GUIID = angular.fromJson($window.localStorage.localuserdata)[0].GUID;*/
   var USERID = angular.fromJson($window.localStorage.localuserdata)[0].UserId;
   
  	var req={method: 'GET', 
  		url: base_url +'/VerifyOTP',
        headers: {'Content-Type': 'application/json'},
        params:{"userId" : USERID, OTP:otp, GUId:GUId,"webServicePassword":"21@bhavyadivya"}
       };
        //console.log(req);
     $http(req).success(function (res) {
             deferred.resolve(res);
          }).error( function(data){deferred.reject(data);
           deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
 };
 this.resendOTP=function(mobile){
  var deferred = $q.defer();

console.log(mobile);
        //var data = $rootScope.globals.currentUser.username;
       /* var userOTP={mobile:data};*/
       /* var req={method: 'GET', url: 'http://bdserviceajax.bhavyadivya.com/BDServiceAjax.svc'+'/GenerateOTP',
        headers: {'Content-Type': 'application/json',data:mobile}};*/
        var req={method: 'GET', 
		        url: base_url +'/GenerateOTP',
		        headers: {'Content-Type': 'application/json'},
		        params:{"webServicePassword":"21@bhavyadivya","mobileNo" : mobile}
       };
     	$http(req).success(function (res) {
             deferred.resolve(res);
          }).error( function(data){deferred.reject(data);
           deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
 };

 this.doLogin=function (data){
 
	var deferred = $q.defer();
           //  $timeout(function () {
	//$http.get("http://www.rssmix.com/u/8216509/rss.json").success(function(d){ alert(JSON.stringify(d));}).error(function(r){ alert("error hello");})
   $http({method : 'GET',
         url     : base_url + '/GetUserLoginInfoNew',
         headers : {'Content-Type': 'application/json'},
         params  :  {userName: data.userName, userPassword : data.userPassword, webServicePassword: '21@bhavyadivya'},
         }).success(function (res) {
       console.log(angular.toJson(res));
                             var user1=angular.fromJson(res);
                             //alert(angular.toJson(user1.isVerified));
                             $window.localStorage.currentUser = angular.toJson(user1.Response);
                             
                     		
                     
 if (res && res.length>0 &&user1.Response[0].ReturnCode == -3 ) 
								{
							// if (user1 !== null && user1.passMD5 == encodedPassword) {
						       response = { 
											success: false, IsVerified :user1.Response[0].IsVerified,code:user1.Response[0].ReturnCode,
											message:"तुम्ही भरलेल्या मोबाईल नं. चे अकाऊंट अस्तित्वात नाही कृपया साइन अप करा "
										  };
						       		console.log("success responce"+angular.toJson(response));
				                                                                          
				                }            

else if (res && res.length>0 &&user1.Response[0].ReturnCode != -1 ) 
				{
			// if (user1 !== null && user1.passMD5 == encodedPassword) {
		       response = { 
							success: true, IsVerified :user1.Response[0].IsVerified,code:user1.Response[0].ReturnCode
						  };
		       		console.log("success responce"+angular.toJson(response));
                                                                          
                }
				

			     else {
			                            response = { success: false, message: 'युजरनेम किंवा पासवर्ड चुकीचा आहे' };
			       console.log(" false response  " + angular.toJson(response));
			                        }
			      deferred.resolve(response);
                       // callback(response);
                    }).error( function(data){deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
 };

this.checkusername=function (data){
 
	var deferred = $q.defer();
	$http({method : 'GET',
         url     : base_url + '/VerifyUserName',
         headers : {'Content-Type': 'application/json'},
         params  :  {userName: data.userName, webServicePassword: '21@bhavyadivya'},
         }).success(function (res) {
       console.log(angular.toJson(res));
                             var user1=angular.fromJson(res);
                             $window.localStorage.localuserdata= angular.toJson(user1.Response);
                            console.log("data ======"+data);
                     
            if (res && res.length>0 &&user1.Response != -1 ) 
				{
			response = { 
							success: true
						  };
		       		console.log("success responce"+angular.toJson(response));
                                                                          
                } else {
                            response = { success: false, message: 'युजरनेम चुकीचा आहे' };
       console.log(" false response  " + angular.toJson(response));
                        }
      deferred.resolve(res);
                    }).error( function(data){deferred.reject(data);});
         return deferred.promise;
 };
//verify otp for forgot password
this.submitOTPforpass=function(otp){
  var deferred = $q.defer();
  var req={method: 'GET', 
	  		url: base_url+'/GenerateOTP',
	        headers: {'Content-Type': 'application/json'},
	        params:{"webServicePassword":"21@bhavyadivya"}
       };
     	$http(req).success(function (res) {
             deferred.resolve(res);
          }).error( function(data){deferred.reject(data);
           deferred.reject(data);});
          return deferred.promise;
 };
 //changePassword
	this.changePassword =function (resetpassworddata) {
		var newpass=angular.fromJson(resetpassworddata).newpassword;
		 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var oldpassword = angular.fromJson(resetpassworddata).originalPassword;
	  	var deferred = $q.defer();
         $http({method : 'GET',
         url     : base_url + '/ResetPassword',
         headers : {'Content-Type': 'application/json'},
         params  :  {userId: USERID, oldPassword :oldpassword, newPassword : newpass, webServicePassword: '21@bhavyadivya'},
         }).success(function (res) {
						        var user1=angular.toJson(res);
						       deferred.resolve(res);
						    }).error( function(data){
							deferred.reject(data);
						    	});
           return deferred.promise;
	};

	this.changePasswordSMS =function (userid) {
		
		
	  	var deferred = $q.defer();
         $http({method : 'GET',
         url     : base_url + '/ReqForPswSMS',
         headers : {'Content-Type': 'application/json'},
         params  :  {userName: userid, webServicePassword: '21@bhavyadivya'},
         }).success(function (res) {
						       // var user1=angular.toJson(res);
						       deferred.resolve(res);
						    }).error( function(data){
							deferred.reject(data);
						    	});
           return deferred.promise;
	};

	this.uploadFileToUrl = function( uploadUrl,DataForm){
            	console.log("inside file upload");
               var fd = new FormData();
          for (var key in DataForm) {
          	 
          	fd.append(key,DataForm[key]);
          }
				  $http.post(uploadUrl, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
            
               .success(function(){
               })
            
               .error(function(){
               	alert("error");
               });
            };

    //special news

    this.specialnewsarea =function () {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		//$http.get('./js/area.json')
		 $http({method  : 'GET',
          url     : base_url+"/GetInfo",
          headers: {'Content-Type': 'application/json'},
          params:{ "webServicePassword" : "21@bhavyadivya"}       
         }).success(function (res) {
				 var infodata={}; 
				 infodata.Response=[];
						         infodata=angular.toJson(res);
						        
						        deferred.resolve(res);
						       /* angular.forEach(res,function(f)
						        {
						        	
						        	infodata.Response.push(f);


						        });
						        console.log("inside news area data : " + new Date());
						        deferred.resolve(angular.toJson(infodata));*/
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};


	this.specialnewsareavibhag =function (infoid) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		/*$http.get('./js/vibhag.json')*/
		 $http({method  : 'GET',
              url     : base_url+"/GetInfo1",
              headers: {'Content-Type': 'application/json'}   ,
              params:{"infoId": infoid, "webServicePassword" : "21@bhavyadivya"}       
             }).success(function (res) {
						        var infodata1=angular.toJson(res);
						         deferred.resolve(res);
						       /* var infodata1={};
						        infodata1.Response=[];
						        angular.forEach(res,function(f)
						        {
						        	if(f.InfoId==infoid)
						        	infodata1.Response.push(f);


						        });
						        
						        deferred.resolve(angular.toJson(infodata1));*/
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.specialnewsareaupvibhagdata =function (infoid1) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       // $http.get('./js/upvibhag.json')
 $http({method  : 'GET',
            url     : base_url+"/GetInfo2",
            headers: {'Content-Type': 'application/json'}   ,
            params:{"InfoType1Id": infoid1, "webServicePassword" : "21@bhavyadivya"}       
           }).success(function (res) {
								
								 var infodata2=angular.toJson(res);
						        deferred.resolve(res);
						        /* var infodata2={};
						        infodata2.Response=[];
						         //infodata2=angular.toJson(res);
						       
						        angular.forEach(res,function(f)
						        {
						        	if(f.InfoType1Id==infoid1)
						        	infodata2.Response.push(f);


						        });
						        deferred.resolve(angular.toJson(infodata2));*/
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.specialinsertarea =function (areaId, vibhagid, upvibhagid) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http({method  : 'GET',
             url     : base_url + '/InsertSpecialNewsFilters',
             headers: {'Content-Type': 'application/json'}, 
  		       params:{ "userId" : USERID ,"infoId" : areaId, "InfoType1Id":vibhagid, "InfoType2Id":upvibhagid , "webServicePassword" : "21@bhavyadivya"}
   			}).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.specialdeletearea =function (infoid) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http({method  : 'GET',
	          url     : base_url + '/DeleteSpecialNewsFilter',
	          headers: {'Content-Type': 'application/json'}, 
	  		    params:{ "newsFilterId" : infoid ,"webServicePassword": "21@bhavyadivya"}
	   			}).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.specialfetcharea =function (infoid1) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
                url     : base_url+'/GetSpecialNewsFilters',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
              }).success(function (res) {
						        var allarea=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchspecialnews =function () {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test= new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-5);
		var deferred = $q.defer();
         $http({ method  : 'GET',
                url     : base_url + '/GetSpeacialNews',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"userId" : USERID,"newsArea":"selected","fromDay": prevdate.getDate(),
                "fromMonth":prevdate.getMonth()+1,"fromYear": prevdate.getFullYear()-1,
                "toDay": test.getDate(),"toMonth":  test.getMonth()+1,"toYear": test.getFullYear(),"infoId":"",
                "InfoType1Id": "", "InfoType2Id":"",
                "webServicePassword":"21@bhavyadivya"}
          	 }).success(function (res) {
						        var specialnwsdetails=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};
	this.fetchselctedspecialnews =function (areaId, vibhagid, upvibhagid) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test= new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-5);
		var deferred = $q.defer();
         $http({ method  : 'GET',
                url     : base_url + '/GetSpeacialNews',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"userId" : USERID,"newsArea":"bysearch","fromDay": prevdate.getDate(),
                "fromMonth":prevdate.getMonth()+1,"fromYear": prevdate.getFullYear()-1,
                "toDay": test.getDate(),"toMonth":  test.getMonth()+1,"toYear": test.getFullYear(),"infoId":areaId,
                "InfoType1Id": vibhagid, "InfoType2Id":upvibhagid,
                "webServicePassword":"21@bhavyadivya"}
          	 }).success(function (res) {
						        var specialnwsdetail=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchspecialnewsdetails =function (id) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetSpecialNewsDetails',
                headers: { 'Content-Type': 'application/json'}, 
               params: {"newsId":id,"UserId" : USERID,"webServicePassword":"21@bhavyadivya"}
                 }).success(function (res) {
						        var specialnwsdetail=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};
	this.fetchspecialnewsfiledetails =function (id) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
         $http({method  : 'GET',
		          url     : base_url + '/GetSpecialNewsFiles',
		          headers: {'Content-Type': 'application/json'},
		          params : {"newsId":id,"webServicePassword" : "21@bhavyadivya"}       
         }).success(function (res) {
						        var filesdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.insertspecialcommentser =function (spcomment) {
		var id = $stateParams.NewsId;
  		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
        url     : base_url + '/InsertSpecialNewsComment',
        headers: {'Content-Type': 'application/json'}, 
        params:{ "newsId" : id, "userId" : USERID ,"comment" : spcomment,
       "webServicePassword" : "21@bhavyadivya"}
        }).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.deletespecialcomment =function (CommentId) {
  		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
                url     : base_url  + '/DeleteSpecialNewsComments',
                headers: {'Content-Type': 'application/json'}, 
                 params:{ "commentId" : CommentId ,"webServicePassword": "21@bhavyadivya"}
                }).success(function (res) {
                				var deletespcomdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchspecialnewscomment =function (id) {
		 var NEWSID = id;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetSpecialNewsComment',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "newsId" : NEWSID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var specialnwsdetails=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.insertcustomnewsdata =function (stateid, districtid, talukaid,description,title) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/InsertNews',
                headers: { 'Content-Type': 'application/json'}, 
               params:{"userId":USERID,"newsTitle":title,"newsDescription":description,
                   "stateIds":stateid,"districtIds":districtid,"talukaIds":talukaid,"isAllArea":"","webServicePassword" : "21@bhavyadivya"}
                 }).success(function (res) {
						        var customnewsdata=angular.toJson(res);
						        
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.insertnewsimages  =function (newsid,image1,image2) {
	
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http.post(base_url+"/SaveNewsImages",
        JSON.stringify({"userId":USERID,"newsID": newsid,"imageStr1": image1, "imageStr2":image2}))
			.success(function (res) {
						        var imagesdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	

	this.fetchingnews =function () {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test = new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-5);
        
		var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNews',
                headers: { 'Content-Type': 'application/json'}, 
               params: {"UserId" : USERID,"newArea":"selected","fromDay": prevdate.getDate(),"fromMonth":prevdate.getMonth()+1,
                         "fromYear": prevdate.getFullYear(),"toDay": test.getDate(),"toMonth":  test.getMonth()+1,
                         "toYear": test.getFullYear(),"stateId":"","districtIdList":"","talukaIdList":"",
                          "webServicePassword":"21@bhavyadivya"}
                 }).success(function (res) {
						        var newsdata=angular.toJson(res);
						    /*    alert(newsdata);*/
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchingselectednews =function (stateid,districtid,talukaid) {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test = new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-8);
		var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNews',
                headers: { 'Content-Type': 'application/json'}, 
               params: {"UserId" : USERID,"newArea":"bysearch","fromDay": prevdate.getDate(),"fromMonth":prevdate.getMonth()+1,
                         "fromYear": prevdate.getFullYear(),"toDay": test.getDate(),"toMonth":  test.getMonth()+1,
                         "toYear": test.getFullYear(),"stateId":stateid,"districtIdList":districtid,"talukaIdList":talukaid,
                          "webServicePassword":"21@bhavyadivya"}
                 }).success(function (res) {
						        var selectednewsdata=angular.toJson(res);
						        //alert(selectednewsdata);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchnewsdetails =function (id) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test = new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-5);
		var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNewsDetails',
                headers: { 'Content-Type': 'application/json'}, 
               params: {"newsId":id,"UserId" : USERID,"webServicePassword":"21@bhavyadivya"}
                 }).success(function (res) {
						        var newsdetail=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchtaggedarea =function (id) {
		var deferred = $q.defer();
		 $http({method  : 'GET',
            url     : base_url + '/GetTaggedAreaForNews',
            headers: {'Content-Type': 'application/json'}, 
           	params:{ "newsId" : id,"webServicePassword" : "21@bhavyadivya"}
        		}).success(function (res) {
						        var taggedareafornews=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};

	this.newsobjection =function (id) {
		 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		 $http({method  : 'GET',
            url     : base_url + '/ObjectionOnNews',
            headers: {'Content-Type': 'application/json'}, 
           params:{ "newsId" : id, "userId" : USERID ,"webServicePassword" : "21@bhavyadivya"}
        	}).success(function (res) {
						        var objdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};

	this.insertnewscomment =function (newscomment) {
		var id = $stateParams.NewsId;
  		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
          url     : base_url + '/InsertNewsComment',
          headers: {'Content-Type': 'application/json'}, 
         params:{ "newsId" : id, "userId" : USERID ,"comment" : newscomment,"webServicePassword" : "21@bhavyadivya"}
        }).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};
	this.deletetnewscomment =function (CommentId) {
		var deferred = $q.defer();
        $http({method  : 'GET',
             url     : base_url + '/DeleteNewsComments',
             headers: {'Content-Type': 'application/json'}, 
           params:{ "commentId" : CommentId ,"webServicePassword": "21@bhavyadivya"}
        }).success(function (res) {
        	  					var deletenewscommentdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetcnewscomments =function (id) {
		var deferred = $q.defer();
		 $http({
                method  : 'GET',
                url     : base_url + '/GetNewsComment',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "newsId" : id,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var taggedareafornews=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};

	this.fetchusernewslist =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test = new Date();
        var prevdate = new Date();
        prevdate.setDate(prevdate.getDate()-5);
		var deferred = $q.defer();
		 $http({
                method  : 'GET',
                url     : base_url + '/GetMyNews',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"UserId" : USERID,"fromDay": prevdate.getDate(),"fromMonth":prevdate.getMonth()+1,"fromYear": prevdate.getFullYear(),
                "toDay": test.getDate(),"toMonth":  test.getMonth()+1,"toYear": test.getFullYear(),
                "webServicePassword":"21@bhavyadivya"}
                }).success(function (res) {
						        var newslistdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};

	this.deleteusernews =function (newsid) {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http({method  : 'GET',
           url     : base_url + '/DeleteMyNews',
           headers: {'Content-Type': 'application/json'}, 
            params:{ "newsId" :newsid ,"webServicePassword": "21@bhavyadivya"}
          }).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchstate =function () {
		
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		$http.get('./js/state.json')
		.success(function (res) {
						       // var statedata=angular.toJson(res);
						        var statedata={};
						        statedata.Response=[];
						        angular.forEach(res,function(a)
						        {
						     
						        	statedata.Response.push(a);

								 });
						        deferred.resolve(angular.toJson(statedata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchdistrict =function (stateId) {
		
		/*var deferred = $q.defer();
        
	    $http.get('./js/district.json')       
         	.success(function (res) {

						        //var districtdata=angular.toJson(res);
						        var localdistrict;
						        if(localStorage.district)  localdistrict=JSON.parse(localStorage.district);
						        if(localdistrict.length > 0) res=localdistrict;
						        var distrcit={};
						        distrcit.Response=[];
						        angular.forEach(res,function(a)
						        {
						        	
						        	if(a.StateId==stateId)
						        		distrcit.Response.push(a);

								 });
						        
						        deferred.resolve(angular.toJson(distrcit));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;*/
				var deferred = $q.defer();
        var DistrictArray={};
			DistrictArray.Response=[];
		var db = idop.result;
	    var tx = db.transaction(["District"], "readonly");
	    
	    var storeV = tx.objectStore("District");
	    var range = IDBKeyRange.only(Number(stateId));
	    var ind=storeV.index("StateIndex");
	    ind.openCursor(range).onsuccess=function(event){//.get(Number(DistrictId)).onsuccess=function(event){
	    												//debugger;
	    												var cursor = event.target.result;
														  if (cursor ) {
														  	//console.log(cursor.value.DistrictId )
														     DistrictArray.Response.push(cursor.value);
														    cursor.continue();
														  }
														  else {
														   // alert("Got all data: " +JSON.stringify(DistrictArray));
														    deferred.resolve(JSON.stringify(DistrictArray));
														  }
	    												};
		ind.openCursor(range).onerror=function(err){deferred.reject(err);};				    
						   
						        
						        
				return deferred.promise;

	};
	this.fetchdistrictprofile =function () {
		
		var deferred = $q.defer();
/*        $http ({method  : 'GET',
	         url     : base_url+"/GetDistrictDummy",
	         headers: {'Content-Type': 'application/json'}
	         }) */  $http.get('./js/district.json') 
		.success(function (res) { 

								//var districtdata=angular.toJson(res);
						         var distrcit={};
						        distrcit.Response=[];
						        angular.forEach(res,function(a)
						        {
						        	
						        	distrcit.Response.push(a);

								 });
						        
						        deferred.resolve(angular.toJson(distrcit));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchtaluka =function (DistrictId) {
		
		var deferred = $q.defer();
        var TalukaArray={};
			TalukaArray.Response=[];
		var db = idop.result;
	    var tx = db.transaction(["Taluka"], "readonly");
	    
	    var storeV = tx.objectStore("Taluka");
	    var range = IDBKeyRange.only(Number(DistrictId));
	    var ind=storeV.index("DistrictIndex");
	    ind.openCursor(range).onsuccess=function(event){//.get(Number(talukaId)).onsuccess=function(event){
	    												//debugger;
	    												var cursor = event.target.result;
														  if (cursor ) {
														  	//console.log(cursor.value.TalukaId )
														     TalukaArray.Response.push(cursor.value);
														    cursor.continue();
														  }
														  else {
														    //alert("Got all data: " +JSON.stringify(TalukaArray));
														    deferred.resolve(JSON.stringify(TalukaArray));
														  }
														};
	    												
		ind.openCursor(range).onerror=function(err){deferred.reject(err);};				    
	    return deferred.promise;
	};

	this.fetchvillages =function (talukaId) {
		
		var deferred = $q.defer();
        var VillageArray={};
			VillageArray.Response=[];
		var db = idop.result;
	    var tx = db.transaction(["Village"], "readonly");
	    
	    var storeV = tx.objectStore("Village");
	    var range = IDBKeyRange.only(Number(talukaId));
	    var ind=storeV.index("TalukaIndex");
	    ind.openCursor(range).onsuccess=function(event){//.get(Number(talukaId)).onsuccess=function(event){
	    												//debugger;
	    												var cursor = event.target.result;
														  if (cursor ) {
														  	//console.log(cursor.value.TalukaId )
														     VillageArray.Response.push(cursor.value);
														    cursor.continue();
														  }
														  else {
														    //alert("Got all data: " +JSON.stringify(VillageArray));
														    deferred.resolve(JSON.stringify(VillageArray));
														  }
	    												//alert(JSON.stringify(event.target.result) + " talukaId==" +talukaId);		

	    													};
						   /* storeV.openCursor().onsuccess = function(event) {
															  var cursor = event.target.result;
															  if (cursor ) {
															  	//console.log(cursor.value.TalukaId )
															     VillageArray.push(cursor.value);
															    cursor.continue();
															  }
															  else {
															    alert("Got all data: " +VillageArray[0].TalukaId);
															  }
															};*/										

		ind.openCursor(range).onerror=function(err){deferred.reject(err);};				    
						   
						        
						        
				return deferred.promise;
	};

	
	this.inserttaluka =function (TalukaId, districtid, stateid) {
	  	var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
		        url     : base_url + '/InsertTaggedTaluka',
		        headers: {'Content-Type': 'application/json'}, 
		        params:{ "userId" : USERID ,"talukaId": TalukaId,"districtId" : districtid,"stateId" : stateid, "webServicePassword" : "21@bhavyadivya"}
   			}).success(function (res) {
			        deferred.resolve(res);
			    }).error( function(data){
			    	deferred.reject(data);
			    	});
         return deferred.promise;
	};

	this.displaytaluka =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
                url     : base_url + '/GetTaggedTaluka',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
              }).success(function (res) {
						        var alltaluka=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.deletetaluka =function (TalukaId) {
		
		var deferred = $q.defer();
       $http({method  : 'GET',
	          url     : base_url + '/DeleteTaggedTaluka',
	          headers: {'Content-Type': 'application/json'}, 
	  		  params:{ "taggedTalukaId" : TalukaId ,"webServicePassword": "21@bhavyadivya"}
	   		}).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.insertvillages =function (villageid) {
	  	var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
              	url     : base_url + '/InsertTaggedVillage',
             	 headers: {'Content-Type': 'application/json'}, 
        		 params:{ "userId" : USERID ,"villageId" : villageid,"webServicePassword" : "21@bhavyadivya"}
   			}).success(function (res) {
			        deferred.resolve(res);
			    }).error( function(data){
			    	deferred.reject(data);
			    	});
         return deferred.promise;
	};

	this.displayvillages =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/GetTaggedVillage',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
              }).success(function (res) {
						        var alltaluka=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.deletevillages =function (villageId) {
		
		var deferred = $q.defer();
       $http({method  : 'GET',
         		url     : base_url + '/DeleteTaggedVillage',
         		headers: {'Content-Type': 'application/json'}, 
  		    	params:{ "taggedVillageId" : villageId ,"webServicePassword": "21@bhavyadivya"}
   			}).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchaccetedevent =function (eventDay,eventMonth,eventYear) {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/GetAcceptedEvents',
                headers: { 'Content-Type': 'application/json'}, 
                params:{"userId": USERID,"eventDay":eventDay,"eventMonth":eventMonth,"eventYear": eventYear,  "webServicePassword" : "21@bhavyadivya"}
              }).success(function (res) {
						       var accepteeventdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.displayallevents =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/GetAllEvents',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"userId" : USERID,"webServicePassword":"21@bhavyadivya"}
              }).success(function (res) {
						       var alleventdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.displayinvitedevents =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/GetInvitedEvents',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"userId" : USERID,"webServicePassword":"21@bhavyadivya"}
              }).success(function (res) {
						       var invitedevents=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.displaypastevents =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var test= new Date();
         //var prevdate = new Date();
        var prevdate =new Date( moment().subtract(3,'month'));
        
        
        prevdate.setDate(prevdate.getDate()-5);
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/GetPastEvents',
                headers: { 'Content-Type': 'application/json'}, 
                params: {"UserId" : USERID,"fromDay": prevdate.getDate(),"fromMonth":prevdate.getMonth()+1,"fromYear": prevdate.getFullYear(),
                "toDay": test.getDate(),"toMonth":  test.getMonth()+1,"toYear": test.getFullYear(),
                "webServicePassword":"21@bhavyadivya"}
              }).success(function (res) {
						       var pasteventsdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.eventdetaillist =function (id,user) {
		var deferred = $q.defer();
		 $http({method  : 'GET',
		          url     : base_url + '/GetEventDetailsById',
		          headers: {'Content-Type': 'application/json'}, 
		          params:{"eventListId":id,"userId":user, "webServicePassword" : "21@bhavyadivya"}
      			}).success(function (res) {
						        var eventlistdata=angular.toJson(res);
						       
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};
	
	this.insertacceptedevent =function (EventId) {
		 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		 $http({method  : 'GET',
		          url     : base_url + '/AcceptEventInvitation',
		          headers: {'Content-Type': 'application/json'}, 
		          params:{ "userId" : USERID ,"eventListId":EventId, "webServicePassword" : "21@bhavyadivya"}
      			}).success(function (res) {
						        var acceptdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};



	this.insertgreetevent =function (EventId) {
		 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		 $http({method  : 'GET',
		        url     : base_url + '/GreetEvent',
		        headers: {'Content-Type': 'application/json'}, 
		        params:{ "userId" : USERID ,"eventListId":EventId, "webServicePassword" : "21@bhavyadivya"}
      			}).success(function (res) {
						        var greetdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};


	this.updateattendanceevent =function (EventId) {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
		 $http({method  : 'GET',
             	url     : base_url + '/UpdateEventAttendance',
              	headers: {'Content-Type': 'application/json'}, 
              	params:{ "userId" : USERID ,"eventListId":EventId, "webServicePassword" : "21@bhavyadivya"}
      			}).success(function (res) {
						        var attendencedata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
                return deferred.promise;
	};

	this.GetUserProfile =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
         	   url     : base_url + '/GetUserProfile',
         	   headers: {'Content-Type': 'application/json'}, 
  		   	   params: {"userId" : USERID, "webServicePassword" : "21@bhavyadivya"}
       }).success(function (res) {
						        var profiledata=angular.toJson(res); 
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.GetSalutation =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
	//	$http.get('./js/saluationdata.json')  getSalutation
		 $http ({method  : 'GET',
         	   url     : base_url + '/getSalutation',
         	   headers: {'Content-Type': 'application/json'}, 
  		   	   params: {"gender" : 'male', "webServicePassword" : "21@bhavyadivya"}
       })
				.success(function (res) {
						        var salutationdata=angular.fromJson(res);
						        
						        /*var salutationdata={};
						        salutationdata.Response=[];
						        angular.forEach(res,function(b)
						        {
						        	
						        		salutationdata.Response.push(b);
						        });*/
						        deferred.resolve(angular.toJson(salutationdata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};
		
	this.GetPadnam =function () {
		var deferred = $q.defer();
		$http.get('./js/padamdata.json') 
						.success(function (res) {
						        //var padnamdata=angular.toJson(res);
						         var padnamdata={};
						        padnamdata.Response=[];
						        angular.forEach(res,function(b)
						        {
						        	padnamdata.Response.push(b);
						        });
						        deferred.resolve(angular.toJson(padnamdata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	}; 

	this.SetSamajikNav =function (gender) {
		var deferred = $q.defer();
		$http.get('./js/samjikNav.json')
					.success(function (res) {
						        //var samajikdata=angular.toJson(res);
						         var samajikdata={};
						        samajikdata.Response=[];
						        angular.forEach(res,function(b)
						        {
						        	if(b.GenderId == gender)
						        	samajikdata.Response.push(b);
						        });
						        deferred.resolve(angular.toJson(samajikdata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.GetProfilePic =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
          		url     : base_url + '/GetProfilePic',
          		headers: {'Content-Type': 'application/json'},
          		params : {"userId":USERID, "webServicePassword" : "21@bhavyadivya"}       
         }).success(function (res) {
						        var profilepicdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	}; 


	this.GetEventtype =function () {
		
		var deferred = $q.defer();
		$http.get('./js/eventType.json')
				.success(function (res) {
						        //var eventtypedata=angular.toJson(res);

						        var eventtypedata={};
						        eventtypedata.Response=[];
						        angular.forEach(res,function(f)
						        {
						        	
						        	eventtypedata.Response.push(f);
						        });
						        deferred.resolve(angular.toJson(eventtypedata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.GetEventNames =function (eventtypeid) {
		
		var deferred = $q.defer();
		$http.get('./js/eventName.json')
					.success(function (res) {
						        //$rootScope.eventnamedata=angular.toJson(res);
						        var eventnamedata={};
						        eventnamedata.Response=[];
						        angular.forEach(res,function(f)
						        {
						        	if(f.EventTypeId==eventtypeid)
						        	eventnamedata.Response.push(f);
						        });
						        deferred.resolve(angular.toJson(eventnamedata));
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};


	this.deletecustomevent =function (EventListId) {
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  var deferred = $q.defer();
       $http ({method  : 'GET',
           url     : base_url + '/DeleteMyCustomEvent',
           headers: {'Content-Type': 'application/json'},
           params:{ "eventListId": EventListId,"userId" : USERID ,"webServicePassword" : "21@bhavyadivya"}          
            }).success(function (res) {
              var deletmycustomeventdata=angular.toJson(res);
              deferred.resolve(res);
          }).error( function(data){
           deferred.reject(data);
           });
    return deferred.promise;
 };



	this.GetRateApp =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
	          url     : base_url + '/GetRateApp',
	          headers: {'Content-Type': 'application/json'},
	          params:{ "userId" : USERID ,"webServicePassword" : "21@bhavyadivya"}          
          		}).success(function (res) {
						        var ratingcount=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.fetchnoticlist =function () {
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNoticeTaggedbyVillage',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var getnoticedata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.detailsfetchnoticlist =function (id,uid) {
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNoticeDetails',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "noticeId" : id,"userId":uid || USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var detailgetnoticedata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};



	this.fetchusernotice =function () {
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetMyNoticeList',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var usernoticedetails=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchsearchnotice =function (villageid,surveyno) {
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/SearchNotice',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "villageId" : villageid,"userId":USERID,"SurveyNo" : surveyno,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var SearchNoticelist=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.ftechnewspaperlist =function () {
			var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetNoticeNewsPaper',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var getnewspaperdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.deletemynotice =function (noticeid) {
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/DeleteMyNotice',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "noticeId" : noticeid,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var deletemynotice=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};


	this.insertnoticevillages =function (villageid) {
	  	var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
        $http({method  : 'GET',
              	url     : base_url + '/InsertTaggedVillageForPublicNotice',
             	 headers: {'Content-Type': 'application/json'}, 
        		 params:{ "userId" : USERID ,"villageId" : villageid,"webServicePassword" : "21@bhavyadivya"}
   			}).success(function (res) {
			        deferred.resolve(res);
			    }).error( function(data){
			    	deferred.reject(data);
			    	});
         return deferred.promise;
	};
	this.fetchnoticevillages =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
         		url     : base_url + '/GetTaggedVillageForPublicNotice',
         		headers: {'Content-Type': 'application/json'}, 
  		    	params:{"userId" : USERID , "webServicePassword" : "21@bhavyadivya"}
    			}).success(function (res) {
						        var getnoticevillages=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.deletevillagenotice =function (villageid) {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({method  : 'GET',
         		url     : base_url + '/DeleteTaggedVillageForPublicNotice',
         		headers: {'Content-Type': 'application/json'}, 
  		    	params:{"villageId" : villageid , "webServicePassword" : "21@bhavyadivya"}
    			}).success(function (res) {
						        var getdeletedata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.getpeoplelist =function (id) {
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetPeopleListByRefId',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "refId" : id,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var peoplelistdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

		this.getcustomevent =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetMyCustomEvents',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var getcustomeventdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
          
          return deferred.promise;
	};
		this.fetchrefcounts =function (reid) {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetRefCount',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "refId" : reid ,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var getcustomeventdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchrefid =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetRefNoByUserId',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var getrefid=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};

	this.fetchshare =function () {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		 var deferred = $q.defer();
         $http({
                method  : 'GET',
                url     : base_url + '/GetShareKara',
                headers: { 'Content-Type': 'application/json'}, 
                params:{ "userId" : USERID,"webServicePassword" : "21@bhavyadivya"}
                }).success(function (res) {
						        var shareid=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
         
          return deferred.promise;
	};
	this.ClearCredentials =function () {
            $rootScope.globals = '';
            
           /* $cookieStore.remove('globals');*/
            //localStorage.testMenudata={};
            localStorage.globals=angular.toJson({});
            $http.defaults.headers.common.Authorization = 'Basic';
        };

	this.insertcustomevents =function (eventId,programPlace,eventTitle,eventDescription,programAddress,eventDay,eventMonth,eventYear,hour,minute,meridiem)
             {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http ({
                method  : 'GET',
                url     : base_url + '/InsertCustomEvent',
                headers: { 'Content-Type': 'application/json'}, 
                 params:{"userId":USERID, "eventId":programPlace, 
                         "programPlace":programAddress,
                           "eventTitle":eventTitle,
                            "eventDescription":eventDescription,
                           "programAddress":programAddress,
                          "eventDay":eventDay, "eventMonth":eventMonth, 
                          "eventYear":eventYear, "hour":hour,
                        "minute":minute, "meridiem":meridiem,
                        "webServicePassword":"21@bhavyadivya"}
              }).success(function (res) {
						       var insertcustomeventsdata=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.eventsearchprogramresult =function (eventid,eventtypeid,VillageId,talukaid,fromDay,fromMonth,fromYear,toDay,toMonth,toYear)
     {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http({method  : 'GET',
               url     : base_url+"/SearchProgram",
               headers: {'Content-Type': 'application/json'}, 
                params: {"userId":USERID,"villageId": VillageId,"talukaId": talukaid,"eventTypeId":eventtypeid,"eventId":eventid,
                          "fromDay":fromDay,"fromMonth":fromMonth,"fromYear": fromYear,
                       "toDay":toDay,"toMonth": toMonth,"toYear":toYear,
                        "webServicePassword" : "21@bhavyadivya"}
             }).success(function (res) {
						       var eventsearchresult=angular.toJson(res);
						        deferred.resolve(res);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
				return deferred.promise;
	};

	this.insertsubmittoadmindata =function (image1,image2,description)
      {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
       $http.post(base_url+"/ExchangeInfoToAdmin",
        JSON.stringify({"userId":USERID, "description": description,"imgStr1": image1,"imgStr2": image2 ,"webServicePassword" : "21@bhavyadivya"}))
		.success(function (res)
		 {
			var submittoadmindata=angular.toJson(res);
			 deferred.resolve(res);
		}).error( function(data)
		{
			deferred.reject(data);
		});
				return deferred.promise;
	};

	this.insertcustomnotice =function (villageid,noticeid,fromDay,fromMonth,fromYear,surveyno,image1,image2)
      {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
      $http.post(base_url+"/InsertNoticeList",
              JSON.stringify({"userId":USERID,"villageId": villageid,"newsPaperId":noticeid,"publishDay":fromDay,
                "publishMonth":fromMonth,"publishYear":fromYear,
                "SurveyNo":surveyno ,"imageStr1": image1, "imageStr2":image2,"webServicePassword": '21@bhavyadivya'}))
		.success(function (res)
		 {
			var insertcustomnoticedata=angular.toJson(res);
			 deferred.resolve(res);
		}).error( function(data)
		{
			deferred.reject(data);
		});
				return deferred.promise;
	};

	this.insertcontactusdata =function (message)
      {
		var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
		var deferred = $q.defer();
     $http({method  : 'GET',
             url     : base_url+"/ContactUs",
             headers: {'Content-Type': 'application/json'}, 
             params:{ "userId" : USERID ,"message":message,"webServicePassword" : "21@bhavyadivya"}
      }).success(function (res)
		 {
			var contactusdata=angular.toJson(res);
			 deferred.resolve(res);
		}).error( function(data)
		{
			deferred.reject(data);
		});
				return deferred.promise;
	};
		this.shortenPosts = function(posts) {
		//we will shorten the post
		//define the max length (characters) of your post content
		var maxLength = 500;
		return _.map(posts, function(post){
			if(post.content.length > maxLength){
				//trim the string to the maximum length
				var trimmedString = post.content.substr(0, maxLength);
				//re-trim if we are in the middle of a word
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf("</p>")));
				post.content = trimmedString;
			}
			return post;
		});
	};

});



