angular.module('bhavydivya.controllers', ['ionic-ratings','ngCordova','ngImageCompress','multipleDatePicker'])
.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig,AuthService,$window) {
/* var alldata = angular.fromJson($window.localStorage.alldata);
  $scope.profiledata=alldata[0];*/


function init(){
                 
                  if($window.localStorage.localprofilepic && $window.localStorage.localprofilepic !==''){
                        $scope.profilepicurl=JSON.parse($window.localStorage.localprofilepic);
                      }
                  else{

                        AuthService.GetProfilePic().then(function(response)
                            {
                              var profileimage = JSON.parse(response).Response;
                              $scope.profilepicurl=profileimage[0];
                            
                              $window.localStorage.localprofilepic=(angular.toJson($scope.profilepicurl));
                              
                             });
                      }
                      if($window.localStorage.alldata && $window.localStorage.alldata.length !==0){
                        $scope.profiledata=JSON.parse($window.localStorage.alldata)[0];
                      }
                      else{
                            AuthService.GetUserProfile().then(function(response)
                            { 
                              var alldata=  JSON.parse(response).Response;
                              $scope.profiledata=alldata[0];
                              $window.localStorage.alldata=(angular.toJson(alldata));
                              
                            });
                      }
            }
            init();
$scope.$on("updateProfile", function(event, args){ 
                     
                      setTimeout(function() {
                          $scope.$apply(function(){
                            var imgProf=document.getElementById("smallimg");
                            if(imgProf){imgProf.src=  args.test;}
                          
                          });
                        }, 0);});


})



.controller('LoginCtrl', function($scope, $state, $templateCache,RequestsService, $q, $rootScope,$ionicLoading,$ionicPopup,AuthService,$window) {

//if(typeof analytics !== undefined) { analytics.trackView("login.html"); }
 $scope.doLogIn = function(logindata){


 
AuthService.doLogin(logindata).then(function(response){
  if (response.success) {
  
                  /*if(response.IsVerified=="True"){
                      $state.go('app.Home');
                   }
                   else{
                          $scope.USERNAME = $window.localStorage.alldata ? angular.fromJson($window.localStorage.alldata)[0].FName : '';
                          $scope.LASTNAME = $window.localStorage.alldata ? angular.fromJson($window.localStorage.alldata)[0].LName : '';
                           AuthService.checkusername($scope.login).then(function(response){ 
                            if (JSON.parse(response).Error=='false' )
                             {    var testguid= JSON.parse(response).Response[0].GUID;
                                 // $ionicLoading.hide();
                                  //$state.go('auth.otp',{'mobile':$scope.login.userName,'GUID':testguid,'pagefp':'login'});
                                  $state.go('app.Home');
                               }
                               else{
                                    $ionicPopup.alert({title: 'दोष',template: err ? angular.toJson(err):"पुन्हा प्रयत्न करा.",text:'ठीक आहे'}); 
                                }
                        });
                    }*/
           $state.go('app.Home');         
      } 

    else {

           
           
                    console.log("erooooooooooooooor "+response.message);
              $scope.login={};
              $ionicLoading.hide();$ionicPopup.alert({
              title: 'दोष',
              template: response.message ? response.message:"पुन्हा प्रयत्न करा.",
               text:'ठीक आहे' });
              
              if(response.code && response.code==-3){
            $state.go('auth.signup');

           }
        
      }
   }, function(err) { 
    console.log(err);
    $ionicLoading.hide();
    $ionicPopup.alert({title: 'दोष',template: err ? angular.toJson(err):"पुन्हा प्रयत्न करा.",text:'ठीक आहे'});
  });
 //$window.localStorage.notification=$window.localStorage.notification|| JSON.stringify([{}]);
 };
 $scope.login = {};
})

.controller('logoutctrl', function($scope,$state,$window,AuthService, $timeout,  _) {
  var logout=function() {AuthService.ClearCredentials();
  localStorage.removeItem('currentUser');
   //localStorage.removeItem('locallogindata');
   localStorage.removeItem('localprofilepic');
   localStorage.removeItem('alldata');
  $state.go('auth.walkthrough');
};
$timeout(function () {logout();},100);
})

.controller('userotpctrl', function($scope, $state, $templateCache, $q, $rootScope,$ionicLoading,$ionicPopup,AuthService,$window,$http) {
 
  $scope.checkusername = function(uid)
  {
   $ionicLoading.show
   ({
    template: 'प्रतिक्षा करा ...'
    });

  AuthService.changePasswordSMS(uid).then(function(response)
  { 
    
    
    if (JSON.parse(response).Error=='false' )
     {    var testguid= JSON.parse(response).Response[0].GUID;
     
          $ionicLoading.hide();
          //$state.go('auth.otp',{'mobile':$scope.login.userName,'GUID':testguid,'pagefp':'forpass'});
         // $state.go('app.home');

         $ionicPopup.alert({template:"तुम्हाला पासवर्ड SMS करण्यात आला आहे.",
                        text:'ठीक आहे'}).then(function(res){ 
                           $state.go('auth.login');
                          //$ionicPlatform.onHardwareBackButton(function(res){event.preventDefault();});
                        });      
          
       } else {
              $scope.login={};
              $ionicLoading.hide();$ionicPopup.alert({
                title: 'दोष',
                template: response.message ? response.message:"पुन्हा प्रयत्न करा.",
                 text:'ठीक आहे' });
                }
  }, function(err) 
  { 
   $ionicLoading.hide();
    $ionicPopup.alert
    ({
        title: 'दोष',
        template: err ? angular.toJson(err):"पुन्हा प्रयत्न करा.",
         text:'ठीक आहे'
    });
  });
  $window.localStorage.locallogindata=(angular.toJson($scope.login));
 };
    $scope.login = {};
})

.controller('SignupCtrl', function($scope,$ionicPlatform,$state,$http,$ionicPopup,$ionicLoading,$window,base_url) {

  //if(typeof analytics !== undefined) { analytics.trackView("signup.html"); }
  $scope.enable=true;
    $scope.doSignUp = function(){
    $state.go('app.feeds-categories');
  };
  $scope.refok=1;
  
$scope.CheckReference=function(test){
var par={};
par.webServicePassword='21@bhavyadivya';
par.refNo=test;
  $http({
      method: 'GET',
      url:base_url+"/CheckRefNo",
      headers: {
            'Content-Type': 'application/json'}, params:par
          }).then(function(data) {
            $ionicLoading.hide();

            var res= JSON.parse(data.data);
            if(res.Error==="false"){
               // alert(res.Response[0].IsExist);
                $scope.refok=res.Response[0].IsExist;
            }
          });
};



  $scope.signup={};
  $scope.addRegistration=function(){
    $scope.enable=false;
    console.log($scope.signup.Birth_date);
    var test = new Date($scope.signup.Birth_date|| new Date('1/1/1990'));
      $scope.signup.birthDay=test.getDate();
      $scope.signup.birthMonth=test.getMonth()+1;
      $scope.signup.birthYear=test.getFullYear();
   
      $scope.signup.webServicePassword="21@bhavyadivya";
      $ionicLoading.show({
       template: 'प्रतिक्षा करा'
      });
      $http({
      method: 'GET',
      url:base_url+"/RegistrationNew",
      headers: {
            'Content-Type': 'application/json'}, params:$scope.signup
          }).then(function(data) {
            $ionicLoading.hide();


            var res= JSON.parse(data.data);
            if(res.Error==="false"){
               //$ionicPlatform.offHardwareBackButton(function(res){event.preventDefault();});
                 if(res.Response[0].UserId === '-1' ){
                  $scope.enable=true;
                      $ionicPopup.alert({
                         title: 'दोष',
                         template:"तुम्ही भरलेल्या मोबाईल नं. चे अकाऊंट अस्तित्वात आहे. नवीन नंबरने पुन्हा  प्रयत्न करा. ",
                        text:'ठीक आहे'}).then(function(res){ 
                          //$ionicPlatform.onHardwareBackButton(function(res){event.preventDefault();});
                        });      
                 }
                 else{
                        $scope.signupdata=res.Response;
                        var testguid= res.Response[0].GUID;
                        $window.localStorage.localuserdata=(angular.toJson($scope.signupdata));
                        popup=$ionicPopup.alert({
                            template:"माहिती जतन झाली" ,
                            text:'ठीक आहे'}).then(function(){
                             // $ionicPlatform.onHardwareBackButton(function(res){event.preventDefault();});
                              //$state.go('auth.otp',{'mobile':$scope.signup.userName,'GUID':testguid,'pagefp':'home'});});
                               $state.go('app.Home');});
                      
                 }

            } 
            else{
              $scope.enable=true;
               $ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा. ",
          text:'ठीक आहे'});
            }  
         },function(err){ $ionicLoading.hide();$scope.enable=false;$ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा. ",
          text:'ठीक आहे'});
            });
 
 };

 $scope.datepickerObject = {
  titleLabel: 'जन्मतारिख',  //Optional
 todayLabel: 'आज',  //Optional
  closeLabel: 'बंद',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date('01/01/1990'),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(1940, 8, 2), //Optional
  to: new Date(2008, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallback(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallback = function (val) {

  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    var eightYearsAgo = moment().subtract(8, "years");
    $scope.signup.Birth_date=  eightYearsAgo.isAfter(new Date(val)) ? moment(new Date(val)) : moment(new Date('01/01/1990'));
    console.log('Selected date is : ', val);
    $scope.datepickerObject.inputDate=   moment(new Date($scope.signup.Birth_date))
        .format('DD-MM-YYYY');
         console.log('Selected date is : ', $scope.datepickerObject.inputDate);
  }
};

/*$scope.validateBirtDate=function (date){
    var eightYearsAgo = moment().subtract(8, "years");
    var birthday = moment(date);
    
    if (!birthday.isValid()) {
        
        $scope.invaildDOB=true;    
    }
    else if (eightYearsAgo.isAfter(birthday)) {
        return "okay, you're good";    
    }
    else {
        return "sorry, no";    
    }
}*/

 
})

.controller('oneTimePasswordCtrl', function($scope,_, $state,$ionicLoading,AuthService,$ionicPopup,$stateParams,$http,$window) {
  $scope.mobile = $stateParams.mobile;
  var frompage = $stateParams.pagefp;
  $scope.Guid = $stateParams.GUID;
  /*alert(angular.toJson($scope.Guid));*/
  $scope.resendStatus=false;
  $scope.submitOTP = function(testOTP){
    $ionicLoading.show({
      template: 'OTP तपासत आहे'
    });
     AuthService.submitOTP(testOTP,$scope.Guid)
      .then(function(data){
        /*var localuserdata=  _.extend(_.findWhere($scope.guidata,{ UserId: $scope.guidata.UserId,GUID:$scope.guidata.GUID }));
        $window.localStorage.localldetaileventdata=(angular.toJson($scope.localuserdata));*/
       var otpdata= angular.fromJson(data);
       /*alert("guiid from submit otp ======" + angular.toJson($scope.Guid));*/
        $ionicLoading.hide();
        if (otpdata.Error== 'false') {
             
           if(otpdata.OTPStatus.indexOf("wrongotp")!=-1){
              $ionicPopup.alert({title: 'चुकीचा OTP',template:"चुकीचा OTP, पुन्हा प्रयत्न करा. " ,
             text:'ठीक आहे'});
              $scope.resendStatus=true;
            }
              else if(otpdata.OTPStatus.indexOf("otpexpired")!=-1){
              $ionicPopup.alert({title: 'OTP कालबाह्य झाला.',template:"कालबाह्य OTP, पुन्हा प्रयत्न करा. " ,
             text:'ठीक आहे'});
              $scope.resendStatus=true;
            }
             else if(otpdata.Response.length>0 && otpdata.OTPStatus.indexOf("otpverified")!=-1){
                $window.localStorage.currentUser = angular.toJson(otpdata.Response);
                if (frompage=="home") {
                  $ionicPopup.alert({title: 'यशस्वी',
                 template:"OTP बरोबर आहे." ,
                text:'ठीक आहे'}).then(function(){ $state.go('app.Home');});
                  
                }else if (frompage=="forpass") {
                    $ionicPopup.alert({title: 'यशस्वी',
                 template:"OTP बरोबर आहे, पासवर्ड बदला." ,
                text:'ठीक आहे'}).then(function(){ $state.go('auth.forgot-password');});
                 
                
                }else if (frompage=='login') {
                  $ionicPopup.alert({title: 'यशस्वी',
                 template:"OTP बरोबर आहे." ,
                text:'ठीक आहे'}).then(function(){ 
                  $state.go('app.Home');});
                 
                }
              
              }
            
        } //end of error false
        else {
          $ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा.",
          text:'ठीक आहे'});
          $scope.resendStatus=true;
        }},

        function(err) { 
        $ionicLoading.hide();$ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा. ",
          text:'ठीक आहे'});
    });
    };
    
  $scope.resendOTP = function(){
    $ionicLoading.show({
    template: 'OTP पाठवत आहोत'
    });
    AuthService.resendOTP($scope.mobile)
      .then(function(data){
        var guidata= angular.fromJson(data);
        
        $scope.Guid = JSON.parse(data).Response[0].GUID;
       /* alert("resendOTP guid" + angular.toJson($scope.Guid));*/
        $scope.resendStatus=false;
        $ionicLoading.hide();$ionicPopup.alert({
          title: 'यशस्वी',
          template:"OTP आपल्या मोबाईल वर पाठवला आहे",
           text:'ठीक आहे' });
    },
        function(err) { 
        $ionicLoading.hide();$ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा." + err,
          text:'ठीक आहे'});
    });
    };
    $scope.user = {};
})

.controller('profilectrl',function($scope,$stateParams,$state,$templateCache,$rootScope, $q,$http,Service,$window,$ionicPopup,$ionicLoading,AuthService,$cordovaCamera,base_url) {
    
//if(typeof analytics !== undefined) { analytics.trackView("profile.html"); }
    $scope.profiledata={};
   
    var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
    $scope.saveaddress=function(image1,profilelist)
        {

           $ionicLoading.show({
       template: 'प्रतिक्षा करा'
      });
         $scope.profiledata.BirthDate=(new Date($scope.datepickerObject.inputDate || new Date('1/1/1990') ));
          //image1= angular.copy($scope.profiledata.profileimage);
          var test = new Date($scope.profiledata.BirthDate || new Date('1/1/1990') );
          $scope.profiledata.birthDay=test.getDate() +"";
          $scope.profiledata.birthMonth=(test.getMonth()+1)+"" ;
          $scope.profiledata.birthYear=test.getFullYear() +"";
          $scope.profiledata.webServicePassword="21@bhavyadivya";
          $scope.profiledata.userId=USERID;
          //$scope.profiledata.IsSameAddr="0";
          $scope.profiledata.IsMarried=$scope.profiledata.IsMarried +"";
        if($scope.profiledata.IsSameAddress)
         {
          
          $scope.profiledata.PresAddress=angular.copy($scope.profiledata.ParAddress);
          $scope.profiledata.PresStateId=angular.copy($scope.profiledata.ParStateId);
          $scope.profiledata.PresDistrictId=angular.copy($scope.profiledata.ParDistrictId);
          $scope.profiledata.PresTalukaId=angular.copy($scope.profiledata.ParTalukaId);
           $scope.profiledata.PresVillageId=angular.copy($scope.profiledata.ParVillageId);
         }
        
      $http({
            method: 'GET',
            url:base_url+"/UpdateUserProfile",
             headers: {'Content-Type': 'application/json'}, 
             params:$scope.profiledata
          }).then(function(data) {
            //alert(angular.toJson(data));
            if (angular.fromJson(data).error)
              {
              $ionicLoading.hide();
              $ionicPopup.alert({
             title: 'दोष',
             template:"पुन्हा प्रयत्न करा." + err,
            text:'ठीक आहे'});
              }
              else
              {
              $ionicLoading.hide();
              $ionicPopup.alert({
                template:"माहिती जतन झाली" ,
               text:'ठीक आहे'});
               if(localStorage.alldata){ localStorage.removeItem("alldata");}
            }
             });

           $http.post(base_url+"/UpdateProfilePic",
              JSON.stringify({"userId" : USERID , "profilePicture": $scope.profilepicurl.PicURL, "webServicePassword" : "21@bhavyadivya"}))
            .then(function(data) 
            {
               if(localStorage.localprofilepic){localStorage.removeItem("localprofilepic");}
               //alert($scope.profilepicurl.PicURL);
               //$scope.$emit("updateProfile", { test: $scope.profilepicurl.PicURL});
               //$window.localStorage.localprofilepic=(angular.toJson($scope.profilepicurl.PicURL));
            });

        };
          

    AuthService.GetUserProfile().then(function(response)
      { 
        var alldata=  JSON.parse(response).Response;
        $scope.profiledata=alldata[0];
        $rootScope.genderid=$scope.profiledata.Gender;
        $scope.samajiknavgender();
        $scope.paraddress(); 
        $scope.pressaddress();
        $scope.datepickerObject.inputDate = new Date ($scope.profiledata.BirthDate || '01-01-1990');
        $window.localStorage.alldata=(angular.toJson(alldata));

      });

      AuthService.fetchstate().then(function(response)
      { 
        $scope.statedata= JSON.parse(response).Response;
      });

      $scope.paraddress=function()
      {
      AuthService.fetchdistrictprofile().then(function(response)
      { 
        $scope.pardistrictdata= JSON.parse(response).Response;
        var DistrictId=$scope.profiledata.ParDistrictId;
         AuthService.fetchtaluka(DistrictId).then(function(response)
            { 
             
              $scope.partaluka= JSON.parse(response).Response;
              var TalukaId=$scope.profiledata.ParTalukaId;
              AuthService.fetchvillages(TalukaId).then(function(response)
              { 
              $scope.parvillages= JSON.parse(response).Response;
             });

            });
      });
  };

  $scope.pressaddress=function()
  {
       AuthService.fetchdistrictprofile().then(function(response)
      { 
        $scope.perssdistrictdata= JSON.parse(response).Response;
        var presDistrictId=$scope.profiledata.PresDistrictId;
         AuthService.fetchtaluka(presDistrictId).then(function(response)
            { 
              $scope.persstaluka= JSON.parse(response).Response;
              var presTalukaId=$scope.profiledata.PresTalukaId;
              AuthService.fetchvillages(presTalukaId).then(function(response)
              { 
              $scope.perrvillages= JSON.parse(response).Response;
             });

            });
      });
    };
    
       $scope.pardistrict=function(stateId)
        {
          AuthService.fetchdistrict(stateId).then(function(response)
           { 
             $scope.pardistrictdata= JSON.parse(response).Response;
            });
        }; 
      $scope.parGetTaluka=function(DistrictId)
      {
            AuthService.fetchtaluka(DistrictId).then(function(response)
            { 
              $scope.partaluka= JSON.parse(response).Response;
            });
      };

      $scope.parGetVillage=function(TalukaId)
      {
            AuthService.fetchvillages(TalukaId).then(function(response)
            { 
              $scope.parvillages= JSON.parse(response).Response;
             });
      };
       $scope.pressdistrict=function(stateId)
        {
          AuthService.fetchdistrict(stateId).then(function(response)
           { 
             
              $scope.perssdistrictdata= JSON.parse(response).Response;
            });
        }; 
      $scope.pressGetTaluka=function(DistrictId)
      {
            AuthService.fetchtaluka(DistrictId).then(function(response)
            { 
              
              $scope.persstaluka= JSON.parse(response).Response;
            });
      };

      $scope.pressGetVillage=function(TalukaId)
      {
            AuthService.fetchvillages(TalukaId).then(function(response)
            { 
              
              $scope.perrvillages= JSON.parse(response).Response;
             });
      };


      AuthService.GetSalutation().then(function(response)
      { 
        $scope.salutationdata = JSON.parse(response).Response;
      });

  

      AuthService.GetPadnam().then(function(response)
      { 
        $scope.padnamdata = JSON.parse(response).Response;
      });


      $scope.samajiknavgender=function(gender){
        gender=gender||$rootScope.genderid;
        if(gender=='1') gen='male'; else gen='female';
        
      AuthService.SetSamajikNav(gen).then(function(response)
      { 
        $scope.samajikdata = JSON.parse(response).Response;
      });
      };
      
      function pic(){
      AuthService.GetProfilePic().then(function(response) 
      {
        var profileimage = JSON.parse(response).Response;
        $scope.profilepicurl=profileimage[0];
         $window.localStorage.localprofilepic=(angular.toJson($scope.profilepicurl.PicURL));
        //console.log("image url"+ angular.toJson($scope.profilepicurl.PicURL));
        
        });
    }
     $scope.profilepicurl.PicURLs = $window.localStorage.localprofilepic ? $window.localStorage.localprofilepic:pic(); 
    
  $scope.changePassword = function(udata){
  
  delete $scope.user.confirmpassword;
  $ionicLoading.show({
    template: 'प्रतिक्षा करा'
   });

  AuthService.changePassword(angular.toJson($scope.user))
    .then(function(data){
     //alert(angular.toJson(data));       
     $ionicLoading.hide();
     
      if(angular.fromJson(data).Error=="false" && angular.fromJson(data).Response[0].ReturnMessage =='PasswordUpdated' )
      {
      $ionicPopup.alert({
           title: 'यशस्वी',
           template:"पासवर्ड बदलला." 
         }).then(function(test){$scope.user = {};
           $state.go('app.Home');});

      }

      else if(angular.fromJson(data).Error=="false" && angular.fromJson(data).Response[0].ReturnMessage == 'WrongOldPassword' )
      {
         $ionicPopup.alert({
           title: 'दोष',
          template:"जुना पासवर्ड चुकीचा आहे. पुन्हा प्रयत्न करा.",
          text:'ठीक आहे'});
      }

      else{
        $ionicPopup.alert({
           title: 'दोष',
          template:"पुन्हा प्रयत्न करा.",
          text:'ठीक आहे'});
      }

    }, function(err) 
    { 
     $ionicLoading.hide();$ionicPopup.alert({
           title: 'दोष',
          template:"पुन्हा प्रयत्न करा.",
          text:'ठीक आहे'});
    });

};
  $scope.user = {};
  var udata;
  udata = $scope.user;

  $scope.datepickerObject = {
  titleLabel: 'तारीख',  //Optional
  todayLabel: 'आज',  //Optional
  closeLabel: 'बंद',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date('01/01/1990'),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(1940, 8, 2), //Optional
  to: new Date(2008, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallback(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallback = function (val) 
{

  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    console.log('Selected date is : ', val);
    $scope.datepickerObject.inputDate= new Date(val);
         console.log('Selected date is : ', $scope.datepickerObject.inputDate);
  }
};
})

.controller('ForgotPasswordCtrl', function($scope,base_url, $state,$q,$ionicPopup,$http,$window) {
  //if(typeof analytics !== undefined) { analytics.trackView("forgot-password.html"); }
  var USERID = angular.fromJson($window.localStorage.localuserdata)[0].UserId;
  $scope.recoverPasswordonMobile = function(recoverpass)
  {
    var deferred = $q.defer();
    var req=
    {
    method: 'GET', url: base_url+'/ForgotPassword',
    headers: {'Content-Type': 'application/json'},
    params : {"userId": USERID, "password": recoverpass, "webServicePassword": "21@bhavyadivya"},
    };
    $http(req).success(function (res) {
       if (res && res.length>0 ) 
      {
        $ionicPopup.alert({
           title: 'यशस्वी',
           template:"पासवर्ड जतन झाला. पुन्हा लॉग इन करा.",
           text:'ठीक आहे'});
           $state.go('auth.login');
      }
        else
      {
          $ionicPopup.alert({
           title: 'दोष',
           template:"हा मोबाईल नोंदववलेला  नाही.",
            text:'ठीक आहे' });
      }
      }).error( function(data){
      deferred.reject(data); $ionicPopup.alert({
           title: 'दोष',
           template:"पुन्हा प्रयत्न करा." + data,
          text:'ठीक आहे'});
      });
          return deferred.promise;
  };
    $scope.user = {};
  
})


.controller('eventctrl', function($scope,$state,AdMob,$filter,$stateParams,AuthService,$rootScope, $templateCache,$ionicPopover,Service,$window, $http,$ionicPopup,$ionicLoading,base_url) {

/*if(typeof analytics !== undefined) { analytics.trackView("all.html"); }
if(typeof analytics !== undefined) { analytics.trackView("acceptance.html"); }
if(typeof analytics !== undefined) { analytics.trackView("invited.html"); }
if(typeof analytics !== undefined) { analytics.trackView("pastevent.html"); }*/

/*console.log("b4 admob banner");
AdMob.showBanner();
console.log("after admob banner");
$scope.$on("$destroy", function() {
       AdMob.removeAds();
       
    });*/

  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  var template = '<ion-popover-view style="margin-top: 7px;height: 292px;">' +
                    ' <ion-content >' +
                    '<ion-item ng-click="hidePopOver(\'app.insertcustomevent\')" style="font-size: 100%;">माझे कार्यक्रम समाविष्ट करा</ion-item>' +
                    '<ion-item ng-click="hidePopOver(\'app.usereventlist\')" style="font-size: 100%;">माझे वैयक्तिक कार्यक्रम रद्द करा</ion-item>' +
                    '<ion-item href="http://bhavyadivya.com/User/New-Event.aspx" style="font-size: 100%;height: 62px;">सार्वजनिक कार्यक्रम समाविष्ट करा</ion-item>' +
                    '<ion-item ng-click="hidePopOver(\'app.pastevents\')" style="font-size: 100%;">झालेले कार्यक्रम</ion-item>' +
                    '<ion-item ng-click="hidePopOver(\'app.insertvillage\')" style="font-size: 100%;">कार्यक्रम पाहण्यासाठी परिसर निवडा</ion-item>' +
                    /*'<ion-item ng-click="hidePopOver(\'app.yearswisecount\')" style="font-size: 100%;" >आजचे वाढदिवस</ion-item>' +*/
                    
                    
                    '</ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

    $scope.hidePopOver = function(path) 
    {
    $scope.popover.hide();
    $state.go(path);
    };

    $scope.acceptedeventshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;आपली रोजनिशी ! येथे आपण स्विकारलेले कार्यक्रम दिसतील. आपले दिवसभराचे मिनीट  टू मिनीट  नियोजन येथे दिसेल. आपल्या परिचयातील मोठे नेते, उद्योजक व अधिकारी यांचेही असेच नियोजन असते, मग आपले का नाही. योग्य नियोजनातूनच आपल्याला भव्यदिव्य कार्य उभे करता येईल. कार्यक्रमास आपण उपस्थिती दर्शवू शकता, ज्यायोगे गर्दीच्या वेळेतही निमंत्रकास कळेल कि आपण येऊन गेलात !आपण स्विकारलेले भविष्यातील कार्यक्रमही आपणास कॅलेंडरमध्ये पाहता येतील.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

    $scope.invitedshowConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;यात आलेली निमंत्रणे हि आपल्याला वैयक्तिकरित्या पाठवलेली असतात कारण निमंत्रकाच्या दृष्टीने आपण एक महत्वपूर्ण व्यक्ती/परिवार आहात... जशी आपल्याला एखाद्या लग्नाची पत्रिका घरपोच येते त्याप्रमाणेच.... आपण ते निमंत्रण स्वीकारल्यास ते आपल्या दैनंदिन कार्यक्रमात, त्या तारखेस दिसू लागेल.'


        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

    $scope.alleventshowConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;आपण निवडलेल्या गावात/परिसरात विविध कार्यक्रम होत असतात. या सर्वांची माहिती तुम्हाला येथे  मिळेल व यापैकी आपणास कुठल्या कार्यक्रमास जायचे असेल तर आपण तो कार्यक्रम स्विकारावा म्हणजे तो कार्यक्रम आपल्या दैनंदिनीत दिसू लागेल.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

    $scope.pasteventshowConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;या ठिकाणी झालेल्या सर्व कार्यक्रमांची यादी आपल्याला दिसेल. आपण त्या कार्यक्रमांना हजर असाल तर “उपस्थिती दर्शवा” म्हणजे निमंत्रकास कळेल कि आपण येऊन गेलात. आपल्या घरी जेव्हा एखादा कार्यक्रम असेल तर त्याला यावेच लागेल.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

  $scope.allDate=[];
    $scope.dateSelection = function()
    {
      
      $http({method  : 'GET',
                  url     : base_url+"/GetEventwiseCountForCalendar",
                  headers: {'Content-Type': 'application/json'}, 
                  params:{"userId": USERID, "webServicePassword" : "21@bhavyadivya"}
                }).then(function(data) {
                      var res= JSON.parse(data.data);
                      $scope.dateAvailable=res.Response;
                      $scope.dateAvailable.forEach(function(entry){
                      
                        var originaldate=new Date(entry.EventDate);
                        //alert(entry.EventDate + ' ' + originaldate);
                         $scope.allDate.push(originaldate);
                      });
                      $scope.myArrayOfDates= calanderDates($scope.allDate);
                      
                      $scope.$broadcast('scroll.refreshComplete');
                  }, function (reason) {
                          $ionicLoading.hide();
                          $scope.$broadcast('scroll.refreshComplete');
                        }

               );

    };

    function calanderDates (daterange)
    {
       var arr=[];
       daterange.forEach(function(d){
        
        arr.push({date: d, css: 'holiday',title: 'Holiday time !'});
        //$scope.date="";
        //$scope.date=arr;
        
       });
        return arr;
      }
         
     $scope.IsHidden = true;
            $scope.ShowHide = function ()
             {
                //If DIV is hidden it will be visible and vice versa.
                $scope.IsHidden = $scope.IsHidden ? false : true;
               if(  $scope.IsHidden){
                $scope.date=new Date();
                acceptcurrentdate(new Date());}
            };

     AuthService.GetEventNames().then(function(response)
      { 
        $scope.eventdata= JSON.parse(response).Response;
      });

    var newdate = $filter('date')(Date.now(), 'yyyy-MM-dd');
    $scope.date = new Date(newdate);

   $scope.changedate=function(date)
   {
    
      $scope.date=date;
   };

     $scope.events={};

      $scope.accptedeventsearch=function(event, date)
       {
            var test = new Date (date.date);

            //test=test||date;
            $scope.events.eventDay=test.getDate();
            $scope.events.eventMonth=test.getMonth()+1;
            $scope.events.eventYear=test.getFullYear();
            $scope.date=test;

            AuthService.fetchaccetedevent($scope.events.eventDay, $scope.events.eventMonth, $scope.events.eventYear).then(function(data) {
                      var res= JSON.parse(data);
                      if (res.Response.length=='0') 
                        {
                        $ionicPopup.alert({
                              template:"माहिती उपलब्ध नाही",
                              text:'ठीक आहे'
                         });
                        $scope.acceptedeventsdata={};
                       // $scope.ShowHide();
                
                         }
                        
                      else
                      {
                         $scope.acceptedeventsdata=res.Response;
                          //$scope.ShowHide();
                        
                      }
                    },function(err){ /*alert("error ======" + angular.toJson(err));*/ });
  };

function acceptcurrentdate(test){

            var eventDay=test.getDate();
            var eventMonth=test.getMonth()+1;
            var eventYear=test.getFullYear();
       
       AuthService.fetchaccetedevent(eventDay, eventMonth, eventYear).then(function(data) {
                      var res= JSON.parse(data);
                      if (res.Response.length=='0') 
                        {
                        /*$ionicPopup.alert({
                              template:"माहिती उपलब्ध नाही",
                              text:'ठीक आहे'
                         });*/
                
                         }
                        
                      else
                      {
                         $scope.acceptedeventsdata=res.Response;
                      }
                    });
              }

//acceptcurrentdate(new Date());
$scope.acceptedeventsdata='';

  $scope.fetchallevents = function()
  {
       AuthService.displayallevents().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  $scope.alleventdata= JSON.parse(response).Response;
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };

  $scope.fetchinvitedevents = function()
  {
       $ionicLoading.show({
       template: 'प्रतिक्षा करा'
      });

       AuthService.displayinvitedevents().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
               $ionicLoading.hide();
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  $scope.invitedevents= JSON.parse(response).Response;
                  $ionicLoading.hide();
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $ionicLoading.hide();
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };

  $scope.fetchpastevents = function()
  {

       AuthService.displaypastevents().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  $scope.pasteventsdata= JSON.parse(response).Response;
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };

 // $scope.fetchpastevents();

  $scope.specificacceptancepage=function(acceptevent){
    $rootScope.acceptEventListId=acceptevent.EventListId;
    $state.go('app.detailacceptedevent',{'EventListId' : acceptevent.EventListId});
  };
  $scope.specificalleventpage=function(allevent){
    $stateParams.EventListId=allevent.EventListId;
    $state.go('app.detailallevent',{'EventListId' : allevent.EventListId});
  };
   $scope.specificinvitedeventpage=function(invitedevents){
    $rootScope.invitedEventListId=invitedevents.EventListId;
    $state.go('app.detailinvitedevents',{'EventListId' : invitedevents.EventListId});
  };
  $scope.specificpasteventpage=function(pastevent){
   $rootScope.pastEventListId=pastevent.EventListId;
    $state.go('app.detailpastevent',{'EventListId' : pastevent.EventListId});
  };
})

.controller('detailalleventlistctrl',function($scope,$stateParams, AuthService, $state, $ionicLoading ,$templateCache,$ionicPopup, $q, $rootScope,$http,Service,$window,_) {
    //if(typeof analytics !== undefined) { analytics.trackView("detailalleventlist.html"); }
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
 
  var id = $stateParams.EventListId;

$scope.geteventdetail=function()
{
    AuthService.eventdetaillist(id,USERID).then(function(response)
     {
       var detailallevent= JSON.parse(response).Response;
        $scope.detailalleventlist=detailallevent[0];
        $scope.isDisabled= $scope.detailalleventlist.IsAccept !== '' ? true : false;
         $scope.isDisabledwish= $scope.detailalleventlist.IsGreet !== '' ? true : false;
      });
};
$scope.geteventdetail();

   $scope.insertacceptedevents=function(EventId)
    {
      AuthService.insertacceptedevent(EventId).then(function(response)
       {
          $scope.acceptdata= JSON.parse(response).Response;
            if (Response.IsAccept!='0') 
            {
              $scope.detailalleventlist.IsAccept=USERID;
              $ionicPopup.alert({
                template:"निमंत्रण स्वीकारले" ,
               text:'ठीक आहे'});
              $scope.isDisabled=true;
              
            }
        });      
    };

  $scope.insertgreetevent=function(EventId)
  {
    AuthService.insertgreetevent(EventId).then(function(response) 
    {
      $scope.greetdata= JSON.parse(response).Response;
        if (Response.IsGreet!='0') 
        {
            $scope.detailalleventlist.IsGreet=USERID;
            $ionicPopup.alert({
                  template:"अभिवादन पाठविले" ,
                 text:'ठीक आहे'});
            $scope.isDisabledwish=true;
                
           }
    });
};

 $scope.detailallshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास कार्यक्रमाची इत्यंभूत माहिती मिळेल. आपण येथूनहि कार्यक्रम आपल्या दैनंदिनीत “स्वीकार” द्वारे नोंदवू शकता. तसेच कार्यक्रमास जाणे शक्य न झाल्यास आपण त्यांना “अभिवादन” द्वारे आपल्या भावना कळवू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

})

.controller('detailinvitedeventsctrl',function($scope,$stateParams,AuthService,$ionicLoading,$state,$rootScope, $templateCache, $q, $http,Service,$window,$ionicPopup,_) {
  
  //if(typeof analytics !== undefined) { analytics.trackView("detailinvitedevents.html"); }
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;

  var invitid = $rootScope.invitedEventListId;

  $scope.getinvitedeventdetail=function()
  {
    AuthService.eventdetaillist(invitid,USERID).then(function(response)
    {
     
      var detailinvitedevent= JSON.parse(response).Response;
      $scope.detailinvitedlist=detailinvitedevent[0];
       $scope.isDisabled= $scope.detailinvitedlist.IsAccept !== '' ? true : false;
      $scope.isDisabledwish= $scope.detailinvitedlist.IsGreet !== '' ? true : false;
      
    });
  };
  $scope.getinvitedeventdetail();
  
  $scope.insertacceptedevents=function(EventId)
    {
      
      AuthService.insertacceptedevent(EventId).then(function(response) 
      {
        
         $scope.acceptdata= JSON.parse(response).Response;
          if (Response.IsAccept!='0') 
           {
              $scope.detailinvitedlist.IsAccept=USERID;
               $ionicPopup.alert({
               template:"निमंत्रण स्वीकारले" ,
              text:'ठीक आहे'});
              $scope.isDisabled=true;
              
            }
       });      
    };

    $scope.insertgreetevent=function(EventId)
    {
      AuthService.insertgreetevent(EventId).then(function(response) 
      {
        
        $scope.greetdata= JSON.parse(response).Response;
         if(Response.IsGreet!='0') 
         {
            $scope.detailinvitedlist.IsGreet=USERID;
            $ionicPopup.alert({
                template:"अभिवादन पाठविले",
                 text:'ठीक आहे' 
              });
              $scope.isDisabledwish=true;
              
           }
         });  
    };

     $scope.detailinvitedeventshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास कार्यक्रमाची इत्यंभूत माहिती मिळेल. आपण येथूनहि कार्यक्रम आपल्या दैनंदिनीत “स्वीकार” द्वारे नोंदवू शकता. तसेच कार्यक्रमास जाणे शक्य न झाल्यास आपण त्यांना “अभिवादन” द्वारे आपल्या भावना कळवू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
})

.controller('detailacceptedeventctrl',function($scope,$stateParams,AuthService,$rootScope,  $state, $templateCache, $q, $http,Service,$window,_,$ionicPopup,$ionicLoading) {
  
   //if(typeof analytics !== undefined) { analytics.trackView("detailacceptedevent.html"); }
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
  var acceptid = $rootScope.acceptEventListId;

$scope.getaccptedeventdetail=function()
{
    
    AuthService.eventdetaillist(acceptid,USERID).then(function(response)
   {
     
      var detailaccepteventlist= JSON.parse(response).Response;
      $scope.detailaccepeteddata= detailaccepteventlist[0];
       $scope.isDisabled= $scope.detailaccepeteddata.IsAccept !== '' ? true : false;
      $scope.isDisabledwish= $scope.detailaccepeteddata.IsGreet !== '' ? true : false;
    });
};
$scope.getaccptedeventdetail();


  $scope.insertacceptedevents=function(EventId)
  {
    AuthService.insertacceptedevent(EventId).then(function(response)
    {
        $scope.acceptdata= JSON.parse(response).Response;
        if (Response.IsAccept!='0') 
           {
            $scope.detailaccepeteddata.IsAccept=USERID;
            $ionicPopup.alert({
            template:"निमंत्रण स्वीकारले" ,
           text:'ठीक आहे'});
           }
          $scope.isDisabled=true;
          
      }) ;      
  };

   $scope.insertgreetevent=function(EventId)
    {
      AuthService.insertgreetevent(EventId).then(function(response)
       {
          $scope.greetdata= JSON.parse(response).Response;
          if (Response.IsGreet!='0') 
          {
             $scope.detailaccepeteddata.IsGreet=USERID;
              $ionicPopup.alert({
                  template:"अभिवादन पाठविले",
                   text:'ठीक आहे' });
              $scope.isDisabledwish=true;
          }
        });  
      };

      $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास कार्यक्रमाची इत्यंभूत माहिती मिळेल. आपण येथूनहि कार्यक्रम आपल्या दैनंदिनीत “स्वीकार” द्वारे नोंदवू शकता. तसेच कार्यक्रमास जाणे शक्य न झाल्यास आपण त्यांना “अभिवादन” द्वारे आपल्या भावना कळवू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
})

.controller('detailpasteventctrl',function($scope,$stateParams,$state,AuthService, $templateCache, $q, $rootScope,$http,Service,$window,_,$ionicPopup) {

 //if(typeof analytics !== undefined) { analytics.trackView("detailpastevent.html"); }

  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
  //$scope.pasteventsdata=(angular.fromJson($window.localStorage.PastEventData));
  var pasteventid = $rootScope.pastEventListId;
  $scope.getpasteventdetail=function()
  {
      AuthService.eventdetaillist(pasteventid,USERID).then(function(response)
      {
         
        var pasteventdata= JSON.parse(response).Response;
        $scope.pasteventlist= pasteventdata[0];
        $scope.isDisabled= $scope.pasteventlist.IsAttended !== '' ? true : false;
      });
};
$scope.getpasteventdetail();

  $scope.updateattendancedata=function(EventId)
  {
      
      AuthService.updateattendanceevent(EventId).then(function(response) 
      {
         $scope.attendencedata= JSON.parse(response).Response;
              if (Response.IsAttended!='0') 
                {
                    $scope.pasteventlist.IsAttended=$scope.attendencedata;
                    $ionicPopup.alert({
                    template:"मी उपस्थित होतो",
                     text:'ठीक आहे' });
                    $scope.isDisabled=true;
                }
        });      
  };

    $scope.detailpasteventshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास झालेल्या कार्यक्रमांची इत्यंभूत माहिती मिळेल. आपण कार्यक्रमाला उपस्थित असाल तर "उपस्थित" द्वारे आपली उपस्थिती नोंदवा. कार्यक्रमाच्या संयोजकाला/निमंत्रकाला आपण कार्यक्रमाला उपस्थित होता, हे कळविले जाईल. '
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
})

.controller('insertcustomeventctrl',function($scope, $ionicLoading,$state,AuthService,$templateCache, $q, $rootScope,$http,Service,$window,$ionicPopup) {
  
  //if(typeof analytics !== undefined) { analytics.trackView("progminclude.html"); }

  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;

  $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपण आपल्या महत्वाच्या मिटिंग्स, गुप्त चर्चा, व्यावसायिक खलबत, विशिष्ठ कामांची आठवण, वैयक्तिक कार्यक्रमांची नोंद येथे करू शकता म्हणजे ते कार्यक्रम आपल्या दैनंदिनीत दिसू लागतील. आपले वैयक्तिक कार्यक्रम हे आपल्या व्यतिरिक्त कोणासही दिसणार नाहीत.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

    AuthService.GetEventtype().then(function(response)
      { 
        $scope.eventtypedata= JSON.parse(response).Response;
      });
    $scope.changeeventname=function(eventtypeid)
    {
    AuthService.GetEventNames(eventtypeid).then(function(response)
      { 
        $scope.eventnamedata= JSON.parse(response).Response;
      });
    };

  $scope.datepickerObject = {
  titleLabel: 'तारीख',  //Optional
  todayLabel: 'आज',  //Optional
  closeLabel: 'बंद',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date(),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(), //Optional
  to: new Date(2020, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallback(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallback = function (val) {

  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    $scope.customevent.date=new Date(val);
    console.log('Selected date is : ', val);
    $scope.datepickerObject.inputDate=   moment(new Date(val))
        .format('DD-MM-YYYY');
         console.log('Selected date is : ', $scope.datepickerObject.inputDate);
  }
};




  $scope.customevent={};
  $scope.event={};
  $scope.insertcustomevents=function(eventId,programPlace,eventTitle,eventDescription,programAddress)
  {
        //debugger;
        tempdate =$scope.customevent.date||new Date();
        var test =   new Date (tempdate) ;
        $scope.customevent.eventDay=test.getDate();
        $scope.customevent.eventMonth=test.getMonth()+1;
        $scope.customevent.eventYear=test.getFullYear();
        delete $scope.customevent.date;
      
        var test1 = new Date ($scope.customevent.time);
        
        $scope.customevent.hour=moment($scope.customevent.time).locale('en').format("hh");
        $scope.customevent.minute=moment($scope.customevent.time).locale('en').format("mm");

        var dt = moment($scope.customevent.time).locale('en').format("hh:mm A");

        /*alert(dt);
        console.log(dt);
        console.log(moment($scope.customevent.time).locale('en').format("hh"));
        console.log(moment($scope.customevent.time).locale('en').format("mm"));
        console.log(moment($scope.customevent.time).locale('en').format("A"));*/
       // var hours = ($scope.customevent.hour+24)%24; 
        $scope.customevent.meridiem=moment($scope.customevent.time).locale('en').format("A");
        /*if(hours===0)
        { //At 00 hours we need to show 12 am
            hours=12;
        }
        else if(hours>12)
        {
            hours=hours%12;
            $scope.customevent.meridiem='pm';
        }*/
         delete $scope.customevent.time;
            AuthService.insertcustomevents(eventId,programPlace,eventTitle,eventDescription,programAddress,
            $scope.customevent.eventDay,$scope.customevent.eventMonth,$scope.customevent.eventYear,
            $scope.customevent.hour,$scope.customevent.minute,$scope.customevent.meridiem)
            .then(function(data) 
            {
               if(JSON.parse(data).Error==='false'){
                      $ionicPopup.alert({
                      template:"माहिती जतन झाली" ,
                     text:'ठीक आहे'});
                   
                      delete $scope.customevent.EventId;
                      delete  $scope.event.EventName;
                       delete $scope.customevent.eventTitle;
                      delete $scope.customevent.eventAddress;
                      delete $scope.customevent.eventDescription;
                      delete $scope.customevent.programAddress;
                        delete $scope.datepickerObject.inputDate;
                      delete $scope.customevent.time;
                     $window.location.reload(true);
             }
             else{
                  $ionicPopup.alert({
                        template:"पुन्हा प्रयत्न करा" ,
                       text:'ठीक आहे'});
                  }
             });     


};
$scope.customevent={};
 $scope.resetcustomevents=function()
 {
  $scope.datepickerObject.inputDate='';
  $scope.customevent.time='';
  $scope.customevent.EventId='';
  $scope.event.EventName='';
   $scope.customevent.eventTitle='';
   $scope.customevent.eventDescription='';
    $scope.customevent.eventAddress='';

  
 };

})

.controller('insertvillagectrl', function($scope,$state,AuthService,$ionicLoading, $templateCache,Service,$http,$timeout,$window,$ionicPopup) {

 // if(typeof analytics !== undefined) { analytics.trackView("progminclude2.html"); }
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;

  $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपले कार्यक्षेत्र व प्रभाव अनेक गावात असतो. व त्या गावात/शहरात होणारे कार्यक्रमाची माहिती आपल्याला होणे आवश्यक असते. जसे आपली पंचक्रोशी, नोकरी-व्यवसायाचे गाव/शहर, आपले मूळ गाव, सासुरवाडी, मामाचे-मावशीचे गाव ई. विविध तालुक्यातील गावे आपण येथून निवडू शकता. आपण जी गावे निवडाल, फक्त त्याच गावातील कार्यक्रम “परिसरातील कार्यक्रम” मध्ये दिसू लागतील.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

  AuthService.fetchstate().then(function(response)
  { 
    $scope.statedata= JSON.parse(response).Response;
  });
 
  $scope.district=function(stateId)
  {  
     AuthService.fetchdistrict(stateId).then(function(response)
      { 
        $scope.districtdata= JSON.parse(response).Response;
      });
  
};


$scope.selTaluka=function(DistrictId)
{
     AuthService.fetchtaluka(DistrictId).then(function(response)
      { 
        $scope.taluka= JSON.parse(response).Response;
       
      });
  }; 

  $scope.village=function(TalukaId)
  {
     AuthService.fetchvillages(TalukaId).then(function(response)
      { 
        $scope.villages= JSON.parse(response).Response;
      });
  }; 

    $scope.villages={};
    $scope.events={};
    $scope.insertvillage=function(villageid)
    {
      AuthService.insertvillages(villageid).then(function(response) 
      {
        $scope.villageresponse= JSON.parse(response).Response;
        if($scope.villageresponse == '-1')
        {
          $ionicPopup.alert({
              template:"आधीच निवडलेला होता ",
               text:'ठीक आहे' });
          $scope.events.StateId="";
          $scope.events.DistrictId="";
          $scope.events.TalukaId="";
          $scope.events.VillageId="";
        }
        else
          {
            $scope.fetchvillage(USERID);
            $ionicPopup.alert({
                template:"माहिती जतन झाली",
                 text:'ठीक आहे' });
             $scope.events.StateId="";
             $scope.events.DistrictId="";
             $scope.events.TalukaId="";
             $scope.events.VillageId="";
          }
        
      });
          
    };

      $scope.fetchvillage=function()
      {
         AuthService.displayvillages().then(function(response)
          { 
            $scope.allvillages= JSON.parse(response).Response;

          });
      }; 
      $scope.fetchvillage(USERID);

    $scope.deletevillage=function(villageId)
     {
        AuthService.deletevillages(villageId).then(function(data) 
        {
          $scope.fetchvillage(USERID);
            $ionicPopup.alert({
            template:"माहिती काढुन टाकली" ,
           text:'ठीक आहे'});
          }) ;

    };
    $scope.events={};
    $scope.resetdata=function()
    {
       $scope.events.StateId="";
       $scope.events.DistrictId="";
       $scope.events.TalukaId="";
       $scope.events.VillageId="";

    };
       

})

.controller('usereventlistCtrl', function($scope,$state,$ionicLoading,$window,$http,$ionicPopup,AuthService,$rootScope,$stateParams) {

//if(typeof analytics !== undefined) { analytics.trackView("displayuserevnetdetail.html"); }

     var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
   $scope.usereventshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;आपण आपला वैयक्तिक कार्यक्रम येथून रद्द करू शकता. आपल्या दैनंदिनीतून हा कार्यक्रम हटवला जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };  

  $rootScope.getmycustomevent = function()
  {
       AuthService.getcustomevent().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  //$scope.eventnamedata= $rootScope.eventnamedata;
                  //alert(angular.toJson($scope.eventnamedata));
                  $scope.cutomeventdata= JSON.parse(response).Response;
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };
$rootScope.getmycustomevent(USERID);

$scope.specificallcustomeventpage=function(customevent){
    $rootScope.customeventid=customevent.EventListId;
    $state.go('app.usereventdetail',{'EventListId' : customevent.EventListId});
  };  
  
})

.controller('usereventdetailCtrl',function($scope,$ionicLoading,$stateParams,$ionicPopup, $state,AuthService, $templateCache, $q, $rootScope,$http,Service,$window) {
  //if(typeof analytics !== undefined) { analytics.trackView("displaydetailusereventdetail.html"); }
 var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
  
  var customid = $rootScope.customeventid;
  $scope.getcustomeventdetail=function()
  {
    AuthService.eventdetaillist(customid,USERID).then(function(response)
     {
        var customevent= JSON.parse(response).Response;
        $scope.customeventlist= customevent[0];
      });
  };
$scope.getcustomeventdetail();

 $scope.userdetailshowconfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास कार्यक्रमाची इत्यंभूत माहिती मिळेल. आपण येथूनहि कार्यक्रम आपल्या दैनंदिनीत “स्वीकार” द्वारे नोंदवू शकता. तसेच कार्यक्रमास जाणे शक्य न झाल्यास आपण त्यांना “अभिवादन” द्वारे आपल्या भावना कळवू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

  $scope.deletemycustomevent = function(EventListId)
  {
       AuthService.deletecustomevent(EventListId).then(function(response)
        { 
          
          $scope.deletecustomevent= JSON.parse(response).Response;
          $ionicPopup.alert({
                              template:"माहिती काढुन टाकली",
                              text:'ठीक आहे'
                         }).then (function(data)
                         {
                            $state.go('app.usereventlist');
                            $rootScope.getmycustomevent(USERID);
                         });
        });

  };

})

.controller('programsearchctrl',function($rootScope,$scope,$state,AuthService, $templateCache, $q,$http,Service,$window,$ionicPopup) {

  /*if(typeof analytics !== undefined) { analytics.trackView("eventsearch.html"); }
  if(typeof analytics !== undefined) { analytics.trackView("displayeventsearch.html"); }*/
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
  $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: 'आपण अनेक कार्यक्रम/समारंभ ठरविताना उपस्थित असतो. उदा. लग्नाची तारीख ठरविताना... लग्नाची तारीख ठरविताना आपणास हे पाहावं लागतं कि आपल्या भावकी किंवा गावकीतील लग्न त्याच दिवशी अगोदरच आयोजिलेले नसले पाहिजे, नाहीतर भावकी व गावकी नाराज होते, पाहुण्या रावळयामध्ये आपण नक्की कुठल्या लग्नाला जायचे या विषयी संभ्रम निर्माण होतो व आपल्या लग्नाला गर्दी होत नाही.<br> आपण कार्यक्रमाचा प्रकार,गाव व कालावधी निवडून कार्यक्रम शोधू शकता.<br> कधीही, आपला कार्यक्रम ठरल्या ठरल्या, भव्यदिव्यच्या वेबसाईटवर जाऊन आपल्या कार्यक्रमाची नोंद करा म्हणजे तो कार्यक्रम इतरांना पाहता येईल व इतर, त्याच दिवशी व त्याच वेळी त्यांचा कार्यक्रम आयोजित करणार नाहीत.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

AuthService.fetchstate().then(function(response)
  { 
    $scope.statedata= JSON.parse(response).Response;
  });
 
  $scope.district=function(stateId)
  {
     AuthService.fetchdistrict(stateId).then(function(response)
      { 
        $scope.districtdata= JSON.parse(response).Response;
      });
  }; 

  $scope.selTaluka=function(DistrictId)

  {
     AuthService.fetchtaluka(DistrictId).then(function(response)
      { 
        $scope.taluka= JSON.parse(response).Response;
      });
  }; 
   

  $scope.village=function(talukaId)
  {

     AuthService.fetchvillages(talukaId).then(function(response)
      { 
        $scope.villages= JSON.parse(response).Response;
      });
  }; 


      AuthService.GetEventtype().then(function(response)
      { 
        $scope.eventtypedata= JSON.parse(response).Response;
      });
    $scope.changeeventname=function(eventtypeid)
    {
    AuthService.GetEventNames(eventtypeid).then(function(response)
      { 
        $scope.eventnamedata= JSON.parse(response).Response;
      });
    };

  $scope.events={};

  $scope.eventsearch=function(VillageId,talukaid,eventid,eventtypeid)
  {
   
        var test = new Date ( $scope.events.fromdate);
        $scope.events.fromDay=test.getDate();
        $scope.events.fromMonth=test.getMonth()+1;
        $scope.events.fromYear=test.getFullYear();

         var test1 = new Date ($scope.events.todate);
        $scope.events.toDay=test1.getDate();
        $scope.events.toMonth=test1.getMonth()+1;
        $scope.events.toYear=test1.getFullYear();
        delete $scope.events.fromdate;
        delete $scope.events.todate;
       /* $http({method  : 'GET',
               url     : base_url+"/SearchProgram",
               headers: {'Content-Type': 'application/json'}, 
                params: {"userId":USERID,"villageId": VillageId,"talukaId": talukaid,"eventTypeId":eventtypeid,"eventId":eventid,
                          "fromDay":$scope.events.fromDay,"fromMonth":$scope.events.fromMonth,
                       "fromYear": $scope.events.fromYear,
                       "toDay":$scope.events.toDay,"toMonth": $scope.events.toMonth,"toYear":$scope.events.toYear,
                        "webServicePassword" : "21@bhavyadivya"}
             })*/AuthService.eventsearchprogramresult(eventid,eventtypeid,VillageId,talukaid,
             $scope.events.fromDay,$scope.events.fromMonth,$scope.events.fromYear,
              $scope.events.toDay,$scope.events.toMonth,$scope.events.toYear
             ).then(function(data) {
                  var res= JSON.parse(data);
                  //$rootScope.eventsearchlist=res.Response;
           if (res.Response.length=='0') 
           {
              $ionicPopup.alert({
                    template:"माहिती उपलब्ध नाही",
                     text:'ठीक आहे' });
                delete $scope.events.eventTypeId;
                delete $scope.events.eventId;
                delete $scope.events.fromdate;
                delete $scope.events.todate;
                delete $scope.events.StateId;
                delete $scope.events.DistrictId;
                delete $scope.events.TalukaId;
                delete $scope.events.VillageId;
                delete $scope.datepickerObject.inputDate;
                delete  $scope.datepickerObject1.inputDate;
            }
            else
            {
              $rootScope.eventsearchlist=res.Response;
                $state.go('app.displayeventsearch');
                delete $scope.events.eventTypeId;
                delete $scope.events.eventId;
                delete $scope.events.fromdate;
                 delete $scope.events.todate;
                delete $scope.events.StateId;
                delete $scope.events.DistrictId;
                delete $scope.events.TalukaId;
                delete $scope.events.VillageId;
               delete $scope.datepickerObject.inputDate;
                delete  $scope.datepickerObject1.inputDate;
            }
         });

       };

$scope.datepickerObject = {
  titleLabel: 'तारीख',  //Optional
  todayLabel: 'आज',  //Optional
  closeLabel: 'बंद',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date(),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(2012, 8, 2), //Optional
  to: new Date(2020, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallback(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallback = function (val) {

  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    $scope.events.fromdate=new Date(val);
    console.log('Selected date is : ', val);
    $scope.datepickerObject.inputDate=   moment(new Date(val))
        .format('DD-MM-YYYY');
         console.log('Selected date is : ', $scope.datepickerObject.inputDate);
  }
};


$scope.datepickerObject1 = {
  titleLabel: 'तारीख',  //Optional
  todayLabel: 'आज',  //Optional
  closeLabel: 'बंद',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date(),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(2012, 8, 2), //Optional
  to: new Date(2020, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallbacks(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallbacks = function (val) {
  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    $scope.events.todate=new Date(val);
    console.log('Selected date is : ', val);
    $scope.datepickerObject1.inputDate=   moment(new Date(val))
        .format('DD-MM-YYYY');
         console.log('Selected date is : ', $scope.datepickerObject1.inputDate);
  }
};

  $scope.specificeventsearchpage=function(eventsearchdata){
    $rootScope.eventsearchid=eventsearchdata.EventListId;
    $state.go('app.detaileventlist',{'EventListId' : eventsearchdata.EventListId});
  };

})

.controller('detaileventlistctrl',function($scope,$stateParams,$state,AuthService, $templateCache, $q, $rootScope,$http,Service,$window,$ionicPopup,$ionicLoading) {
  //if(typeof analytics !== undefined) { analytics.trackView("detaileventlist.html"); }
  var USERID = angular.fromJson(localStorage.currentUser)[0].UserId;
  var eventsearchidid =$rootScope.eventsearchid;


$scope.geteventsearchdetail=function()
{
    AuthService.eventdetaillist(eventsearchidid,USERID).then(function(response)
    {
      var detaileventsearchlist= JSON.parse(response).Response;
      $scope.detaileventsearchlist=detaileventsearchlist[0];
      $scope.isDisabled= $scope.detaileventsearchlist.IsAccept !== '' ? true : false;
      $scope.isDisabledwish= $scope.detaileventsearchlist.IsGreet !== '' ? true : false;
    
    });
};
$scope.geteventsearchdetail();

    $scope.insertacceptedevents=function(EventId)
    {
      AuthService.insertacceptedevent(EventId).then(function(response) 
      {
         $scope.acceptdata= JSON.parse(response).Response;
          if (Response.IsAccept!='0') 
            {
              $scope.detaileventsearchlist.IsAccept=USERID;
              $ionicPopup.alert({
                template:"निमंत्रण स्वीकारले" ,
               text:'ठीक आहे'});
              $scope.isDisabled=true;
              
            }
        });      
    };
  
      $scope.insertgreetevent=function(EventId)
      {
        AuthService.insertgreetevent(EventId).then(function(response) 
          {
            $scope.greetdata= JSON.parse(response).Response;
             if (Response.IsGreet!='0') 
              {
                 $scope.detaileventsearchlist.IsGreet=USERID;
                 $ionicPopup.alert({
                      template:"अभिवादन पाठविले" ,
                     text:'ठीक आहे'});
                    $scope.isDisabledwish=true;
                    
               }
      });
    };

      $scope.detaileventderachshowconfrm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास कार्यक्रमाची इत्यंभूत माहिती मिळेल. आपण येथूनहि कार्यक्रम आपल्या दैनंदिनीत “स्वीकार” द्वारे नोंदवू शकता. तसेच कार्यक्रमास जाणे शक्य न झाल्यास आपण त्यांना “अभिवादन” द्वारे आपल्या भावना कळवू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

})

.controller('areanewsctrl', function($scope,$rootScope,$state,AuthService,$templateCache,$ionicPopover,$ionicPopup,$http) {
 
//if(typeof analytics !== undefined) { analytics.trackView("selectednewslist.html"); }

  var template = '<ion-popover-view style="height:210px;margin-top: 7px;">' +
                    '   <ion-content>' +
                    '     <ion-item  ng-click="hidePopOver(\'app.newsinclude\') " style="font-size: 100%;">बातमी समाविष्ट करा</ion-item>' +
                    '     <ion-item ng-click="hidePopOver(\'app.newslist\')" style="font-size: 100%;">माझ्या बातम्या</ion-item>'  +
                    '     <ion-item ng-click="hidePopOver(\'app.taluka\')" style="font-size: 100%;">तालुके निवडा</ion-item>'+
                    ' <ion-item href="https://www.mahanews.gov.in" style="font-size: 100%;">महान्युज</ion-item>'+
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

   $scope.hidePopOver = function(path) {
      $scope.popover.hide();
      $state.go(path);
    };

     $scope.showConfirm = function()
        {
         var alertPopup = $ionicPopup.alert({
          title: 'माहिती',
          template: '&nbsp;&nbsp;&nbsp;जगभरातील बातम्या सांगायला अनेक माध्यमे आहेत पण आपल्या तालुक्यातील बातमी सांगायला कोणी नाही, अशी स्थिती आहे. म्हणूनच लोकांच्या सहभागातूनच आपल्या परिसरातील/तालुक्यातील बातम्या आपल्यापर्यंत पोहोचविल्या आहेत. आपणही आपणास समजलेली वा आपल्या परिसरात घडलेली बातमी ताबडतोब प्रसारित करा. आपण निवडलेल्या तालुक्यातीलच बातम्या येथे दिसतील.'
          });

           alertPopup.then(function(res) {
             if(res) {
               console.log('You are sure');
             } else {
               console.log('You are not sure');
             }
           });
        };

   $scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title : 'माहिती',
       template: 'चुकीची व अफवा पसरविणाऱ्या बातम्या काढून टाकण्यात येतील.प्रक्षोभक किंवा समाज विघातक बातम्यासाठी आपणावर कायदेशीर कारवाई करण्यात येईल आणि आपले खाते बंद करण्यात येईल.',
        text:'ठीक आहे'
     });

     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };

 $scope.posts = [];
  $scope.page = 1;
  $scope.newsdata = 1;
  $scope.fetchnews = function()
  {

      AuthService.fetchingnews().then(function(response)
        { 
          if(JSON.parse(response).Error=="true")
          {
            $scope.$broadcast('scroll.refreshComplete');
            $ionicPopup.alert({
                template:"पुन्हा प्रयत्न करा" ,
                text:'ठीक आहे'});
          }
          else
          {
              $scope.newsdata= JSON.parse(response).Response;
              $scope.$broadcast('scroll.refreshComplete');
                }
           }, function (reason) 
           {
                $scope.$broadcast('scroll.refreshComplete');
            });

  };
  $scope.fetchnews();
  $scope.loadMoreData = function(){
    $scope.page += 1;

    AuthService.fetchingnews($scope.page)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.newsdata = data.pages;
      alert(angular.toJson($scope.newsdata));
      var new_posts = AuthService.shortenPosts(data.posts);
      $scope.posts = $scope.posts.concat(new_posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.newsdata > $scope.page;
  };

  
   $scope.specificnewspage=function(news){
    $rootScope.NewsId=news.NewsId;
    $state.go('app.displaydetailnews',{'NewsId' : news.NewsId});
  };

   
})

.controller('displaydetailnewsctrl',function($scope,AuthService,AdMob,$state, $templateCache, $ionicPopup,$q, $rootScope,$http,$window,_){
  
  //if(typeof analytics !== undefined) { analytics.trackView("displaydetailnews.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  var id = $rootScope.NewsId;

  $scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे बातमी संपूर्ण स्वरूपात व जास्तीत जास्त २ प्रतिमांसहित दिसेल. आपण सदर बातमी वाचून त्यावर प्रतिक्रिया देखील लिहू शकता. तसेच सदर बातमी आपणास आक्षेपार्ह वाटल्यास आपण “आक्षेप” वर क्लिक करून आक्षेप घेऊ शकता. ५ जणांनी आक्षेप घेतल्यास सदर बातमी काढून टाकण्यात येईल. परंतु बातमी योग्य आहे आणि उगाच त्रास देण्याच्या हेतूने कोणी आक्षेप घेत असल्यास अशाचे सदस्यत्व कायमस्वरूपी रद्द केले जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

    function getfetchNews()
    {
        AuthService.fetchnewsdetails(id).then(function(response)
          {  
            $scope.newsdetail= JSON.parse(response).Response[0];
            $scope.isDisabled= $scope.newsdetail.IsObjected !== 0 ? false : true;
           });


    } 
    getfetchNews();

    AuthService.fetchtaggedarea(id).then(function(response)
    {  
          $scope.taggedareafornews= JSON.parse(response).Response;
    });

     AuthService.fetchshare().then(function(response)
      { 
       var ShareKara=JSON.parse(response).Response;
        $rootScope.shareid=ShareKara[0];
        var shareid  = $rootScope.shareid.ShareKara;
      });

  $scope.sharePost = function(link){
    window.plugins.socialsharing.share($scope.newsdetail.Heading , null, null,  $rootScope.shareid.ShareKara);
  };

  $scope.objection= function (id)
  {
     AuthService.newsobjection(id).then(function(response) 
      {
         $scope.objdata=JSON.parse(response).Response;
          if (response.IsObjected!='0') 
          {
             $scope.newsdetail.IsObjected=$scope.objdata;
             $ionicPopup.alert({
                template:"आक्षेप नोंदवला",
                text:'ठीक आहे' });
              $scope.isDisabled=true;
          }
        }); 
   };
})

.controller('newsaddcommentctrl',function($scope,AuthService,$stateParams,$state, $templateCache,$q, $rootScope,$http,$ionicActionSheet,$window,$ionicPopup) {
  
 // if(typeof analytics !== undefined) { analytics.trackView("newsaddcomment.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

  var id =$stateParams.NewsId||$rootScope.NewsId;
 

  $scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपण या बातमीवर प्रतिक्रिया देऊ शकता, आपले मन व्यक्त करू शकता. प्रतिक्रिया देताना सदर प्रतिक्रिया विघातक व जाती धर्मात तेढ निर्माण करणारी नसेल याची खात्री करा.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

  $scope.insertcomment= function (newscomment)
  {
     AuthService.insertnewscomment(newscomment).then(function(data) 
      {
        delete $scope.insertcomment.comment;
         $scope.fetcnewscomment(USERID);
      });    
  };

 $scope.fetcnewscomment = function()
  {
    AuthService.fetcnewscomments(id).then(function(response)
        { 
          $scope.allcomment= JSON.parse(response).Response;
        });
  };
  $scope.fetcnewscomment(USERID);

  $scope.showActionsheet = function(CommentId,UserId) {
    $ionicActionSheet.show({
      titleText: 'प्रतिक्रिया काढुन टाका',
      buttons: [
       ],
      destructiveText: 'काढुन टाका',
      cancelText: 'काढुन टाका',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        return true;
      },
      destructiveButtonClicked: function() {

      if(UserId==USERID)
      {
        AuthService.deletetnewscomment(CommentId).then(function(data) {
           $ionicPopup.alert({
                template:"माहिती काढुन टाकली" ,
               text:'ठीक आहे'});
             $scope.fetcnewscomment(USERID);
           }) ;
          return true;
      }
      else
      {
        $ionicPopup.alert({
          template:"लॉगिन वापरकर्ता भिन्न आहेत म्हणून हटवले जाऊ शकत नाही" ,
         text:'ठीक आहे'});
      }    
      }
    });
  };
 })

.controller('displaynewsctrl', function($scope,$state,AuthService,$templateCache,$ionicPopover,$http,$window,$ionicPopup,$rootScope) {

/*if(typeof analytics !== undefined) { analytics.trackView("others.html"); }

if(typeof analytics !== undefined) { analytics.trackView("taluka.html"); }*/
var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

  var template = '<ion-popover-view style="height:210px;margin-top: 7px;">' +
                    '   <ion-content>' +
                    '     <ion-item  ng-click="hidePopOver(\'app.newsinclude\') " style="font-size: 100%;">बातमी समाविष्ट करा</ion-item>' +
                    '     <ion-item ng-click="hidePopOver(\'app.newslist\')" style="font-size: 100%;">माझ्या बातम्या</ion-item>'  +
                    '     <ion-item ng-click="hidePopOver(\'app.taluka\')" style="font-size: 100%;">तालुके निवडा</ion-item>'+
                    ' <ion-item href="https://www.mahanews.gov.in" style="font-size: 100%;">महान्युज</ion-item>'+
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

 $scope.hidePopOver = function(path) {
    $scope.popover.hide();
    $state.go(path);
  };

   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपणास ज्या तालुक्यातील बातम्या पहायच्या असतील असे राज्यातील कुठलेही तालुके आपण निवडू शकता. आपण सध्या राहता तो तालुका/शहर, मूळ तालुका, नोकरी-व्यवसायाच्या दृष्टीने महत्वाचा तालुका आपण निवडू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
    $scope.othershowConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;बऱ्याचदा आपल्याला आपल्या परिसराव्यतिरिक्त काही विशिष्ट भागातील/तालुक्यातील बातमी पहायची असते, अशांकरिता हि तात्पुरती व्यवस्था केली आहे.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

      AuthService.fetchstate().then(function(response)
      { 
        $scope.statedata= JSON.parse(response).Response;
      });
     
      $scope.district=function(stateId)
      {
         AuthService.fetchdistrict(stateId).then(function(response)
          { 
            $scope.districtdata= JSON.parse(response).Response;
          });
      }; 

      $scope.selTaluka=function(DistrictId)
      {
        
         AuthService.fetchtaluka(DistrictId).then(function(response)
          { 
            $scope.taluka= JSON.parse(response).Response;
          });
      }; 
   
    
      $scope.inserttaluka=function(stateid,districtid,TalukaId)
      {

        AuthService.inserttaluka(TalukaId, districtid, stateid).then(function(response) 
        {
         
         $scope.talukaresponse= JSON.parse(response).Response;
          if($scope.talukaresponse == "-1")
          {
            $ionicPopup.alert({
                template:"आधीच निवडलेला होता",
                 text:'ठीक आहे' });
            $scope.events.StateId="";
            $scope.events.DistrictId="";
            $scope.events.TalukaId="";
          }
          else
            {
            $scope.fetchtaluka(USERID);
            $ionicPopup.alert({
                template:"माहिती जतन झाली",
                 text:'ठीक आहे' });
            $scope.events.StateId="";
             $scope.events.DistrictId="";
             $scope.events.TalukaId="";
             $scope.events.VillageId="";
            }
          });      
      };

      $scope.fetchtaluka = function()
      {
         AuthService.displaytaluka().then(function(response)
          { 
            $scope.alltaluka= JSON.parse(response).Response;
          });
      };

      $scope.fetchtaluka(USERID);

        $scope.dodisplaynews = function()
        {
          $state.go('app.displaynews');
        };

      $scope.deleteTaluka=function(TalukaId)
      {
          AuthService.deletetaluka(TalukaId).then(function(data) 
          {
            $ionicPopup.alert({
                  template:"माहिती काढुन टाकली" ,
                   text:'ठीक आहे'});
            $scope.fetchtaluka();
          }) ;      
      }; 

      $scope.events={};
      $scope.resetdatas=function()
      {
        $scope.events.StateId="";
        $scope.events.DistrictId="";
        $scope.events.TalukaId="";
      } ;

     $scope.searchnews = function(stateid,districtid,talukaid)
      {
          AuthService.fetchingselectednews(stateid,districtid,talukaid).then(function(response)
          { 
             var searchresponse=JSON.parse(response);
            if(searchresponse.Response.length=='0')
            {
              $ionicPopup.alert({
                                template:"बातम्या उपलब्ध नाही",
                                text:'ठीक आहे'
                           });
              delete  $scope.events.StateId;
              delete  $scope.events.DistrictId;
              delete  $scope.events.TalukaId;
                  
            }
            else
            {
              $scope.searchresult= searchresponse.Response;
               delete  $scope.events.StateId;
              delete  $scope.events.DistrictId;
              delete  $scope.events.TalukaId;
            }

            });
     }; 

   $scope.specificsearchnewspage=function(selectednews){
    $rootScope.searchnewsid=selectednews.NewsId;
    $state.go('app.searchnewslist',{'NewsId' : selectednews.NewsId});
  };

})

.controller('displaydetailsearchnews',function($scope,$stateParams,AuthService,$state,$templateCache,$ionicPopup,$q,$rootScope,$http,$window,_){
  
  //if(typeof analytics !== undefined) { analytics.trackView("searchnewslist.html"); }

  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  
  var id = $stateParams.NewsId||$rootScope.searchnewsid;

  $scope.searchnewsshowConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे बातमी संपूर्ण स्वरूपात व जास्तीत जास्त २ प्रतिमांसहित दिसेल. आपण सदर बातमी वाचून त्यावर प्रतिक्रिया देखील लिहू शकता. तसेच सदर बातमी आपणास आक्षेपार्ह वाटल्यास आपण “आक्षेप” वर क्लिक करून आक्षेप घेऊ शकता. ५ जणांनी आक्षेप घेतल्यास सदर बातमी काढून टाकण्यात येईल. परंतु बातमी योग्य आहे आणि उगाच त्रास देण्याच्या हेतूने कोणी आक्षेप घेत असल्यास अशाचे सदस्यत्व कायमस्वरूपी रद्द केले जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

    function getsearchingNews()
    {
     AuthService.fetchnewsdetails(id).then(function(response)
        {  
          $scope.newsdetail= JSON.parse(response).Response[0];

           $scope.isDisabled= $scope.newsdetail.IsObjected !== 0 ? false : true;
         
         });
       } 
    getsearchingNews();

    AuthService.fetchtaggedarea(id).then(function(response)
    {  
          $scope.taggedareafornews= JSON.parse(response).Response;
    });

    AuthService.fetchshare().then(function(response)
      { 
       var ShareKara=JSON.parse(response).Response;
        $rootScope.shareid=ShareKara[0];
         var shareid  = $rootScope.shareid.ShareKara;
         /*alert($rootScope.shareid.ShareKara);*/
      });

      $scope.sharePost = function(link){
        window.plugins.socialsharing.share($scope.newsdetail.Heading, null, null, $rootScope.shareid.ShareKara);
      };

    $scope.objection= function (id)
    {
      AuthService.newsobjection(id).then(function(response) 
      {
          $scope.objdata=JSON.parse(response).Response;
          if (response.IsObjected!='0') 
           {
             $scope.newsdetail.IsObjected=$scope.objdata;
              $ionicPopup.alert({
                  template:"आक्षेप नोंदवला",
                    text:'ठीक आहे' });
              $scope.isDisabled=true;
            }
        }); 
     };
})

.controller('newslistctrl',function($scope,$state,AuthService,$templateCache,$q,$rootScope,$http,$window,$ionicPopup) {
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

   // if(typeof analytics !== undefined) { analytics.trackView("newslist.html"); }

 $scope.usernewslistshowConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपण प्रकाशित केलेल्या बातम्यांची यादी येथे दिसेल. प्रत्येक बातमीची वाचक संख्या व एकूण प्रतिक्रीया  देखील दिसतील. आपण जर दर्जेदार बातम्या देत असाल व आपला वाचकवर्ग मोठा असेल तर आपणास आमचे वार्ताहर म्हणून देखील काम करण्याची संधी भविष्यात उपलब्ध होऊ शकते.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };
  $rootScope.fetchusernewslist = function()
  {
      AuthService.fetchusernewslist().then(function(response)
        { 
          $scope.newslistdata= JSON.parse(response).Response;
          $scope.$broadcast('scroll.refreshComplete');
      }, function (reason) {
          $scope.$broadcast('scroll.refreshComplete');
      }
       );
  }; 
 $rootScope.fetchusernewslist(USERID);

  $scope.userspecificnewspage=function(newslist){
    $rootScope.newslistid=newslist.NewsId;
    $state.go('app.detailusernews',{'NewsId' : newslist.NewsId});
  };
})


.controller('detailusernewsctrl',function($scope,$stateParams,AuthService,$state, $templateCache,$ionicPopup, $q, $rootScope,$http,$window,_){
  
  //if(typeof analytics !== undefined) { analytics.trackView("displayuserdetailnews.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  var id = $rootScope.newslistid;

  $scope.detailusernewsshowConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे बातमी संपूर्ण स्वरूपात व जास्तीत जास्त २ प्रतिमांसहित दिसेल. आपण सदर बातमी वाचून त्यावर प्रतिक्रिया देखील लिहू शकता. तसेच सदर बातमी आपणास आक्षेपार्ह वाटल्यास आपण “आक्षेप” वर क्लिक करून आक्षेप घेऊ शकता. ५ जणांनी आक्षेप घेतल्यास सदर बातमी काढून टाकण्यात येईल. परंतु बातमी योग्य आहे आणि उगाच त्रास देण्याच्या हेतूने कोणी आक्षेप घेत असल्यास अशाचे सदस्यत्व कायमस्वरूपी रद्द केले जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

    function getdetailuserNews()
    {
       AuthService.fetchnewsdetails(id).then(function(response)
          {  
           
            $scope.newsdetail= JSON.parse(response).Response[0];
             $scope.isDisabled= $scope.newsdetail.IsObjected !== 0 ? false : true;
           
           });
     } 
    getdetailuserNews();

    AuthService.fetchtaggedarea(id).then(function(response)
    {  
      $scope.taggedareafornews= JSON.parse(response).Response;
    });


    $scope.deleteusernews=function(newsid)
      {
         AuthService.deleteusernews(newsid).then(function(data) 
         {
            
            $ionicPopup.alert({
                template:"माहिती काढुन टाकली" ,
               text:'ठीक आहे'}).then(function(data)
               {
                $state.go('app.newslist');
                 $rootScope.fetchusernewslist(USERID);
               });
           
          }) ;      
     };


  $scope.sharePost = function(link){
    window.plugins.socialsharing.share('Check the bhavyadivya: ', null, null,  "https://www.facebook.com/bhavyadivya/");
  };

  $scope.objection= function (id)
  {
      AuthService.newsobjection(id).then(function(response) {
        $scope.objdata=JSON.parse(response).Response;
        if (response.IsObjected!='0') 
         {
            $scope.newsdetail.IsObjected=$scope.objdata;
            $ionicPopup.alert({
               template:"आक्षेप नोंदवला",
                text:'ठीक आहे' });
            $scope.isDisabled=true;
                
          }
          }); 
   };
})

.controller('insertnewsctrl', function($scope,$state,$ionicPlatform,AuthService,$cordovaCamera, $templateCache,Service,$http,$window,$ionicPopover,$ionicPopup,$cordovaFileTransfer,$ionicLoading) {

//if(typeof analytics !== undefined) { analytics.trackView("newsinclude.html"); }
 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
$scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपणास आपल्या परिसरात घडलेली घटना, दुर्घटना, कार्यक्रमाची माहिती वा अन्य काही समर्पक मजकूर जर बातमीच्या स्वरुपात मांडायचा असेल तर आपण सदर बातमी आपणास हवा असलेला प्रसारण विभाग निवडून समाविष्ठ करू शकता. सदस्य आपल्या बातम्या तालुका निहाय, जिल्हा निहाय किंवा संपूर्ण राज्यासाठी देखील प्रकाशित करू शकतात. आपला मजकूर खोटा, चुकीचा, समाज विघातक असल्यास वा ५ जणांनी त्यावर आक्षेप घेतल्यास सदर बातमी काढून टाकली जाईल. तसेच आपली सदस्यता कायमची रद्द केली जाईल व आपणावर कायदेशीर कारवाईची देखील तजवीज केली जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

  /*    AuthService.GetUserProfile().then(function(response)
      { 
        var alldata=  JSON.parse(response).Response;
        $scope.profiledata=alldata[0];
         var DistrictId= $scope.profiledata.ParDistrictId;
        //alert(angular.toJson(DistrictId));
         AuthService.fetchtaluka(DistrictId).then(function(response)
          {  
              $scope.taluka= JSON.parse(response).Response;
          });
      });*/

    $scope.getFile = function () 
    {
          $scope.progress = 0;
          fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function(result) {
                            $scope.imageSrc = result;
                        });
      };

      AuthService.fetchstate().then(function(response)
      { 
        $scope.statedata= JSON.parse(response).Response;
      });
       AuthService.fetchdistrictprofile().then(function(response)
      { 
        $scope.districtdata= JSON.parse(response).Response;
       
      });

      $scope.district=function(stateId)
      {
        AuthService.fetchdistrict(stateId).then(function(response)
         { 
            $scope.districtdata= JSON.parse(response).Response;
        });
       }; 


      $scope.GetTaluka=function(DistrictId)
      {
            AuthService.fetchtaluka(DistrictId).then(function(response)
            { 
              $scope.taluka= JSON.parse(response).Response;
             });
      };

      $scope.GetVillage=function(TalukaId)
      {
            AuthService.fetchvillages(TalukaId).then(function(response)
            { 
              $scope.villages= JSON.parse(response).Response;
             });
      };

     $scope.insertnews={};
         
          $scope.insertnewsdata=function(choice,stateid, districtid, talukaid,image1,image2,description,title)

         {
            if(choice && choice =="राज्य"){districtid="";talukaid='';}
            if(choice && choice =="जिल्हा"){talukaid='';stateid='';}
            if(choice && choice =="तालुका"){districtid="";stateid='';}
          
          AuthService.insertcustomnewsdata(stateid,districtid,talukaid,description,title).then(function(data)
           {
            var res= JSON.parse(data);
            $scope.newsid=res.Response;
                AuthService.insertnewsimages($scope.newsid,image1,image2).then(function(data) 
                {
                  if(JSON.parse(data).Error=='false')
                  {
                     $ionicPopup.alert({
                          template:"माहिती जतन झाली" ,
                         text:'ठीक आहे'}).then(function(res){
                          $scope.resetnewsdata();});
                    }
                  });
                     
              });  
            
         };

$scope.resetnewsdata=function()
  {
   $window.location.reload(true);
 };
    
})

.controller('specialinfoctrl', function($scope,$state,AuthService,$templateCache,$ionicPopup, $ionicPopover,Service,$http,$window,$ionicLoading,$rootScope) {

//if(typeof analytics !== undefined) { analytics.trackView("selectedspecialnews.html"); }

  var template = '<ion-popover-view style="height:55px;margin-top:7px;" >' +
                    '   <ion-content >' +
                    '      <ion-item ng-click="hidePopOver(\'app.area\')" style="font-size:100%;"> विशेष माहिती क्षेत्र निवडा</ion-item>' +
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

  $scope.hidePopOver = function(path) 
  {
    $scope.popover.hide();
    $state.go(path);
  }; 

  $scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;समाजाचे हित जोपासताना आम्ही कार्यकर्त्याच्या जीवन क्षेत्राचाही विचार केला आहे. त्याचा समाज, त्याचे शिक्षण, त्याच्याशी निगडीत शासकीय योजना, त्याच्या शेतातील पिके, तो सदस्य असलेल्या सहकारी संस्था, त्याच्या व्यवसाय वृद्धीसाठी संधी, त्याचे आरोग्य, त्याचा राजकीय पक्ष / राजकीय नेता, आवडता विचारवंत, आवडता अध्यात्मिक प्रवाह ई. विषयी नेमकी आणि विशेष माहिती आम्ही त्याला त्या त्या क्षेत्रातील तज्ञ व अधिकारी व्यक्तींकडून पुरविणार आहोत.<br> आपण निवडलेल्याच जीवन क्षेत्रातील माहिती या ठिकाणी दिसेल. आपणास नको असलेल्या माहितीचा भडीमार आम्ही करणार नाही. आपण फक्त सुयोग्य जीवन क्षेत्र निवडा.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };
    $scope.othershowConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपणास तात्पुरत्या स्वरुपात इतर क्षेत्रात काय चालले आहे, हे पाहण्यासाठी शोधाद्वारे माहिती घेता येईल. पण आपणास सातत्याने या क्षेत्रातील बातम्या पुरविल्या जाणार नाहीत.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };
  
  $scope.fetchspecialnews = function()
  {
      AuthService.fetchspecialnews().then(function(response)
        { 
          $scope.specialnwsdetails= JSON.parse(response).Response;
          $scope.$broadcast('scroll.refreshComplete');
      }, function (reason) {
          $scope.$broadcast('scroll.refreshComplete');
            }
      );
   };
    $scope.fetchspecialnews();

    $scope.selectedspecificnewspage=function(specialnews){
    $rootScope.spnewsid  =specialnews.NewsId;
    $state.go('app.spcialnewsdetail',{'NewsId' : specialnews.NewsId});
  };
})

.controller('spcialnewsdetailsctrl',function($scope,$stateParams,AuthService,$ionicActionSheet,$state, $templateCache, $q, $rootScope,$http,Service,$window,$ionicPopup,$ionicLoading,$cordovaFileTransfer,$timeout) {


/*if(typeof analytics !== undefined) { analytics.trackView("spcialnewsdetails.html"); }
if(typeof analytics !== undefined) { analytics.trackView("specialnewsaddcomment.html"); }*/
  $scope.filesdata=[];
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  var id = $rootScope.spnewsid;


     $scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास विस्तृतपणे विशेष माहिती पाहता येईल. प्रतिक्रिया देता येतील. प्रश्न विचारता येतील. त्याचप्रमाणे विशेष माहिती सोबत पाठविलेल्या फाईल्सही डाऊनलोड करता येतील.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

   $scope.ytVidId=   function (url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11,})(?:\S+)?$/gmi;
        return (url.match(p)) ? RegExp.$1 : false;
      };


    $scope.openVideo=function(link)
    {
      if(link.indexOf(".pdf")!== -1 || link.indexOf(".doc")!== -1 || link.indexOf(".xls")!== -1)

                    link = "http://docs.google.com/viewer?url=" +  encodeURIComponent(link) ;

        var ref =cordova.InAppBrowser.open(link, "_blank", "location=no");
    };

    function getspecialNews()
    {
        AuthService.fetchspecialnewsdetails(id).then(function(response)
        {  
         
          $scope.specialnwsdetail= JSON.parse(response).Response[0];
         });
    } 
    getspecialNews();

     AuthService.fetchspecialnewsfiledetails(id).then(function(response)
      {  
        $scope.filesdata= JSON.parse(response).Response;
      });

$scope.anotherGoodOne = 'https://www.youtube.com/watch?v=ovZrGBneV3I';
$scope.isClicked=false;
   $scope.download=function(path)
    {
          $scope.isClicked=true;
          var FilesData=$scope.filesdata[0];
          var filename= path.split("/").pop();
         // var targetPath = cordova.file.externalRootDirectory +'bhavyadivya' + "/" + filename;
         var targetPath = cordova.file.dataDirectory +'bhavyadivya' + "/" + filename;
        
          var trustHosts = true;
          var options = {};
         $cordovaFileTransfer.download(path, targetPath, options, trustHosts)
         .then(function(result){
           $ionicPopup.alert({
           template:"फाइल डाउनलोड झाली in the "+ targetPath + " folder",
            text:'ठीक आहे' }).then(function(data){ $scope.isClicked=false;document.getElementById("ft-prog").value =  $scope.downloadProgress =0;});
         }, function(err) {
          $scope.isClicked=false;
        // Error
      },function (progress) {
        $timeout(function (result) {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
          document.getElementById("ft-prog").value =  $scope.downloadProgress;
        });
      
   });
  };
    
  $scope.insertspecialcomment= function (spcomment)
  {
      AuthService.insertspecialcommentser(spcomment).then(function(data) 
      {
        delete $scope.insertspecialcomment.comment;
         $scope.fetcspecialnewscomment(USERID);
      });    
  };

  $scope.fetcspecialnewscomment = function()
  {
    AuthService.fetchspecialnewscomment(id).then(function(response)
        { 
          $scope.allspecialcomment= JSON.parse(response).Response;
        });
  };
   $scope.fetcspecialnewscomment(USERID);

  $scope.showActionsheet = function(CommentId,UserId) {
    $ionicActionSheet.show({
      titleText: 'प्रतिक्रिया काढुन टाका',
      buttons: [
       ],
      destructiveText: 'काढुन टाका',
      cancelText: 'Cancel',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        return true;
      },
      destructiveButtonClicked: function() {
      if(UserId==USERID)
        {
          AuthService.deletespecialcomment(CommentId).then(function(data) {
                   $ionicPopup.alert({
                        template:"माहिती काढुन टाकली",
                        text:'ठीक आहे' });
                    $scope.fetcspecialnewscomment(USERID);
                });
                return true;      
            }
        else
        {
           $ionicPopup.alert({
            template:"लॉगिन वापरकर्ता भिन्न आहेत म्हणून हटवले जाऊ शकत नाही",
             text:'ठीक आहे' });
        }    
      }
    });
  };

     AuthService.fetchshare().then(function(response)
      { 
       var ShareKara=JSON.parse(response).Response;
        $rootScope.shareid=ShareKara[0];
         var shareid  = $rootScope.shareid.ShareKara;
      });
  $scope.sharePost = function(link){
    window.plugins.socialsharing.share($scope.specialnwsdetail.Heading, null, null, $rootScope.shareid.ShareKara);
  };
})

.controller('specialareactrl', function($scope,$state,$ionicLoading,$stateParams,$templateCache,Service,AuthService,$http,$timeout,$window,$ionicPopup,$rootScope) {
var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
           
  /*if(typeof analytics !== undefined) { analytics.trackView("area.html"); }
    if(typeof analytics !== undefined) { analytics.trackView("all.html"); }*/
  $scope.showConfirm = function()
      {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;भव्यदिव्य द्वारे सामजिक कार्यकर्त्याला स्वत:च्या समाजाला एका धाग्याने जोडता येईल. समाजाला अत्याधुनिक ज्ञानाने व परिपूर्ण माहितीने अधिक संपन्न व सजग बनविता येईल. समाजविषयक शासनाच्या विविध योजना, उपक्रम, घोषणा, महामंडळाच्या योजना, समाजपुरुषांची जयंती –पुण्यतिथी, आंदोलने, मोर्चे, हक्क जाणीवा या विषयी प्रबोधन करता येईल.   आपण जर शेतकरी असाल तर पिकनिहाय माहिती आपणास दिली जाईल.आपण जर नोकरी/व्यवसाय करू इच्छित असाल तर आपल्या शिक्षणाला अनुसरुन नोकरी/व्यवसायाच्या संधी आपणास कळविण्यात येतील. आपली जर शेतजमीन असेल तर महसूल व संबंधातील कायद्याविषयी बदल - माहिती आपणास देण्यात येईल.आपण जर राजकीय पक्षाचे सदस्य/पदाधिकारी असाल तर संबंधित पक्षाच्या सूचना, आवाहने निवेदने आपणापर्यंत पोहोचविली जातील.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
      };

  AuthService.specialnewsarea().then(function(response)
  { 
  $scope.infodata= JSON.parse(response).Response;
  });
 
  $scope.vibhagdata=function(infoid)
    {
         AuthService.specialnewsareavibhag(infoid).then(function(response)
          { 
            $scope.infodata1= JSON.parse(response).Response; 
          });
    };

  $scope.upvibhagdata=function(infoid1)
  {
    AuthService.specialnewsareaupvibhagdata(infoid1).then(function(response)
          { 
            $scope.infodata2= JSON.parse(response).Response;
          });
  };

  $scope.areas={};
  $scope.areas1={};
  $scope.areas2={};
    $scope.insertarea=function(areaId, vibhagid, upvibhagid)
    {
      
      AuthService.specialinsertarea(areaId, vibhagid, upvibhagid).then(function(response) 
        {
         $scope.arearesponse= JSON.parse(response).Response;
          if($scope.arearesponse == '-1')
          {
            $ionicPopup.alert({
                template:"आधीच निवडलेला होता ",
                 text:'ठीक आहे' });
            $scope.area.InfoId="";
            $scope.areas1.InfoType1Id="";
            $scope.areas2.InfoType2Id="";
          }
          else
            {
            $scope.fetcharea(USERID);
            $ionicPopup.alert({
                template:"माहिती जतन झाली",
                 text:'ठीक आहे' });
            $scope.area.InfoId="";
            $scope.areas1.InfoType1Id="";
            $scope.areas2.InfoType2Id="";
            }
        });      
    };

    $scope.fetcharea = function()
    {
      AuthService.specialfetcharea().then(function(response)
        { 
          $scope.allarea= JSON.parse(response).Response;
        });
     };
     $scope.fetcharea(USERID);
     $scope.deletearea=function(infoid)
      {
        AuthService.specialdeletearea(infoid).then(function(data) 
          {
           $ionicPopup.alert({
                template:"माहिती काढुन टाकली",
                 text:'ठीक आहे' });
            $scope.fetcharea(USERID);
          }) ;      
     };
    $scope.areas1={};
    $scope.areas2={};
   
     $scope.resetarea=function()
     {
        $scope.areas.InfoId="";
        $scope.areas1.InfoType1Id="";
        $scope.areas2.InfoType2Id="";

     };

    $scope.areas={};
    $scope.fetchspnews=function(areaId, vibhagid, upvibhagid)
    {
        AuthService.fetchselctedspecialnews(areaId, vibhagid, upvibhagid).then(function(response)
        { 
          if(JSON.parse(response).Response.length=='0')
          {
            $ionicPopup.alert({
                              template:"माहिती उपलब्ध नाही",
                              text:'ठीक आहे'
                         });
            delete $scope.areas.InfoId;
            delete $scope.areas1.InfoType1Id;
            delete  $scope.areas2.InfoType2Id;

                
          }
          else
          {
            $scope.specialnwsdetail= JSON.parse(response).Response;
            delete $scope.areas.InfoId;
            delete $scope.areas1.InfoType1Id;
            delete  $scope.areas2.InfoType2Id;
          }

          });
                
   };

   $scope.selectedspecificnewspage=function(specialnews){
    $rootScope.spnewsid  =specialnews.NewsId;
    $state.go('app.spcialnewsdetail',{'NewsId' : specialnews.NewsId});
  };
      
})

.controller('ratectrl', function($scope,$http,$window,$ionicPopup,AuthService,base_url) {

  $scope.myTitle = 'IONIC RATINGS DEMO';
 var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
 var ratings = angular.fromJson($window.localStorage.Rating);
  $scope.ratingsObject = {
    iconOn: 'ion-ios-star', //Optional
    iconOff: 'ion-ios-star-outline', //Optional
   iconOnColor: '#ff964c', //Optional
    iconOffColor: 'black', //Optional
    rating: ratings, //Optional
   
    readOnly: false, //Optional
    callback: function(rating,index) { //Mandatory    
      $scope.ratingsCallback(rating,index);
    }
     
  };

  $scope.ratingsCallback = function(rating, index) {
   $http({method  : 'GET',
             url     : base_url+"/InsertRateApp",
             headers: {'Content-Type': 'application/json'}, 
             params:{ "userId" : USERID ,"rate":rating ,"webServicePassword" : "21@bhavyadivya"}
      }).then(function(data) 
      {
          $ionicPopup.alert({
          template:"माहिती जतन झाली" ,
         text:'ठीक आहे'});
       });      

  };

  AuthService.GetRateApp().then(function(response)
    { 
      $scope.ratingcount= JSON.parse(response).Response;
      $window.localStorage.Rating=(angular.toJson($scope.ratingcount));
    });

})

.controller('submittoadminctrl', function($scope, $state,$window,$ionicLoading,$http,$ionicPopup,AuthService) {
  
  //if(typeof analytics !== undefined) { analytics.trackView("submittoadmin.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
  $scope.insertdesc={};


  $scope.submittoadmindata=function(image1,image2,description)
  { 
      /*$http.post(base_url+"/ExchangeInfoToAdmin",
     JSON.stringify({"userId":USERID, "description": $scope.insertdesc.desc,"imgStr1": image1,"imgStr2": image2 ,"webServicePassword" : "21@bhavyadivya"}))*/
           AuthService.insertsubmittoadmindata(image1,image2,description) .then(function(data) 
            {
             
             if (JSON.parse(data).Error=== "true")
              {  
                $ionicPopup.alert({
                              template:"पुन्हा प्रयत्न करा",
                              text:'ठीक आहे'
                         });}
              else{
                  var alertPopup=  $ionicPopup.alert({
                                    template:"माहिती जतन झाली",
                                    text:'ठीक आहे'
                               });
                    alertPopup.then(function(res){$scope.resetinsertnoticedata();});
                  }
              
       
             
          });
};
     $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;संपूर्ण मराठी समाज आम्ही एका धाग्याने जोडण्याचा प्रयत्न करतो आहोत. आम्हास आपणाकडून या कार्यात सहकार्याची अपेक्षा आहे. आपल्याला या ॲप मध्ये कुठलीही माहिती, कार्यक्रम पत्रिका, लग्न पत्रिका, बातमी, विशेष माहिती समाविष्ठ करायची असेल तर आपण येथून ती माहिती आम्हास पाठवू शकता. आम्ही सदर माहितीची शहानिशा करून ती माहिती सर्वांपर्यंत पोहोचवू. ॲप संदर्भात आपल्या आपल्या सूचना, सल्ले, तक्रारी, गावे – समाज वाढविणे/कमी करणे, आवडी-निवडी वा आपणास जे वाटते ते, येथून आमच्या पर्यंत पोहोचवा. आम्ही आपल्या प्रत्येक सूचनेची दखल घेऊ.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
   
    $scope.resetinsertnoticedata=function()
    {
         $window.location.reload(true);
   
    };


})



.controller('sharectrl', function($scope, $state,$window,$http,$ionicPopup,AuthService,$rootScope) {


//if(typeof analytics !== undefined) { analytics.trackView("shareapp.html"); }

    AuthService.fetchshare().then(function(response)
      { 
       var ShareKara=JSON.parse(response).Response;
        $rootScope.shareid=ShareKara[0];
         var shareid  = $rootScope.shareid.ShareKara;
      });
  
       $scope.sharePost = function(link)
       {
        window.plugins.socialsharing.share($rootScope.shareid.ShareKara, null, null);
        };
        
       $scope.shareconfirm = function()
         {
           var alertPopup = $ionicPopup.alert({
            title: 'माहिती',
            template: '&nbsp;&nbsp;&nbsp;मराठी सामाजिक कार्यकर्ते व मराठी भाषिकांसाठी निर्माण केलेले भव्यदिव्य हे ॲप व्हॉटसअप, फेसबुक व इतर सोशल मीडियाद्वारे आपण आपल्या मित्र परिवारात, सग्या-सोयऱ्यात, पाहुण्या- रावळयात, गावकी-भावकीमध्ये शेअर करा व आम्हास संपूर्ण मराठी समाज एका धाग्याने बांधण्यात सहकार्य करा. शेअर करताना कृपया आपला ६ आकडी रेफरन्स नंबर टाकण्यास विसरू नका.'

            });

             alertPopup.then(function(res) {
               if(res) {
                 console.log('You are sure');
               } else {
                 console.log('You are not sure');
               }
             });
        };     
  
})

.controller('peoplelistcountctrl', function($scope, $state,$rootScope,$window,$http,$ionicPopup,AuthService) {
//var id = angular.fromJson($window.localStorage.localalldata).RefId;
    //if(typeof analytics !== undefined) { analytics.trackView("peoplelist.html"); }
   $scope.peopleshowconfirm = function()
     {
       var confirmPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;भव्यदिव्य ॲप आपल्या माणसात पसरविण्यासाठी आम्हाला मदत करा. आपण व्हॉटसअप, फेसबुक व इतर सोशल मीडियाद्वारे हे ॲप पसरविल्यामुळे किती लोकांनी ते डाऊनलोड केले आहे हे आपणास येथे दिसेल. डाऊनलोड केलेल्या यादीतील मंडळी हि आपल्यावर १००% टक्के विश्वास टाकणारी मंडळी आहेत. कृपया त्यांना कुठलीही अडचण पडल्यास पहिली मदत करा.'

        });

         confirmPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    }; 

    $scope.openBrowser = function() { 
     var options = {
          location: 'yes',
          clearcache: 'yes',
          toolbar: 'no'
       };

     // $cordovaInAppBrowser.open('www.ravighate.com', '_blank', options)
      var ref =cordova.InAppBrowser.open('http://bhavyadivya.com/User/SendNotificationToMyList.aspx', "_blank", "location=no");
     
       };    
    AuthService.fetchrefid().then(function(response)
      { 
       var RefId=JSON.parse(response).Response;
        $rootScope.refid=RefId[0];
         var reid  = $rootScope.refid.RefId;
            AuthService.fetchrefcounts(reid).then(function(response)
            { 
              var refcount=JSON.parse(response).Response;
              $scope.refcountdata=refcount[0];
             });

             $scope.getpeoplelist=function()
               {
                  AuthService.getpeoplelist(reid).then(function(response)
                  { 
                    $scope.peoplelistdata=JSON.parse(response).Response;
         
                 });
    
              };
          
      });
  
})

.controller('publicnoticectrl', function($scope,$state,$ionicLoading,AuthService,$rootScope, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicPopover,$stateParams) {
 /*if(typeof analytics !== undefined) { analytics.trackView("getnoticelist.html"); }
if(typeof analytics !== undefined) { analytics.trackView("searchnotice.html"); }*/
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;


  var template = '<ion-popover-view style="height:270px;margin-top:7px;" >' +
                    '   <ion-content >' +
                     '<ion-item ng-click="hidePopOver(\'app.userpublicnotice\')" style="font-size:100%;">मी भरलेल्या नोटीसा</ion-item>' +
                    '  <ion-item ng-click="hidePopOver(\'app.insertvillagepublicnotice\')" style="font-size:100%;"> गावे निवडा</ion-item>' +
                    ' <ion-item href="https://mahabhulekh.maharashtra.gov.in" style="font-size: 100%;">७/१२ व ८ अ उतारा</ion-item>'+
                    ' <ion-item href="https://gras.mahakosh.gov.in/echallan/igr" style="font-size: 100%;">इ - चलन</ion-item>'+
                    ' <ion-item href="https://aaplesarkar.maharashtra.gov.in" style="font-size: 100%;">आपले सरकार</ion-item>'+
                           
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

  $scope.hidePopOver = function(path) 
  {
    $scope.popover.hide();
    $state.go(path);
  }; 

  $scope.getnoticeconfrim = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;२ एप्रिल २०१७ पासून सेवा सुरु....<br> आपण आत्ताच आपली मालमत्ता असलेली गावे निवडल्यास, आम्हाला तेवढ्याच गावांसाठी लवकरच हि सेवा सुरु करता येईल. व तसे आपणास नोटिफिकेशनद्वारे कळविले जाईल. आपण निवडलेल्या गावातीलच जाहीर नोटीसा येथे दिसतील.<br>आपण आपले गाव सोडून कामा-धंद्या निमित्त परगावात/महानगरात स्थलांतरित झालेलो असतो. गावाला आपली जमीन कोणाच्यातरी भरवशावर सोडून दिलेली असते. आज आपण वर्तमान पत्रात जमिनीच्या फसवणुकीच्या अनेक घटना वाचतो. मालकाच्या संमतीशिवाय खोटा इसम उभा करून जमिनीची विक्री केली जाते, सातबाऱ्यावर दुसऱ्याचे नाव लावले जाते, असे अनेक प्रकार घडत असतात. खरेदी विक्री होताना जाहीर नोटिस दिली जाते, परंतु, आपण परगावी असल्याने आपल्याला ते माहितीच होत नाही. व आपल्या जमिनीची आपल्या परवानगीशिवाय खरेदी-विक्री होऊ शकते.<br> म्हणून भव्यदिव्य या ॲप द्वारे आपल्या जिल्ह्यातील प्रसिद्ध वर्तमानपत्रातील जाहीर नोटिसा पाहता येतील व स्वत:च्या फसवणुकीची शक्यता कमी करता येईल.'
    });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

$scope.fetchnoticlist = function()
  {
       AuthService.fetchnoticlist().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  $scope.getnoticelistdata= JSON.parse(response).Response;
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };

$scope.fetchnoticlist();

 $scope.specificnoticepage=function(getnotice){
    $rootScope.getnoticeid=getnotice.NoticeId;
    $state.go('app.detailnoticelist',{'NoticeId' : getnotice.NoticeId});
  };

  })

.controller('detailnoticelistctrl',function($scope,$stateParams, AuthService, $state, $templateCache,$ionicPopup,$ionicLoading, $q, $rootScope,$http,Service,$window,_) {
    
    //if(typeof analytics !== undefined) { analytics.trackView("detailnoticelist.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
 
  var id = $rootScope.getnoticeid;

$scope.getnoticedetails=function()
{
     AuthService.detailsfetchnoticlist(id,USERID).then(function(response)
        {
         var getnoticelist= JSON.parse(response).Response;
        $scope.detailgetnoticelist= getnoticelist[0];
        
      });
};
$scope.getnoticedetails();

 $scope.detailnoticeshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास प्रसिद्ध झालेली नोटीस पाहता येतील. वर्तमानपत्रात आलेल्या नोटीसीचे प्रकाशन हक्क त्या त्या वर्तमानपत्राकडे असतात.​'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };
   

})
.controller('searchnoticectrl', function($scope,$state,$ionicLoading,AuthService,$rootScope, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicPopover,$stateParams) {
 /*if(typeof analytics !== undefined) { analytics.trackView("getnoticelist.html"); }
if(typeof analytics !== undefined) { analytics.trackView("searchnotice.html"); }*/
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;


  var template = '<ion-popover-view style="height:270px;margin-top:7px;" >' +
                    '   <ion-content >' +
                     '<ion-item ng-click="hidePopOver(\'app.userpublicnotice\')" style="font-size:100%;">मी भरलेल्या नोटीसा</ion-item>' +
                    '  <ion-item ng-click="hidePopOver(\'app.insertvillagepublicnotice\')" style="font-size:100%;"> गावे निवडा</ion-item>' +
                    ' <ion-item href="https://mahabhulekh.maharashtra.gov.in" style="font-size: 100%;">७/१२ व ८ अ उतारा</ion-item>'+
                    ' <ion-item href="https://gras.mahakosh.gov.in/echallan/igr" style="font-size: 100%;">इ - चलन</ion-item>'+
                    ' <ion-item href="https://aaplesarkar.maharashtra.gov.in" style="font-size: 100%;">आपले सरकार</ion-item>'+
                           
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

  $scope.hidePopOver = function(path) 
  {
    $scope.popover.hide();
    $state.go(path);
  }; 


    $scope.searchnoticeshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;एखाद्या विशिष्ट गावाच्या संदर्भामध्ये प्रसिद्ध झालेल्या जाहीर नोटीसा तात्पुरत्या स्वरूपात इथे शोधू शकता.'


        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };


    AuthService.fetchstate().then(function(response)
    { 
      $scope.statedata= JSON.parse(response).Response;
    });
   
    $scope.district=function(stateId)
    {  
      AuthService.fetchdistrict(stateId).then(function(response)
        { 
          $scope.districtdata= JSON.parse(response).Response;
           var DistrictId=$scope.districtdata[0].DistrictId;
          
        });
    
    };
    
    $scope.selTaluka=function(DistrictId)
    {
     AuthService.fetchtaluka(DistrictId).then(function(response)
        { 
          $scope.taluka= JSON.parse(response).Response;
          
        });
    }; 

    $scope.village=function(TalukaId)
    {
       AuthService.fetchvillages(TalukaId).then(function(response)
        { 
          $scope.villages= JSON.parse(response).Response;
        });
    }; 



    $scope.searchnotice=function(villageid,surveyno)
    {
      
      AuthService.fetchsearchnotice(villageid,surveyno).then(function(response)
          { 
            $scope.searchnoticedata= JSON.parse(response).Response;

            if($scope.searchnoticedata.length ===0 && JSON.parse(response).Error=== "false" )
            {
              
              $ionicPopup.alert({
                                template:"माहिती उपलब्ध नाही",
                                text:'ठीक आहे'
                           });
                  
            }
            else if (JSON.parse(response).Error=== "true")
            {  
              $ionicPopup.alert({
                                template:"पुन्हा प्रयत्न करा",
                                text:'ठीक आहे'
                           });
            }
            else
            {
              $scope.searchnoticedata= JSON.parse(response).Response;
             
            }

            });
         
    };

  $scope.searchsurveynoticepage=function(searchnotice){
    $rootScope.searchnoticeid=searchnotice.NoticeId;
    $state.go('app.detailsearchlist',{'NoticeId' : searchnotice.NoticeId});
  }; 

  })

  .controller('detailsearchlist', function($scope,$state,AuthService,$rootScope, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicLoading ,$ionicPopover,$stateParams) {
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

 //if(typeof analytics !== undefined) { analytics.trackView("detailsearchlist.html"); }
 $scope.detailsearchnoticeshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास प्रसिद्ध झालेली नोटीस पाहता येतील. वर्तमानपत्रात आलेल्या नोटीसीचे प्रकाशन हक्क त्या त्या वर्तमानपत्राकडे असतात.'


        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };


  var id = $rootScope.searchnoticeid;
  
    $scope.getsearchnoticedetails=function()
    {
      AuthService.detailsfetchnoticlist(id).then(function(response)
       {
         var getnoticelist= JSON.parse(response).Response;
          $scope.detailsearchlist= getnoticelist[0];
         
        });
    };
  $scope.getsearchnoticedetails();

})

  .controller('insertnoticectrl', function($scope,$state,AuthService, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicLoading,$ionicPopover,$stateParams) {
  /*if(typeof analytics !== undefined) { analytics.trackView("insertnoticec.html"); }
  if(typeof analytics !== undefined) { analytics.trackView("insertvillagepublicnotice.html"); }*/
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

     $scope.insertshowConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपण एखादी मालमत्ता खरेदी करत असाल अगर आपण स्वत: वकिल म्हणून प्रकाशित करत असाल अगर आपल्या नजरेत  आलेली जाहीर नोटीस आपण आम्हास पाठवू शकता. ती ज्या गावाची असेल त्या गावच्या मालमत्ताधारकांना दिसेल.<br> आपणास जर आपल्या घरी येणा-या वर्तमानपत्रातील जाहीर नोटीसा या अँपवर अपलोड करायच्या असल्यास आमच्याशी संपर्क साधा, हि व्यवसाय संधी “प्रथम येणा-यास प्राधान्य” या तत्वावर देण्यात येईल.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

    AuthService.fetchstate().then(function(response)
    { 
      $scope.statedata= JSON.parse(response).Response;
    });
 
      $scope.district=function(stateId)
      { 
        AuthService.fetchdistrict(stateId).then(function(response)
          { 
            $scope.districtdata= JSON.parse(response).Response;
             var DistrictId=$scope.districtdata[0].DistrictId;
            
          });
       };

      $scope.selTaluka=function(DistrictId)
      {
       AuthService.fetchtaluka(DistrictId).then(function(response)
          { 
            $scope.taluka= JSON.parse(response).Response;
            
          });
      }; 

      $scope.village=function(TalukaId)
      {
         AuthService.fetchvillages(TalukaId).then(function(response)
          { 
            $scope.villages= JSON.parse(response).Response;
          });
      }; 

      AuthService.ftechnewspaperlist().then(function(response)
       {
         $scope.getnewspaperlist= JSON.parse(response).Response;
      });


 $scope.datepickerObject = {
  titleLabel: 'तारीख',  //Optional
  todayLabel: 'आज',  //Optional
  closeLabel: 'Close',  //Optional
  setLabel: 'ठीक',  //Optional
  
  inputDate: new Date(),  //Optional
  mondayFirst: true,  //Optional
  templateType: 'popup', //Optional
  showTodayButton: 'true', //Optional
  
  from: new Date(2012, 8, 2), //Optional
  to: new Date(2020, 8, 25),  //Optional
  callback: function (val) {  //Mandatory
    datePickerCallback(val);
  },
  dateFormat: 'dd-MM-yyyy', //Optional
  closeOnSelect: false, //Optional
};
var datePickerCallback = function (val) {

  if (typeof(val) === 'undefined') {
    console.log('No date selected');
  } else {
    $scope.notice.fromdate=new Date(val);
    console.log('Selected date is : ', val);
    $scope.datepickerObject.inputDate=   moment(new Date(val))
        .format('DD-MM-YYYY');
         console.log('Selected date is : ', $scope.datepickerObject.inputDate);
  }
};
$scope.notice={};
  $scope.insertnotice=function(villageid,surveyno,noticeid,image1,image2)
  {
    
    var test = new Date ( $scope.notice.fromdate|| new Date());
        $scope.notice.fromDay=test.getDate();
        $scope.notice.fromMonth=test.getMonth()+1;
        $scope.notice.fromYear=test.getFullYear();
   
        AuthService.insertcustomnotice(villageid,noticeid,$scope.notice.fromDay,$scope.notice.fromMonth,$scope.notice.fromYear,surveyno,image1,image2) 
            .then(function(data) 
            {
              if (JSON.parse(data).Error=== "true"){  $ionicPopup.alert({
                              template:"पुन्हा प्रयत्न करा",
                              text:'ठीक आहे'
                         });}
              else{
                  var alertPopup=  $ionicPopup.alert({
                                    template:"माहिती जतन झाली",
                                    text:'ठीक आहे'
                               });
                    alertPopup.then(function(res){$scope.resetnoticedata();});
                    
              }
             },function(error)
             {
              $ionicPopup.alert({
                              template:"पुन्हा प्रयत्न करा",
                              text:'ठीक आहे'
                         }); 
            });
  };

  $scope.resetnoticedata=function()
  {
    $window.location.reload(true);
   
  };

  $scope.insertvillagenotice=function(villageid)
    {
      
      AuthService.insertnoticevillages(villageid).then(function(response)
       {
        $scope.noticevillageresponse= JSON.parse(response).Response;
        if($scope.noticevillageresponse == '-1')
        {
          $ionicPopup.alert({
              template:"आधीच निवडलेला होता ",
               text:'ठीक आहे' });
          $scope.notice.StateId="";
          $scope.notice.DistrictId="";
          $scope.notice.TalukaId="";
          $scope.notice.VillageId="";
        }
        else
          {
          $scope.fetchnoticevillage(USERID);
          $ionicPopup.alert({
              template:"माहिती जतन झाली",
               text:'ठीक आहे' });
           $scope.notice.StateId="";
          $scope.notice.DistrictId="";
          $scope.notice.TalukaId="";
          $scope.notice.VillageId="";
          }
        
      }) ;
          
    };

  $scope.fetchnoticevillage=function()
  {
     AuthService.fetchnoticevillages().then(function(response)
      { 
        $scope.getnoticevillages= JSON.parse(response).Response;
      });
  }; 
  $scope.fetchnoticevillage(USERID);

    $scope.deletevillagenotice=function(villageId)
     {
      AuthService.deletevillagenotice(villageId).then(function(data) 
        {
             
              $scope.fetchnoticevillage(USERID);
              $ionicPopup.alert({
              template:"माहिती काढुन टाकली" ,
             text:'ठीक आहे'});
        }) ;

    };
  $scope.notice={};
  $scope.resetinsertvillagedata=function()
  {

      $scope.notice.StateId='';
     $scope.notice.DistrictId='';
     $scope.notice.TalukaId='';
     $scope.notice.VillageId='';
  };
  $scope.publicnoticevillageshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;ज्या ज्या गावात आपली मालमत्ता असेल, अशी गावे आपण येथून निवडू शकता. संबंधित गावातील जाहीर नोटिसांची जेवढी आम्हास शक्य असेल तेवढी माहिती आम्ही आपणास कळवू.'
     });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };



})

.controller('displayusernoticectrl', function($scope,$state,AuthService,$stateParams,$rootScope, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicLoading ,$ionicPopover) {
 //if(typeof analytics !== undefined) { analytics.trackView("usernoticelist.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

  $scope.usernoticeconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;आपण या अँपवर अपलोड केलेल्या सर्व नोटीसा येथे दाखविण्यात येतील. आपणास जर आपल्या घरी येणा-या वर्तमानपत्रातील जाहीर नोटीसा या अँपवर अपलोड करायच्या असल्यास आमच्याशी संपर्क साधा, हि व्यवसाय संधी “प्रथम येणा-यास प्राधान्य” या तत्वावर देण्यात येईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

     $rootScope.fetchusernotices = function()
    {
       AuthService.fetchusernotice().then(function(response)
        { 
          if(JSON.parse(response).Error=="true"){
                  $scope.$broadcast('scroll.refreshComplete');
                  $ionicPopup.alert({
                    template:"पुन्हा प्रयत्न करा" ,
                   text:'ठीक आहे'});
          }
          else{
                  $scope.usernoticedata= JSON.parse(response).Response;
                  $scope.$broadcast('scroll.refreshComplete');
                }
              }, function (reason) {
                  $scope.$broadcast('scroll.refreshComplete');
                }
              );

   };
 
  $rootScope.fetchusernotices(USERID);

  $scope.usernoticespecificpage=function(usernotice)
  {
      $rootScope.usernoticeid=usernotice.NoticeId;
      $state.go('app.detailusernoticelist',{'NoticeId' : usernotice.NoticeId});
    }; 
})

.controller('detailusernoticelist', function($scope,$state,AuthService,$rootScope, $templateCache,Service,$http,$timeout,$window,$ionicPopup,$ionicPopover,$ionicLoading,$stateParams) {
  //if(typeof analytics !== undefined) { analytics.trackView("detailusernoticelis.html"); }
  var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;

 $scope.detailusernoticeshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;येथे आपणास प्रसिद्ध झालेली नोटीस पाहता येतील. वर्तमानपत्रात आलेल्या नोटीसीचे प्रकाशन हक्क त्या त्या वर्तमानपत्राकडे असतात.​'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };

var id = $rootScope.usernoticeid;

  $scope.getusernoticedetails=function()
  {
   
  AuthService.detailsfetchnoticlist(id).then(function(response)
   {
   var detailusernotice= JSON.parse(response).Response;
    $scope.detailusernoticelist= detailusernotice[0];
     
    });
  };

  $scope.getusernoticedetails();

  $scope.deletemynotices=function(noticeid)
  {
        
       AuthService.deletemynotice(noticeid).then(function(response)
        { 
          $scope.deletemynoticedata= JSON.parse(response).Response;
          $ionicPopup.alert({
                              template:"माहिती काढुन टाकली",
                              text:'ठीक आहे'
                         }).then(function(data)
                           {
                            $state.go('app.userpublicnotice');
                             $rootScope.fetchusernotices(USERID);
                           });
        });

  };

})

.controller('contactusctrl', function($scope,$state,$window,$http,$ionicPopup,$ionicLoading,AuthService) {
//if(typeof analytics !== undefined) { analytics.trackView("contactus.html"); }
    var USERID = angular.fromJson($window.localStorage.currentUser)[0].UserId;
   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;आपणास आमच्याशी वैयक्तिकरित्या काही माहिती शेअर करायची असल्यास, काही व्यावसायिक संबंध  जोडायचे असल्यास, कुठल्या विभागाचे प्रतिनिधित्व करायचे असल्यास, आपला विषय सर्व सदस्यापर्यंत पोहोचवायचा असल्यास, आपण जर एखाद्या विषयातील तज्ञ किंवा जाणकार असल्यास, आपण जर एखाद्या संस्था/संघटना/पक्षाचे नेते असल्यास आपल्या सभासद/सदस्य/नागरिकांना एकत्र जोडण्यासाठी आम्हास संपर्क साधू शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    }; 
      $scope.contactmessage={};
     $scope.contactusdata = function(rating, index)
      {
        AuthService.insertcontactusdata($scope.contactmessage.con_message).then(function(data) 
        {
            
            if (JSON.parse(data).Error=== "true")
              {  
                $ionicPopup.alert({
                                template:"पुन्हा प्रयत्न करा",
                                text:'ठीक आहे'
                           });}
                else{
                    var alertPopup=  $ionicPopup.alert({
                                      template:"माहिती जतन झाली",
                                      text:'ठीक आहे'
                                 });
                      alertPopup.then(function(res){});
                    }
         });      

  };
     
  })


.controller('termsconditionsctrl', function($scope, $state,$window,$http,$ionicPopup) {
//if(typeof analytics !== undefined) { analytics.trackView("termsconditions.html"); }
    
    
   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template:'&nbsp;&nbsp;&nbsp;कृपया Disclaimer,Privacy Policy, Terms & Conditions, Legal ई. माहिती आमच्या www.BhavyaDivya.com या वेबसाईटवर पहा. येथे फक्त बोलीभाषेतील शब्द रचना वापरली आहे.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };     
  
})

.controller('notificationctrl', function($scope,$state,$window,$http,$ionicPopup,_) {
//if(typeof analytics !== undefined) { analytics.trackView("notification.html"); }
   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;भव्यदिव्यकडून सर्व सदस्यांसाठी अत्यंत महत्वाची माहिती, ॲप मध्ये झालेले बदल, विनंती, सूचना येथे दिल्या जातील. त्याच बरोबर आपल्या परिसरातील एखाद्या व्यक्तीचे निधन झाल्यास, शासन यंत्रणेस – पोलीसदलास काही तातडीचे निरोप द्यायचे असल्यास किंवा काही नैसर्गिक आपत्ती ओढवल्यास येथे सूचित केले जाईल.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    }; 
$scope.$on('new Notification',function(){
  $scope.showNotification();
});
    $scope.showNotification=function(){

        $scope.notes= localStorage.notification?angular.fromJson(localStorage.notification): [];
        $scope.notes.reverse();

    } ;

     $scope.deleteNote=function(n){

        $scope.notes= angular.fromJson(localStorage.notification)|| [];
       
   
     var status = _.reject($scope.notes, function(all) {

                return all.payload.title == n.payload.title && all.payload.body == n.payload.body ;
            });
     $scope.notes=status;
     
    window.localStorage.notification = JSON.stringify(status);
     $scope.notes.reverse();
  


    } ;

  $scope.showNotification();
})

.controller('aboutusctrl', function($scope, $state,$window,$http,$ionicPopup,$cordovaInAppBrowser) {

    //if(typeof analytics !== undefined) { analytics.trackView("aboutus.html"); }
   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;समाजाच्या सुख: दुख:त सामील होऊन, सातत्याने समाजाच्या भल्याचा विचार करणा-या सामाजिक कार्यकर्त्यांसाठी भव्यदिव्य या ॲप ची निर्मिती करण्यात आलेली आहे. महाराष्ट्रातील निमशहरी व  ग्रामीण जीवनामध्ये मोठ्या कुटुंबात/घरटी असा एकतरी कारभारी/कार्यकर्ता असतो कि ज्याने सामाजिक जीवनात स्वत:ला झोकून दिले आहे. समाजासाठी अहोरात्र झटणारा असा कार्यकर्ता काही भव्यदिव्य ध्येयाने वाटचाल करत असतो. या वाटेवर त्याला खूप जास्त आवश्यकता असते ती सूत्रबद्ध नियोजनाची. आपला दिवस कसा आखावा, कुठले कार्यक्रम स्वीकारावेत, जास्तीत जास्त कार्यक्रमांमध्ये कसे सामील व्हावे हे नियोजन कार्यकर्त्याला या ॲप द्वारे करता येईल व दिवसातील प्रत्येक मिनीट त्याला अधिक चांगल्या प्रकारे वापरता येईल. स्वत:च्या सक्षम नेतृत्वातून, सुयोग्य नियोजनाने, भव्यदिव्य असे कार्यकर्तुत्व आपल्या हातून घडावे हीच या ॲप च्या निर्मितीमागील प्रेरणा आहे. माहिती दळणवळण तंत्रज्ञानाच्या माध्यमातून गेली १५ वर्षे समाजपरिवर्तन व संपन्नतेसाठी कार्यरत असलेल्या श्री रवी घाटे यांच्या संकल्पनेतून या ॲप ची निर्मिती करण्यात आली आहे. रवी घाटे यांना सन २००५-०६ सालचा “राष्ट्रीय युवा पुरस्कार” भारताचे तत्कालीन राष्ट्रपती महामहीम डॉ. ए. पी. जे. अब्दुल कलाम साहेबांच्या शुभहस्ते प्राप्त झाला आहे. देश विदेशातील अनेक नामांकित पुरस्कार व सन्मान त्यांना प्राप्त झाले आहेत. त्यांच्यावर जगभरातील नामांकीत विद्यापीठे व संस्थानी शोधनिबंध व संशोधन पत्र प्रसिद्ध केली आहेत. २०११ साली जगप्रसिद्ध फोर्ब्स इंडिया मासिकाने त्यांच्या उपक्रमास देशातील ५ सर्वोत्तम नवउद्यमांमध्ये सामील केले होते. त्यांच्याविषयी अधिक माहितीसाठी कृपया www.RaviGhate.com या संकेतस्थळास भेट द्यावी.या ॲप ची तांत्रिकबाजू इंजिनियर श्री चेतन जाधव यांनी सक्षमपणे सांभाळली आहे. त्याच्याच २ वर्षांच्या अथक प्रयत्नातून हे ॲप साकारू शकले आहे.'

        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    }; 

    $scope.openBrowser = function() { 
     var options = {
          location: 'yes',
          clearcache: 'yes',
          toolbar: 'no'
       };

     // $cordovaInAppBrowser.open('www.ravighate.com', '_blank', options)
      var ref =cordova.InAppBrowser.open('http://www.ravighate.com', "_blank", "location=no");
     
       };    
        
  
})
.controller('yearwisecountCtrl', function($scope, $state,$window,$http,$ionicPopup) {

    
   $scope.showConfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती ',
        template: '&nbsp;&nbsp;&nbsp;भव्यदिव्यचे सदस्य झालेल्या आपल्या मित्रपरिवाराचे व नातेवाईकांचे वाढदिवस आपल्याला येथे दिसतील. आपण त्यांना येथूनच शुभेच्छाही देऊ शकता.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };     
  
})
// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope,$http) {
  
  // if(typeof analytics !== undefined) { analytics.trackView("feeds-entries.html"); }
  $scope.feeds_categories = [];
  $http.get('http://bdserviceajax.bhavyadivya.com/BDServiceAjax.svc/GetNewsPaper?webServicePassword=21@bhavyadivya').success(function(data) {
    
    var res= JSON.parse(data);
         
    $scope.feeds_categories = res.Response;
  });
})

//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams,base_url) {
 // $scope.category_sources = [];

  $scope.categoryId = $stateParams.categoryId;

$http({
      method: 'GET',
      url:base_url+"/GetNewsRSSCategoryList",
      headers: {
            'Content-Type': 'application/json'}, params:{'NewsPaperId':$scope.categoryId,'webServicePassword':'21@bhavyadivya'}
          })

  .then(function(response) {
    //alert(angular.toJson(response));
    var test=response.data;
    var category= JSON.parse(test);
    $scope.category1=category.Response;
    //alert(angular.toJson($scope.category_sources));
   // var category = _.find(response, {id: $scope.categoryId});
    $scope.categoryTitle = category.Response.CategoryName;
    $scope.category_sources = category.Response.URL;
  });
})

//this method brings posts for a source provider
.controller('FeedEntriesCtrl',function($scope, $stateParams, $http,SocketService , $rootScope,FeedList, $q, $ionicLoading,$ionicPopup) {
     $scope.rssfeedshowconfirm = function()
     {
       var alertPopup = $ionicPopup.alert({
        title: 'माहिती',
        template: '&nbsp;&nbsp;&nbsp;घोषणा: आपणास ताज्या बातम्या मिळवण्यासाठी आता वेगवेगळ्या साईट्स/ ॲप वर जाण्याची गरज नाही. आम्ही मराठीतील नामांकित व विश्वसनीय वर्तमानपत्रांच्या ताज्या बातम्या येथे पाहण्याची सोय केली आहे. बातम्यांच्या मजकुराची जबाबदारी व सर्व हक्क संबंधित वर्तमानपत्राकडे सुरक्षित आहेत.<br>Disclaimer: The news story on this page is the copyright of the cited publication. This is in keeping with the principle of ’Fair dealing’ or ’Fair use’. Visitors may click on the ​"विस्तृत बातमी - ​​Detailed News", in the news story, to visit the original article as it appears on the publication’s website.'
        });

         alertPopup.then(function(res) {
           if(res) {
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    };


  $scope.feed = [];

  //var categoryId = $stateParams.categoryId,
       // sourceId = FeedList.get('http://www.saamana.com/desh-videsh/feed');//$stateParams.sourceId;
    //sourceId1 =  FeedList.get("https://www.majhapaper.com/latest-marathi-news/feed/");
  
  /*  sourceId3 =  FeedList.get("http://www.pudhari.com/feed.aspx?cat_id=54");
    sourceId4 =  FeedList.get("http://www.loksatta.com/maharashtra/feed/");
    sourceId5 =  FeedList.get("http://www.lokmat.com/feeds.php?news=1");
     sourceId6 =  FeedList.get("http://dainikprabhat.com/category/%E0%A4%AE%E0%A4%B9%E0%A4%BE%E0%A4%B0%E0%A4%BE%E0%A4%B7%E0%A5%8D%E0%A4%9F%E0%A5%8D%E0%A4%B0/feed/");
*/
$scope.doRefresh=function(){
    $http({method  : 'GET',
          url     : "https://bhavyadivya.com/BDService.svc/GetNewsRSSXML",
         
          params : {"webServicePassword" : "21@bhavyadivya"}       
         })
      
        .success(function(response){
          console.log(response);
          var x2js = new X2JS();
          var testJson="{\"Error\": \"false\",\"Response\":\"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<rss version=\"2.0\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\">\n<channel>\n  <title>Bhavya Divya News</title>\n  <generator>RSSMix</generator>\n  <link>http://www.rssmix.com/</link>\n  <description>This feed was created by mixing existing feeds from various sources.</description>\n  <language>en-gb</language>\n  \n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-crime-news/150554.html</guid>\n<title><![CDATA[खोलीत कोंडून तरुणीला मारहाण, कपडे फाडले]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-crime-news/150554.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 02:38:22 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/satara-news/150429.html</guid>\n<title><![CDATA[शरद पवारांनी चाखली ‘एक भाकरी चुलीवरची’]]></title>\n<link>http://www.pudhari.com/news/latestnews/satara-news/150429.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 02:18:06 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/nagpur-crime-news/150437.html</guid>\n<title><![CDATA[अल्पवयीन मुलीशी अश्\u200dलील चाळे]]></title>\n<link>http://www.pudhari.com/news/latestnews/nagpur-crime-news/150437.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 02:18:06 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/nashik-news/150478.html</guid>\n<title><![CDATA[आदिवासी महामंडळाला लावला 10 कोटींचा चुना]]></title>\n<link>http://www.pudhari.com/news/latestnews/nashik-news/150478.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 02:17:47 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/national-news/150514.html</guid>\n<title><![CDATA[हादरवून सोडणारे निर्भया कांड]]></title>\n<link>http://www.pudhari.com/news/latestnews/national-news/150514.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 02:17:46 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/rats-in-bihar-are-alcoholics-drink-over-9-lakh-litres-of-seized-alcohol-claims-police/150325.html</guid>\n<title><![CDATA[बिहारी 'उंदरां'नी ढोसली ९ लाख लिटर दारू!]]></title>\n<link>http://www.pudhari.com/news/latestnews/rats-in-bihar-are-alcoholics-drink-over-9-lakh-litres-of-seized-alcohol-claims-police/150325.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Sat, 06 May 2017 01:00:00 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467551</guid>\n<title><![CDATA[राष्ट्रवादीत नेतृत्व विस्ताराची संधी]]></title>\n<link>http://www.loksatta.com/maharashtra-news/internal-dispute-in-ncp-dhananjay-munde-1467551/</link>\n<description><![CDATA[पक्षांतर्गत फाटाफुट]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Sat, 06 May 2017 00:14:32 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467550</guid>\n<title><![CDATA[विदर्भात खादी व ग्रामोद्योगातील रोजगारांत घट]]></title>\n<link>http://www.loksatta.com/maharashtra-news/khadi-and-village-industries-marathi-articles-1467550/</link>\n<description><![CDATA[केळकर समितीच्या शिफारशींकडे दुर्लक्ष]]></description>\n<author>मोहन अटाळकर</author>\n<pubDate>Sat, 06 May 2017 00:12:20 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467549</guid>\n<title><![CDATA[आज उमेदवारी अर्जासाठी गर्दी उसळणार]]></title>\n<link>http://www.loksatta.com/maharashtra-news/municipal-corporation-elections-in-malegaon-1467549/</link>\n<description><![CDATA[मालेगाव महापालिका निवडणूक]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Sat, 06 May 2017 00:10:21 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467548</guid>\n<title><![CDATA[छेडछाडीमुळे युवतीची आत्महत्या]]></title>\n<link>http://www.loksatta.com/maharashtra-news/girl-commits-suicide-after-alleged-sexual-harassment-1467548/</link>\n<description><![CDATA[वडिलांनी संशयिताविरोधात पोलिसांत तक्रार दाखल]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Sat, 06 May 2017 00:09:09 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/story/150365.html</guid>\n<title><![CDATA[सोलापूर : बैलाने मारल्याने शेतकर्\u200dयाचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/story/150365.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 21:50:31 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467270</guid>\n<title><![CDATA[तुरीनंतर हळद गडगडली!]]></title>\n<link>http://www.loksatta.com/maharashtra-news/turmeric-price-increases-tur-dal-1467270/</link>\n<description><![CDATA[हळदीचे भाव पडल्याने शेतकरी वर्ग संकटात]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Fri, 05 May 2017 20:55:47 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467269</guid>\n<title><![CDATA[राज्यातील वीज टंचाईला ‘रतन इंडिया’चाही हातभार!]]></title>\n<link>http://www.loksatta.com/maharashtra-news/rattanindia-power-project-electricity-shortage-1467269/</link>\n<description><![CDATA[विजेचा तुटवडा असल्याने बिकट स्थिती]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Fri, 05 May 2017 20:53:31 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467246</guid>\n<title><![CDATA[विलंब नेमका कोणामुळे?]]></title>\n<link>http://www.loksatta.com/maharashtra-news/raje-ambrishrao-atram-indian-army-marathi-articles-shaheed-jawan-suresh-telami-part-1-1467246/</link>\n<description><![CDATA[पोलीस दलाच्या वतीने येथे प्रसिद्धीला दिलेल्या पत्रकात म्हटले आहे]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Fri, 05 May 2017 20:42:46 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1467271</guid>\n<title><![CDATA[भूसुरुंग स्फोटात कंपनी दलमचा हात?]]></title>\n<link>http://www.loksatta.com/maharashtra-news/the-explosion-of-landmines-at-gadchiroli-district-naxal-attack-1467271/</link>\n<description><![CDATA[गडचिरोली जिल्ह्य़ातील नक्षलवादी हल्ला]]></description>\n<author>रवींद्र जुनारकर</author>\n<pubDate>Fri, 05 May 2017 19:48:29 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/isro-launches-south-asia-satellite-gsat9-from-andhra-pradesh-srikharikota/150296.html</guid>\n<title><![CDATA[श्रीहरिकोटा : 'जीसॅट-९'चे यशस्\u200dवी प्रक्षेपण]]></title>\n<link>http://www.pudhari.com/news/latestnews/isro-launches-south-asia-satellite-gsat9-from-andhra-pradesh-srikharikota/150296.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 18:20:43 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sc-stays-domicile-reservation-in-deemed-private-institutes-in-maharashtra/150301.html</guid>\n<title><![CDATA[मेडिकल प्रवेश: महाराष्ट्राला सुप्रीम कोर्टाचा झटका]]></title>\n<link>http://www.pudhari.com/news/latestnews/sc-stays-domicile-reservation-in-deemed-private-institutes-in-maharashtra/150301.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 17:29:17 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/singer-honey-singh-studio-is-spending-8-hours-everyday/150294.html</guid>\n<title><![CDATA[यो यो हनी सिंहचे नव्\u200dया अल्\u200dबमसाठी हार्डवर्क]]></title>\n<link>http://www.pudhari.com/news/latestnews/singer-honey-singh-studio-is-spending-8-hours-everyday/150294.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 16:31:59 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/facebook-to-hire-3000-more-moderators-to-check-content/150292.html</guid>\n<title><![CDATA[आत्महत्या रोखण्यासाठी फेसबुक नेमणार ३००० कर्मचारी]]></title>\n<link>http://www.pudhari.com/news/latestnews/facebook-to-hire-3000-more-moderators-to-check-content/150292.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 16:00:33 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/supreme-court-pronounced-verdict-confirms-death-penalty-in-delhi-gangrape-case/150291.html</guid>\n<title><![CDATA[निर्भया बलात्कार प्रकरणी चारही दोषींची फाशी कायम]]></title>\n<link>http://www.pudhari.com/news/latestnews/supreme-court-pronounced-verdict-confirms-death-penalty-in-delhi-gangrape-case/150291.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 15:44:55 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sunny-leone-to-do-an-item-song-in-marathi-film/150295.html</guid>\n<title><![CDATA[सनी लिओनी आता मराठी  चित्रपटात]]></title>\n<link>http://www.pudhari.com/news/latestnews/sunny-leone-to-do-an-item-song-in-marathi-film/150295.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 15:28:17 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/gadchiroli-new/150293.html</guid>\n<title><![CDATA[गडचिरोली येथील जखमी जवानांची मुख्यमंत्र्यांनी घेतली भेट]]></title>\n<link>http://www.pudhari.com/news/latestnews/gadchiroli-new/150293.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 14:32:51 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1466985</guid>\n<title><![CDATA[राज्य सरकार ताशी पावणेदोन लाख रूपये भाड्याची विमानं, हेलिकाॅप्टर्स घेणार]]></title>\n<link>http://www.loksatta.com/maharashtra-news/maharashtra-government-to-rent-planes-helicopters-costing-lakhs-per-hour-1466985/</link>\n<description><![CDATA[भाड्याने घेताना दोन खासगी कंपन्यांची मदत]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Fri, 05 May 2017 13:57:43 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/suresh-raina-consoling-young-rishabh-pant-when-he-edges-behind-after-a-whirlwind-knock-of-97-in-43-balls/150290.html</guid>\n<title><![CDATA[...त्\u200dयामुळे रैनाकडून ऋषभ पंतचे कौतुक]]></title>\n<link>http://www.pudhari.com/news/latestnews/suresh-raina-consoling-young-rishabh-pant-when-he-edges-behind-after-a-whirlwind-knock-of-97-in-43-balls/150290.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 13:43:03 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/take-electric-buses-for-state-transport-say-nitin-gadkari/150288.html</guid>\n<title><![CDATA[‘राज्य परिवहनसाठी  इलेक्ट्रिक बसेस घ्या’]]></title>\n<link>http://www.pudhari.com/news/latestnews/take-electric-buses-for-state-transport-say-nitin-gadkari/150288.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 13:34:20 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/massive-door-to-door-search-in-kashmir/150287.html</guid>\n<title><![CDATA[दहशतवाद्यांचा खात्मा करण्यासाठी ४ हजार जवान उतरवले]]></title>\n<link>http://www.pudhari.com/news/latestnews/massive-door-to-door-search-in-kashmir/150287.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 13:08:15 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/grenade-found-in-well-red-fort/150286.html</guid>\n<title><![CDATA[लाल किल्ल्याजवळ ग्रेनेड सापडल्याने खळबळ]]></title>\n<link>http://www.pudhari.com/news/latestnews/grenade-found-in-well-red-fort/150286.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 12:58:02 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/bollywood-actor-salman-khans-movie-tubelight-teaser-released/150285.html</guid>\n<title><![CDATA['ट्यूबलाइट'चा टीजर रिलीज : 'दबंग' सल्\u200dलू दिसला मासूम]]></title>\n<link>http://www.pudhari.com/news/latestnews/bollywood-actor-salman-khans-movie-tubelight-teaser-released/150285.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 12:25:18 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/14-killed-in-road-accident-etah-uttar-pradesh/150284.html</guid>\n<title><![CDATA[उत्तर प्रदेश : एटात भीषण अपघात, १४ ठार]]></title>\n<link>http://www.pudhari.com/news/latestnews/14-killed-in-road-accident-etah-uttar-pradesh/150284.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 12:18:29 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1466912</guid>\n<title><![CDATA[अंबेजोगाईत शेतात मोबाइल टॉवर उभारण्याच्या बहाण्याने फसवणूक]]></title>\n<link>http://www.loksatta.com/maharashtra-news/fraud-case-file-against-mobile-tower-company-in-ambajogai-1466912/</link>\n<description><![CDATA[शहर पोलिस ठाण्यात फसवणुकीचा गुन्हा दाखल करण्यात आला आहे.]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Fri, 05 May 2017 11:19:15 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sc-verdict-today-on-appeals-of-nirbhaya-case-convicts/150283.html</guid>\n<title><![CDATA[निर्भया बलात्कार प्रकरणी चारही दोषींची फाशी कायम]]></title>\n<link>http://www.pudhari.com/news/latestnews/sc-verdict-today-on-appeals-of-nirbhaya-case-convicts/150283.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 10:29:07 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/solapur-jalyukata-shivar-and-pani-foundation-social-work/150282.html</guid>\n<title><![CDATA[मेडशिंगी: जलयुक्तच्या कामाला यश; अवकाळीचे पाणी साठले]]></title>\n<link>http://www.pudhari.com/news/latestnews/solapur-jalyukata-shivar-and-pani-foundation-social-work/150282.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 09:54:41 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-news/150280.html</guid>\n<title><![CDATA[लोडशेडिंगचा झटका]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-news/150280.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 02:50:50 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sports-news/150268.html</guid>\n<title><![CDATA[भारतीय फुटबॉल ‘शंभर नंबरी’]]></title>\n<link>http://www.pudhari.com/news/latestnews/sports-news/150268.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 02:35:51 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kolhapur-crime-news/150269.html</guid>\n<title><![CDATA[वीज कोसळून बालक जागीच ठार]]></title>\n<link>http://www.pudhari.com/news/latestnews/kolhapur-crime-news/150269.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 02:35:39 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sports-news/150265.html</guid>\n<title><![CDATA[दिल्लीने लायन्सला धुतले!]]></title>\n<link>http://www.pudhari.com/news/latestnews/sports-news/150265.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 02:30:36 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/goa-news/150222.html</guid>\n<title><![CDATA[मद्यपी दंगेखोरांना आता जेलची हवा]]></title>\n<link>http://www.pudhari.com/news/latestnews/goa-news/150222.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Fri, 05 May 2017 02:09:57 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1466589</guid>\n<title><![CDATA[तब्बल ४४ वर्षांनंतर मंदिर समिती स्थापन होणार]]></title>\n<link>http://www.loksatta.com/maharashtra-news/shri-vitthal-rukmini-temple-committee-44-years-later-1466589/</link>\n<description><![CDATA[महिलांना अध्यक्ष करा]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Thu, 04 May 2017 22:24:23 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.loksatta.com/?p=1466563</guid>\n<title><![CDATA[थकीत वीज बिलाने म्हैसाळ योजना बंद पडण्याची चिन्हे]]></title>\n<link>http://www.loksatta.com/maharashtra-news/break-power-supply-from-may-7th-at-mhaisal-village-1466563/</link>\n<description><![CDATA[४० हजार हेक्टरवर सिंचन; सांगली परिसरातील चार तालुक्यांना लाभ]]></description>\n<author>लोकसत्ता टीम</author>\n<pubDate>Thu, 04 May 2017 22:21:28 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/terrorists-attack-army-party-in-south-kashmir-2-soldiers-injured-civilian-killed/150108.html</guid>\n<title><![CDATA[शोफियनमध्ये लष्कराच्या तुकडीवर हल्ला]]></title>\n<link>http://www.pudhari.com/news/latestnews/terrorists-attack-army-party-in-south-kashmir-2-soldiers-injured-civilian-killed/150108.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 21:54:58 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/panaji-city-ranked-90th-cleanest-city-in-india-govt-survey/150074.html</guid>\n<title><![CDATA[स्वच्छ सर्वेक्षणात पणजी शहराचा ९० वा क्रमांक]]></title>\n<link>http://www.pudhari.com/news/latestnews/panaji-city-ranked-90th-cleanest-city-in-india-govt-survey/150074.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 18:50:30 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/barack-obama-love-story-before-he-met-michelle-obama/150073.html</guid>\n<title><![CDATA[म्\u200dहणून ओबामांचे पहिले प्रेम झाले नाही यशस्\u200dवी]]></title>\n<link>http://www.pudhari.com/news/latestnews/barack-obama-love-story-before-he-met-michelle-obama/150073.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 18:12:40 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/ramdev-baba-comment-by-patanjali-s-turn-over/150071.html</guid>\n<title><![CDATA[पतंजलीची उलाढाल बघून कपालभाती करावे लागेल : रामदेव]]></title>\n<link>http://www.pudhari.com/news/latestnews/ramdev-baba-comment-by-patanjali-s-turn-over/150071.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 17:15:00 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/indias-nuclear-capable-agni-ii-ballistic-missile-test-fires-from-wheeler-island-odisha-coast/150069.html</guid>\n<title><![CDATA[अग्नि-२ क्षेपणास्\u200dत्राची यशस्\u200dवी चाचणी]]></title>\n<link>http://www.pudhari.com/news/latestnews/indias-nuclear-capable-agni-ii-ballistic-missile-test-fires-from-wheeler-island-odisha-coast/150069.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:42:32 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/ambajogai-crime-news/150070.html</guid>\n<title><![CDATA[टॉवर उभारण्याच्या बहाण्याने ४८ हजारांची फसवणूक]]></title>\n<link>http://www.pudhari.com/news/latestnews/ambajogai-crime-news/150070.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:39:51 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/maratha-reservation-mumbai-hc-seeks-govts-stand/150067.html</guid>\n<title><![CDATA[मराठा आरक्षणाचा चेंडू पुन्हा सरकारकडेच]]></title>\n<link>http://www.pudhari.com/news/latestnews/maratha-reservation-mumbai-hc-seeks-govts-stand/150067.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:37:31 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/gadchiroli-naxalite-news/150068.html</guid>\n<title><![CDATA[नक्षलवाद्यांनी सी-६० कमांडोंची गाडी उडवली, १ जवान शहीद]]></title>\n<link>http://www.pudhari.com/news/latestnews/gadchiroli-naxalite-news/150068.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:28:24 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/bhiwandi-news/150066.html</guid>\n<title><![CDATA[भिवंडी : चप्\u200dपलच्\u200dया गोदामाला आग]]></title>\n<link>http://www.pudhari.com/news/latestnews/bhiwandi-news/150066.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:20:07 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/thane-1-kills-in-railway-accident/150065.html</guid>\n<title><![CDATA[ठाणे : रेल्वे अपघातात महिलेचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/thane-1-kills-in-railway-accident/150065.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 16:17:15 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/stephen-hawking-warns-we-must-colonize-another-planet-soon/150064.html</guid>\n<title><![CDATA[१०० वर्षात माणसाला पृथ्वी सोडावी लागेल : हॉकिंग]]></title>\n<link>http://www.pudhari.com/news/latestnews/stephen-hawking-warns-we-must-colonize-another-planet-soon/150064.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 15:50:35 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/indore-ranked-cleanest-city-in-india-govt-survey/150062.html</guid>\n<title><![CDATA[इंदौर सर्वात स्वच्छ : नवी मुंबई ८ व्या क्रमांकावर]]></title>\n<link>http://www.pudhari.com/news/latestnews/indore-ranked-cleanest-city-in-india-govt-survey/150062.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 14:42:34 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/35-killed-in-iran-coal-mine-blast/150063.html</guid>\n<title><![CDATA[इराण : कोळसा खाणीत स्\u200dफोट, ३५ ठार]]></title>\n<link>http://www.pudhari.com/news/latestnews/35-killed-in-iran-coal-mine-blast/150063.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 14:27:47 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/indian-army-successfully-test-fires-advanced-block-iii-version-of-brahmos-cruise-missile/150061.html</guid>\n<title><![CDATA[ब्राह्\u200dमोस सुपरसोनिक क्षेपणास्\u200dत्राची दुसरी चाचणीही यशस्\u200dवी]]></title>\n<link>http://www.pudhari.com/news/latestnews/indian-army-successfully-test-fires-advanced-block-iii-version-of-brahmos-cruise-missile/150061.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 13:27:10 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/bombay-highcourt-dismisses-appeals-of-12-convicts-in-bilkis-bano-rape-and-murder-case-upholds-of-life-imprisonment/150060.html</guid>\n<title><![CDATA[बिल्\u200dकिस बानो बलात्\u200dकार व हत्\u200dया : ११ आरोपींची जन्\u200dमठेप कायम]]></title>\n<link>http://www.pudhari.com/news/latestnews/bombay-highcourt-dismisses-appeals-of-12-convicts-in-bilkis-bano-rape-and-murder-case-upholds-of-life-imprisonment/150060.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 13:19:49 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/eman-ahmed-flies-to-abu-dhabi-today/150059.html</guid>\n<title><![CDATA[इमानवर आता दुबईत उपचार; वजन ५० टक्के कमी]]></title>\n<link>http://www.pudhari.com/news/latestnews/eman-ahmed-flies-to-abu-dhabi-today/150059.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 13:19:12 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/curruption-case-sasikala-files-review-petition-in-supreme-court/150057.html</guid>\n<title><![CDATA[भ्रष्\u200dटाचार प्रकरणी शशिकलांकडून पुनर्विचार याचिका दाखल]]></title>\n<link>http://www.pudhari.com/news/latestnews/curruption-case-sasikala-files-review-petition-in-supreme-court/150057.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 12:46:07 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sushant-singh-rajput-kendall-jenner-pair-up-for-vogue-india/150058.html</guid>\n<title><![CDATA[सुशांतसोबतची सौंदर्यवती कोण ?]]></title>\n<link>http://www.pudhari.com/news/latestnews/sushant-singh-rajput-kendall-jenner-pair-up-for-vogue-india/150058.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 12:33:18 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/massive-anti-terror-operation-launched-in-j-k-s-shopian/150056.html</guid>\n<title><![CDATA[शोफियनमध्ये दहशतवाद्यांना शोधण्यासाठी मोहीम]]></title>\n<link>http://www.pudhari.com/news/latestnews/massive-anti-terror-operation-launched-in-j-k-s-shopian/150056.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 12:28:35 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/toilet-wont-be-displaced-bmc-answers-to-salman-khans-demand/150055.html</guid>\n<title><![CDATA[सलमानच्या घरासमोरील शौचालय हटवण्यास नकार]]></title>\n<link>http://www.pudhari.com/news/latestnews/toilet-wont-be-displaced-bmc-answers-to-salman-khans-demand/150055.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 11:12:29 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/story/150054.html</guid>\n<title><![CDATA[नगर : लोणी व्यंकनाथ येथे दरोडेखोरांचा ४ तास धुमाकूळ]]></title>\n<link>http://www.pudhari.com/news/latestnews/story/150054.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 10:51:08 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/ordinance-empowers-reserve-bank-of-india-to-act-against-top-loan-defaulters/150053.html</guid>\n<title><![CDATA[करबुडव्यांविरोधात कारवाई करण्यासाठी रिझर्व बँकेला सर्वाअधिकार]]></title>\n<link>http://www.pudhari.com/news/latestnews/ordinance-empowers-reserve-bank-of-india-to-act-against-top-loan-defaulters/150053.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 10:30:48 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/gadchiroli-1-jawan-martyr-in-naxal-attack/150052.html</guid>\n<title><![CDATA[गडचिरोली : नक्षलवाद्यांच्या हल्ल्यात १ जवान शहीद]]></title>\n<link>http://www.pudhari.com/news/latestnews/gadchiroli-1-jawan-martyr-in-naxal-attack/150052.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 09:11:34 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-news/150043.html</guid>\n<title><![CDATA[नागपाड्यात आयएसआयचा एजंट पकडला]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-news/150043.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 02:46:33 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/aurangabad-crime-news/150010.html</guid>\n<title><![CDATA[औरंगाबाद: ‘फिल्मी स्टाईल’ खुनी हल्ला]]></title>\n<link>http://www.pudhari.com/news/latestnews/aurangabad-crime-news/150010.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 02:25:27 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kolhapur-news/150028.html</guid>\n<title><![CDATA[भाजपचा ‘शत-प्रतिशत’चा शड्डू]]></title>\n<link>http://www.pudhari.com/news/latestnews/kolhapur-news/150028.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 02:25:26 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kolhapur-news/150030.html</guid>\n<title><![CDATA[कोल्हापूरच्या फुटबॉलपटूंचे तब्बल 27 गोल्स...]]></title>\n<link>http://www.pudhari.com/news/latestnews/kolhapur-news/150030.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 02:25:26 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-news/150042.html</guid>\n<title><![CDATA[पाकमध्ये घुसून त्याचे तुकडे पाडा]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-news/150042.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 02:25:25 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/producing-p-v-sindhu-biopic-is-challenging-tells-sonu-sood/150072.html</guid>\n<title><![CDATA['पी. व्\u200dही. सिंधुवर बायोपिक बनवणे आव्\u200dहानात्\u200dमक']]></title>\n<link>http://www.pudhari.com/news/latestnews/producing-p-v-sindhu-biopic-is-challenging-tells-sonu-sood/150072.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Thu, 04 May 2017 01:00:00 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-crime-news/149863.html</guid>\n<title><![CDATA[चिमुरड्याचा खाडीत बुडून मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-crime-news/149863.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 22:11:17 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/two-boys-died-by-poisoning-after-eating-fruits-in-beed/149855.html</guid>\n<title><![CDATA[फळांच्या विषबाधेने दोन चिमुकल्यांचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/two-boys-died-by-poisoning-after-eating-fruits-in-beed/149855.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 21:28:42 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/smartron-sachin-tendulkar-phone-launched-price-starts-at-rs-12-999/149860.html</guid>\n<title><![CDATA[स्मार्ट्रोन एसआरटी मोबाईल लाँच]]></title>\n<link>http://www.pudhari.com/news/latestnews/smartron-sachin-tendulkar-phone-launched-price-starts-at-rs-12-999/149860.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 20:45:06 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/rain-in-kolhapur/149809.html</guid>\n<title><![CDATA[कोल्हापूर: विजांच्या कडकडाटासह अवकाळी बरसला]]></title>\n<link>http://www.pudhari.com/news/latestnews/rain-in-kolhapur/149809.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 19:44:25 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/fire-breaks-out-in-godown-in-mumbai/149796.html</guid>\n<title><![CDATA[मुंबई : फायबर गोदामाला भीषण आग]]></title>\n<link>http://www.pudhari.com/news/latestnews/fire-breaks-out-in-godown-in-mumbai/149796.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 18:16:09 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/actor-kamal-haasan-virupam-2-film-poster-release/149797.html</guid>\n<title><![CDATA['विश्वरूपम-२'चे पोस्\u200dटर रिलीज]]></title>\n<link>http://www.pudhari.com/news/latestnews/actor-kamal-haasan-virupam-2-film-poster-release/149797.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 18:06:05 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/fbi-lady-agent-went-to-syria-to-marry-is-terrorist/149795.html</guid>\n<title><![CDATA[गुप्\u200dतहेरच पडली दहशतवाद्याच्\u200dया प्रेमात]]></title>\n<link>http://www.pudhari.com/news/latestnews/fbi-lady-agent-went-to-syria-to-marry-is-terrorist/149795.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 17:57:44 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mns-movement-in-pune/149786.html</guid>\n<title><![CDATA[महापौराच्या घरासमोर मनसेचे कचरा फेको आंदोलन]]></title>\n<link>http://www.pudhari.com/news/latestnews/mns-movement-in-pune/149786.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 17:23:12 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/yavatmal-murder-news/149788.html</guid>\n<title><![CDATA[६ महिन्\u200dयाच्\u200dया बाळाचा कात्रीने भोसकून खून]]></title>\n<link>http://www.pudhari.com/news/latestnews/yavatmal-murder-news/149788.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 17:19:30 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/azlan-shah-hockey-india-defeated-japan-4-3/149782.html</guid>\n<title><![CDATA[भारताचा जपानला दणका]]></title>\n<link>http://www.pudhari.com/news/latestnews/azlan-shah-hockey-india-defeated-japan-4-3/149782.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 16:57:33 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/twenty-thousand-indians-to-return-from-saudi-arabia/149785.html</guid>\n<title><![CDATA[सौदीहून २० हजार भारतीय मायदेशी परतणार]]></title>\n<link>http://www.pudhari.com/news/latestnews/twenty-thousand-indians-to-return-from-saudi-arabia/149785.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 16:49:21 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/death-of-two-children-drowning-in-the-bay-in-thane/149780.html</guid>\n<title><![CDATA[ठाणे : खाडीत बुडून दोन मुलांचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/death-of-two-children-drowning-in-the-bay-in-thane/149780.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 16:47:07 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/narendra-modi-visits-patanjali-research-center-baba-ramdev/149779.html</guid>\n<title><![CDATA['राष्ट्र ऋषि' म्हणल्याने जबाबदारी वाढली : पंतप्रधान]]></title>\n<link>http://www.pudhari.com/news/latestnews/narendra-modi-visits-patanjali-research-center-baba-ramdev/149779.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 16:10:22 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/nagpur-murder-news/149781.html</guid>\n<title><![CDATA[बहिणीशी प्रेमसंबंध; भावाकडून प्रियकराचा खून]]></title>\n<link>http://www.pudhari.com/news/latestnews/nagpur-murder-news/149781.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 14:20:38 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/wrestler-vijay-chaudhari-appointed-as-a-dysp-by-maharashtra-government/149777.html</guid>\n<title><![CDATA['ट्रिपल महाराष्ट्र केसरी' विजय चौधरीची डीवायएसपी पदी नियुक्ती]]></title>\n<link>http://www.pudhari.com/news/latestnews/wrestler-vijay-chaudhari-appointed-as-a-dysp-by-maharashtra-government/149777.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 14:00:10 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/venezuela-inflation-surges/149776.html</guid>\n<title><![CDATA[७०० % महागाईचा देश]]></title>\n<link>http://www.pudhari.com/news/latestnews/venezuela-inflation-surges/149776.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 13:54:39 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/accept-electricity-bill-by-digital-payment-center-gov/149775.html</guid>\n<title><![CDATA[वीज बील डिजिटल पेमेंटद्वारे स्वीकारा; केंद्र]]></title>\n<link>http://www.pudhari.com/news/latestnews/accept-electricity-bill-by-digital-payment-center-gov/149775.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 13:42:25 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/aap-crisis-kumar-vishvas-arvind-kejriwal/149778.html</guid>\n<title><![CDATA['आप'च्\u200dया 'विश्\u200dवास'चा आज फैसला?]]></title>\n<link>http://www.pudhari.com/news/latestnews/aap-crisis-kumar-vishvas-arvind-kejriwal/149778.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 13:32:21 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/pm-narendra-modi-visit-kedarnath-temple-today/149774.html</guid>\n<title><![CDATA[महाप्रलयानंतर 'केदारनाथ'चे दरवाजे खुले, पंतप्रधानांची भेट]]></title>\n<link>http://www.pudhari.com/news/latestnews/pm-narendra-modi-visit-kedarnath-temple-today/149774.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 13:07:34 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kabul-8-killed-in-suicide-bomb-attack-on-nato-convoy/149773.html</guid>\n<title><![CDATA[काबुल : यूएस दूतावासानजीक आत्मघातकी हल्ला, ८ ठार]]></title>\n<link>http://www.pudhari.com/news/latestnews/kabul-8-killed-in-suicide-bomb-attack-on-nato-convoy/149773.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 12:42:55 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-cbi-arrested-commissioner-of-income-tax-department/149772.html</guid>\n<title><![CDATA[मुंबई : आयकर विभागाच्\u200dया आयुक्\u200dतांना अटक]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-cbi-arrested-commissioner-of-income-tax-department/149772.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 12:38:30 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/militants-snatch-6-rifles-from-policemen-guarding-kashmir-court-complex/149771.html</guid>\n<title><![CDATA[जम्मूत दहशतवाद्यांनी पोलिसांच्या ५ रायफल लुटल्या]]></title>\n<link>http://www.pudhari.com/news/latestnews/militants-snatch-6-rifles-from-policemen-guarding-kashmir-court-complex/149771.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 10:46:47 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/nagar-in-accident-1-killed-and-1-seriously-injured/149770.html</guid>\n<title><![CDATA[नगर : अपघातात १ ठार; १ गंभीर जखमी]]></title>\n<link>http://www.pudhari.com/news/latestnews/nagar-in-accident-1-killed-and-1-seriously-injured/149770.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 10:29:44 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/pune-2-wada-in-shukrawar-peth-catches-fire/149768.html</guid>\n<title><![CDATA[पुणे : शुक्रवार पेठेत दोन वाड्यांना आग; एकाचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/pune-2-wada-in-shukrawar-peth-catches-fire/149768.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 08:59:08 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-news/149752.html</guid>\n<title><![CDATA[मराठा आरक्षणाचा चेंडू राज्य सरकारच्या कोर्टात]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-news/149752.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 02:33:29 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/nagpur-crime-news/149587.html</guid>\n<title><![CDATA[सावकाराच्या जाचाने दाम्पत्याची आत्महत्या]]></title>\n<link>http://www.pudhari.com/news/latestnews/nagpur-crime-news/149587.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 02:12:52 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/sangli-news/149621.html</guid>\n<title><![CDATA[शेततळ्यात बुडून दोन भावांचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/sangli-news/149621.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 02:12:33 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/story/149651.html</guid>\n<title><![CDATA[पोलिस कोठडीत युवकाला रक्ताच्या उलट्या]]></title>\n<link>http://www.pudhari.com/news/latestnews/story/149651.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 02:12:32 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/story/149672.html</guid>\n<title><![CDATA[आर्ची, परशा अन् नागराज आता ‘वॅक्स म्युझियम’मध्ये!]]></title>\n<link>http://www.pudhari.com/news/latestnews/story/149672.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 02:12:07 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/shiva-thapa-advances-to-asian-boxing-championship-quarterfinals-with-hard-fought-victory-in-tashkent-uzbekistan/149532.html</guid>\n<title><![CDATA[आशियाई बॉक्सिंग चॅम्\u200dपियनशिप: शिव, सुमित उपांत्\u200dयपूर्व फेरीत]]></title>\n<link>http://www.pudhari.com/news/latestnews/shiva-thapa-advances-to-asian-boxing-championship-quarterfinals-with-hard-fought-victory-in-tashkent-uzbekistan/149532.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Wed, 03 May 2017 01:00:00 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/mumbai-crime-news/149555.html</guid>\n<title><![CDATA[कल्याणमध्ये भर रस्त्यात पत्नीला पेटविले]]></title>\n<link>http://www.pudhari.com/news/latestnews/mumbai-crime-news/149555.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 21:02:10 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/the-state-will-create-a-textile-policy-say-subhash-deshmukh/149554.html</guid>\n<title><![CDATA[‘राज्याचे वस्त्रोद्योग धोरण तयार करणार’]]></title>\n<link>http://www.pudhari.com/news/latestnews/the-state-will-create-a-textile-policy-say-subhash-deshmukh/149554.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 20:02:58 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kolhapur-police-commissionerate-decision-will-be-taken-soon-tells-dgp-in-press-conference/149531.html</guid>\n<title><![CDATA['कोल्\u200dहापूर पोलिस आयुक्\u200dतालयाबाबत लवकरच निर्णय']]></title>\n<link>http://www.pudhari.com/news/latestnews/kolhapur-police-commissionerate-decision-will-be-taken-soon-tells-dgp-in-press-conference/149531.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 17:05:48 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/gst-bill-session-on-20-21-22-may/149528.html</guid>\n<title><![CDATA[GST विधेयक अधिवेशन २०, २१, २२ मेला]]></title>\n<link>http://www.pudhari.com/news/latestnews/gst-bill-session-on-20-21-22-may/149528.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 16:08:59 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/cbi-ed-team-reached-in-london-to-expedite-vijay-mallya-case/149530.html</guid>\n<title><![CDATA[मल्\u200dल्\u200dयांना भारतात आणण्\u200dयासाठी 'सीबीआय' लंडनमध्\u200dये]]></title>\n<link>http://www.pudhari.com/news/latestnews/cbi-ed-team-reached-in-london-to-expedite-vijay-mallya-case/149530.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 15:23:45 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/virendra-sehwag-angry-over-barbaric-killing-of-two-indian-soldiers/149529.html</guid>\n<title><![CDATA['पाकला मोठ्या डोसची गरज' : सेहवाग]]></title>\n<link>http://www.pudhari.com/news/latestnews/virendra-sehwag-angry-over-barbaric-killing-of-two-indian-soldiers/149529.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 14:18:40 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/you-could-soon-buy-a-4g-feature-phone-at-just-rs-1500/149518.html</guid>\n<title><![CDATA[१५०० रुपयात मिळणार ४ जी मोबाईल]]></title>\n<link>http://www.pudhari.com/news/latestnews/you-could-soon-buy-a-4g-feature-phone-at-just-rs-1500/149518.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 14:12:56 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/h-1b-effect-infosys-to-hire-10000-american-techies-open-technology-hubs-in-us/149517.html</guid>\n<title><![CDATA[इन्\u200dफोसिस देणार १० हजार अमेरिकनांना रोजगार]]></title>\n<link>http://www.pudhari.com/news/latestnews/h-1b-effect-infosys-to-hire-10000-american-techies-open-technology-hubs-in-us/149517.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 14:04:28 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/government-gives-army-free-hand-to-avenge-pakistan-s-mutilation-act-sources/149515.html</guid>\n<title><![CDATA[पाकला प्रत्युत्तर देण्यासाठी भारतीय सैन्याला पूर्ण सूट]]></title>\n<link>http://www.pudhari.com/news/latestnews/government-gives-army-free-hand-to-avenge-pakistan-s-mutilation-act-sources/149515.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 13:16:06 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/kajol-clarifies-she-didnt-eat-beef-as-miscommunicated/149516.html</guid>\n<title><![CDATA[काजोलचा 'त्\u200dया' व्\u200dहिडिओचा खुलासा]]></title>\n<link>http://www.pudhari.com/news/latestnews/kajol-clarifies-she-didnt-eat-beef-as-miscommunicated/149516.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 13:06:41 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/satara-youth-death-drown-in-dhom-reservoir/149512.html</guid>\n<title><![CDATA[सातारा : धोम जलाशयात बुडून तरुणाचा मृत्यू]]></title>\n<link>http://www.pudhari.com/news/latestnews/satara-youth-death-drown-in-dhom-reservoir/149512.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 12:37:26 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/all-republican-parties-to-merge-in-republican-party-of-india-gawai-group/149514.html</guid>\n<title><![CDATA['रिपब्\u200dलिकन ऐक्या'च्या पुनरावृत्तीचे संकेत]]></title>\n<link>http://www.pudhari.com/news/latestnews/all-republican-parties-to-merge-in-republican-party-of-india-gawai-group/149514.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 12:34:06 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/government-hikes-lpg-by-rs-2-kerosene-by-26-paise/149511.html</guid>\n<title><![CDATA[अनुदानित एलपीजी सिलेंडर, रॉकेल महाग]]></title>\n<link>http://www.pudhari.com/news/latestnews/government-hikes-lpg-by-rs-2-kerosene-by-26-paise/149511.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 12:19:03 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.pudhari.com/news/latestnews/election-commission-rejects-loksabha-byelections-in-anantnag-jammu-kashmir/149513.html</guid>\n<title><![CDATA[काश्\u200dमीर : अनंतनाग येथील लोकसभेची पोटनिवडणूक रद्द]]></title>\n<link>http://www.pudhari.com/news/latestnews/election-commission-rejects-loksabha-byelections-in-anantnag-jammu-kashmir/149513.html</link>\n<description><![CDATA[]]></description>\n<pubDate>Tue, 02 May 2017 01:00:00 +0100</pubDate>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873987&amp;catid=1</guid>\n<title><![CDATA[कचऱ्याने दमले प्रशासन]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873987&amp;catid=1</link>\n<description><![CDATA[तब्बल १ हजार टन कचऱ्यावर शहरातील वेगवेगळ्या प्रकल्पांमध्ये रोज प्रक्रिया होत आहे, तरीही ६०० टन कचरा शिल्लक राहतो]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874003&amp;catid=1</guid>\n<title><![CDATA[बँक घोटाळ्याची चौकशी सुभाष मोहोड करणार]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874003&amp;catid=1</link>\n<description><![CDATA[वादग्रस्त सेवानिवृत्त जिल्हा न्यायाधीशाला बाजूला करून राज्य शासनाने सेवानिवृत्त प्रधान जिल्हा न्यायाधीश सुभाष मोहोड यांच्याकडे]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873740&amp;catid=1</guid>\n<title><![CDATA[सावधान ! तुम्ही आंबे खात आहात...]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873740&amp;catid=1</link>\n<description><![CDATA[आंबे पिकवण्यासाठी घातक रसायनांचा वापर होत असल्याची माहिती समोर येत आहे.]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873748&amp;catid=1</guid>\n<title><![CDATA[पुस्तकांच्या गावाला शरद पवारांच्या रुपात पहिले वाचक]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873748&amp;catid=1</link>\n<description><![CDATA[सर्व प्रथम गावच्या सरपंच वंदना भिलारे यांच्या घरातील वाचनालयाला त्यांनी भेट दिली.]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873827&amp;catid=1</guid>\n<title><![CDATA[काळानुरुप ‘कोल्हापुरी’त बदल आवश्यक]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873827&amp;catid=1</link>\n<description><![CDATA[सुभाष देसाई : जागतिक बाजारपेठेत मोठी संधी; कोल्हापुरी चप्पल कारागिरांची कार्यशाळा उत्साहात]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873855&amp;catid=1</guid>\n<title><![CDATA[महिला व बालविकास विभागाने रोखला बालविवाह]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873855&amp;catid=1</link>\n<description><![CDATA[कारंजा तालुक्यातील यावर्डी येथील १६ वर्षीय मुलीचा विवाह महिला व बालविकास विभागने रोखला.]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873932&amp;catid=1</guid>\n<title><![CDATA[भोर तालुका : वादळी वाऱ्यासह पावसाचा तडाखा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873932&amp;catid=1</link>\n<description><![CDATA[शहर व परिसरात झालेल्या वादळी वाऱ्यासह सुमारे २ तास झालेल्या पावसामुळे ठिकठिकाणी झाडे मोडून पडल्याने विजेच्या तारा तुटल्या]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873940&amp;catid=1</guid>\n<title><![CDATA[अखाद्य बर्फाचा होतोय जीवघेणा गोळा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873940&amp;catid=1</link>\n<description><![CDATA[उन्हाळा सुरू झाल्याने सध्या शहरातील विविध चौकांत सर्वत्र बर्फ गोळ्यांची दुकाने लागली आहेत. मुलांसह तरुण या बर्फ गोळ्यांचा आस्वाद]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873945&amp;catid=1</guid>\n<title><![CDATA[शहान्नवव्या वर्षी नेत्रदानातून दृष्टी]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873945&amp;catid=1</link>\n<description><![CDATA[आयुष्याची ९६ वर्षे सरली; पण एवढ्या वृद्धापकाळात कोणाला दृष्टिदान मिळाल्याचे आजोबांनी ऐकले होते ना पाहिले. म्हणूनच हे आजोबा]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873956&amp;catid=1</guid>\n<title><![CDATA[वैद्यकीय शिक्षण जनकेंद्री व्हावे]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873956&amp;catid=1</link>\n<description><![CDATA[आजारांशी लढायचे असेल तर सर्व वैद्यकीय शास्त्रांमधील डॉक्टरांमध्ये सुसंवाद असणे आणि उपचारपद्धतींचा आदर करणे गरजेचे]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874004&amp;catid=1</guid>\n<title><![CDATA[आणखी १० अधिकाऱ्यांच्या बदल्या]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874004&amp;catid=1</link>\n<description><![CDATA[राज्यातील आणखी १० सहाय्यक पोलीस आयुक्त/उपअधीक्षक पदावरील अधिकाऱ्यांच्या बदल्या जाहीर करण्यात आल्या आहेत]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873989&amp;catid=1</guid>\n<title><![CDATA[शेतक-यांना कर्जमुक्ती मिळेपर्यंत शिवसेना स्वस्थ बसणार नाही- सावंत]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873989&amp;catid=1</link>\n<description><![CDATA[बाळापुरात निघाला शेतक-यांचा रूमणे मोर्चा]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873996&amp;catid=1</guid>\n<title><![CDATA[राणेंना पक्षश्रेष्ठींची भेट नाहीच]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873996&amp;catid=1</link>\n<description><![CDATA[काँग्रेसचे ज्येष्ठ नेते नारायण राणे यांना पक्षश्रेष्ठींनी बोलावून घेतले असल्याची चर्चा असली तरी प्रत्यक्षात ते काँग्रेसचे सरचिटणीस]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873997&amp;catid=1</guid>\n<title><![CDATA[विस्तारीकरणाच्या विरोधात  लातूर, उस्मानाबादेत बंद]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873997&amp;catid=1</link>\n<description><![CDATA[मुंबई-लातूर एक्स्प्रेसचे बीदरपर्यंत केलेले विस्तारीकरण रद्द करून लातूररेल्वे पूर्ववत करावी. तसेच बीदरला स्वतंत्र रेल्वे सोडावी]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873998&amp;catid=1</guid>\n<title><![CDATA[पालिकेला करावी लागणार प्रतीक्षा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873998&amp;catid=1</link>\n<description><![CDATA[शिवसेना प्रमुख बाळासाहेब ठाकरे यांच्या स्मारकासाठी महापौर बंगल्याजवळील जागा मिळवण्यासाठी आणखी काही काळ प्रतीक्षा]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873999&amp;catid=1</guid>\n<title><![CDATA[लोढा समुहाला ठोठावलेल्या दंडाच्या आदेशाला स्थगिती]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873999&amp;catid=1</link>\n<description><![CDATA[भाजपाचे आमदार मंगलप्रभात लोढा यांच्या लोढा समुहाला वडाळा येथील भूखंडावरील मुद्रांक शुल्क चुकविल्याप्रकरणी]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874000&amp;catid=1</guid>\n<title><![CDATA[पाणी आहे तर वीज नाही : उपलब्धतेत ४ हजार मेगावॅटची घट]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874000&amp;catid=1</link>\n<description><![CDATA[राज्यातील बहुसंख्य भागात तापमानाने चाळीशी गाठल्याने व शेतीला पाणी देण्यासाठी विजेच्या मागणीत तब्बल २ हजार मेगावॅटची]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874001&amp;catid=1</guid>\n<title><![CDATA[पुस्तकांच्या गावाला पवारांची भेट]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874001&amp;catid=1</link>\n<description><![CDATA[पुस्तकांच्या गावाला म्हणजेच भिलारमध्ये शुक्रवारी पहिले वाचक आले ते ज्येष्ठ नेते राष्ट्रवादी काँगे्रस पक्षाचे अध्यक्ष शरद पवार]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874002&amp;catid=1</guid>\n<title><![CDATA[...आता गवतापासून मद्यनिर्मिती?]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874002&amp;catid=1</link>\n<description><![CDATA[गवतापासून मद्यनिर्मिती करण्याचा राज्य सरकारचा मानस असून त्यासाठी येथील खासगी कंपनी आणि आगरकर इन्स्टिट्यूटमध्ये]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874066&amp;catid=1</guid>\n<title><![CDATA[चेरफलवाडी आदिवासी बांधवांचा आदर्श निर्णय]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874066&amp;catid=1</link>\n<description><![CDATA[सर्वसामान्यांचे व आदिवासींचे महागाईने कंबरडे मोडले आहे. त्यात मुला-मुलीचा विवाह करणे म्हणजे खिशाला कात्री, यासाठी वडील]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18873732&amp;catid=1</guid>\n<title><![CDATA[मराठी भाषेत परीपत्रके द्या अन्यथा आंदोलन]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18873732&amp;catid=1</link>\n<description><![CDATA[कानडी वृत्त वाहिन्या तसेच सोशल मीडियावर मराठी आणि समिती विरोधी मत व्यक्त करत मराठी द्वेष दाखवून दिला आहे .]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874020&amp;catid=1</guid>\n<title><![CDATA[महाराष्ट्र विधिमंडळ, आॅस्ट्रेलिया संसदेत करार]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874020&amp;catid=1</link>\n<description><![CDATA[महाराष्ट्र विधिमंडळ आणि न्यू साऊथ वेल्स संसद (आॅस्ट्रेलिया) यांच्यात संसदीय सामंजस्य करार करण्यात आला. या]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874008&amp;catid=1</guid>\n<title><![CDATA[नारायण राणे कुठेही गेले, तरी फरक पडत नाही]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874008&amp;catid=1</link>\n<description><![CDATA[राजकारणात जे समोर येतील त्यांच्याशी सामना करण्याची शिवसेनेची तयारी आहे. आमदार नारायण राणे हे कोणत्याही पक्षात गेले]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874009&amp;catid=1</guid>\n<title><![CDATA[‘पतंजली’ला किरकोळ भावात भूखंड का?]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874009&amp;catid=1</link>\n<description><![CDATA[नागपूर येथील ६ हजार एकर भूखंड योगगुरू रामदेव बाबांच्या पतंजली आयुर्वेदिकला खरोखरच किरकोळ भावात देण्यात आला का]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874010&amp;catid=1</guid>\n<title><![CDATA[रेल्वे प्रकल्पाच्या भूसंपादनासाठी ‘शासन शेतकऱ्यांच्या दारी’]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874010&amp;catid=1</link>\n<description><![CDATA[वर्धा-यवतमाळ-नांदेड या महत्त्वाकांक्षी रेल्वेप्रकल्पाचे भूसंपादन वेगाने व्हावे म्हणून शासन मोबदल्यासाठी थेट शेतकऱ्यांच्या]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874013&amp;catid=1</guid>\n<title><![CDATA[महाडमधील लाचखोर पोलिसाला अटक]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874013&amp;catid=1</link>\n<description><![CDATA[शहर पोलीस ठाण्यातील सहायक पोलीस निरीक्षक कुंदन गावडे यांना शुक्रवारी एका व्यक्तीकडून बारा हजार रुपयांची लाच स्वीकारत]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874014&amp;catid=1</guid>\n<title><![CDATA[खारेगाव टोल नाका बंद होणार]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874014&amp;catid=1</link>\n<description><![CDATA[राज्यातील पहिला टोल नाका अशी ओळख असलेला ठाण्यातील खारेगाव टोल नाका अखेर येत्या १३ मेपासून कायमस्वरूपी बंद करण्यात]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874018&amp;catid=1</guid>\n<title><![CDATA[परदेश दौऱ्यावरून मंत्र्यांना परत बोलवा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874018&amp;catid=1</link>\n<description><![CDATA[राज्य सरकारच्या ढिसाळ नियोजनामुळे तुरीचे चांगले उत्पादन येऊनही खरेदी व्यवस्था पूर्णपणे कोलमडली. शेतकऱ्यांवर आत्महत्येची]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874019&amp;catid=1</guid>\n<title><![CDATA[निष्कारण फायली फिरवत बसू नका : मुनंगटीवार]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874019&amp;catid=1</link>\n<description><![CDATA[वित्त विभागाकडे मंजुरीसाठी येणाऱ्या प्रस्तावांवर अधिक स्पष्टीकरणाची गरज असेल तिथे संबंधित अधिकाऱ्यांना बोलावून घ्या, सर्व]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874006&amp;catid=1</guid>\n<title><![CDATA[सव्वा गुंठ्यावर तब्बल ५८४ कोटींचे कर्ज!]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874006&amp;catid=1</link>\n<description><![CDATA[कागदपत्र व नियमांची पूर्तता करूनही सर्वसामान्यांना काही लाख रुपयांचे कर्ज देण्यास टाळाटाळ करणाऱ्या बँकांनी अवघ्या सव्वा]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874023&amp;catid=1</guid>\n<title><![CDATA[घोडबंदरवरील नव्या बांधकामांना स्थगिती]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874023&amp;catid=1</link>\n<description><![CDATA[लोक उष्म्यामुळे त्रासले आहेत, अशा स्थितीत पाणी हे जीवन वाचविणारे आहे. ठाणे महापालिका त्यांच्या वैधानिक कर्तव्यापासून]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874021&amp;catid=1</guid>\n<title><![CDATA[माजी मंत्री विजयकुमार गावितांवर भ्रष्टाचाराचा ठपका]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874021&amp;catid=1</link>\n<description><![CDATA[आदिवासी विकास योजनांमध्ये कोट्यवधी रुपयांचा घोटाळा झाल्याचे राज्य सरकारने नियुक्त केलेल्या एम.जी. गायकवाड चौकशी]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874024&amp;catid=1</guid>\n<title><![CDATA[सर्व औषध खरेदी  दीक्षित समितीमार्फतच]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874024&amp;catid=1</link>\n<description><![CDATA[वैद्यकीय शिक्षण विभागाने माजी पोलीस महासंचालकांच्या अध्यक्षतेखाली नेमलेल्या समितीला राज्य शासनाच्या सर्व विभागांमार्फत]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874025&amp;catid=1</guid>\n<title><![CDATA[मेडिकल प्रवेशप्रकरणी राज्याची निराशा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874025&amp;catid=1</link>\n<description><![CDATA[खासगी आणि अभिमत वैद्यकीय महाविद्यालयांतील ६७ टक्के अधिवास कोट्याला मुंबई उच्च न्यायालयाने दिलेला स्थगिती]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874026&amp;catid=1</guid>\n<title><![CDATA[मेट्रो-३चा मार्ग मोकळा]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874026&amp;catid=1</link>\n<description><![CDATA[जेवढी झाडे कापण्यात येणार तेवढी झाडे प्रकल्पाचे काम पूर्ण झाल्यावर त्याच परिसरात लावण्यात येतील, अशी हमी मुंबई मेट्रो रेल]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874039&amp;catid=1</guid>\n<title><![CDATA[सहकारी बॅँकेमध्ये नोटाबंदीची झळ कायम]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874039&amp;catid=1</link>\n<description><![CDATA[नोटबंदीच्या दणक्यानंतर सावरणाऱ्या ठाणे व पालघर जिल्ह्यातील सहकारी बॅँकाची परवड अजून सुरुच असून शेतकऱ्यांच्या मित्र]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874042&amp;catid=1</guid>\n<title><![CDATA[दारूसाठी पालघर पालिकेची धावपळ]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874042&amp;catid=1</link>\n<description><![CDATA[सर्वोच्च न्यायालयाच्या दणक्याने पालघर मधील बंद पडलेल्या दारू दुकाने पुन्हा सुरु करण्यावर पालघर नगरपरिषदेच्या शनिवारच्या सभेत शिक्का]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874045&amp;catid=1</guid>\n<title><![CDATA[औरंगाबादमधील आरोपी ठाण्यात चतुर्भुज]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874045&amp;catid=1</link>\n<description><![CDATA[पाच महिन्यांपूर्वी औरंगाबाद पोलिसांच्या तावडीतून पळालेल्या खुनाच्या आरोपीस ठाणे पोलिसांनी शुक्रवारी अटक केली. या आरोपीच्या]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874046&amp;catid=1</guid>\n<title><![CDATA[विहार लेकची पक्ष्यांना संजीवनी]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874046&amp;catid=1</link>\n<description><![CDATA[उन्हाळा व पावसाळ्याच्या कालावधीत सर्वांचे लक्ष वेधून घेणाऱ्या संजय गांधी नॅशनल पार्क व त्यास लागून असलेल्या ठाणे शहराचे]]></description>\n</item>\n\n<item>\n<guid>http://www.lokmat.com/storypage.php?newsid=18874077&amp;catid=1</guid>\n<title><![CDATA[सफाई कामगारच पुरस्काराचे खरे मानकरी]]></title>\n<link>http://www.lokmat.com/storypage.php?newsid=18874077&amp;catid=1</link>\n<description><![CDATA[स्वच्छ भारत अभियानामध्ये महापालिकेला मिळालेल्या पुरस्काराचे खरे श्रेय सफाई कामगारांचे आहे. शहराचे खरे स्वच्छतादूत]]></description>\n</item>\n</channel>\n</rss>\"}"
          var par=JSON.parse(testJson);
      var aftCnv = x2js.xml_str2json(par.Response);
          console.log(aftCnv);
          
        })
        .error(function(data){
          //deferred.reject();  
        });
}
 
  $scope.openNews=function(link)
  {
      var ref =cordova.InAppBrowser.open(link, "_blank", "location=no");
  };

  $scope.doRefresh();
});

